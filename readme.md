## About Larasport

Larasport is a powerful e-commerce platform based on the Laravel PHP framework.


## Key features

- Authentication and Authorization
- Login, Register functionality
- Social media Authentication
- User roles
- Messages and Notifications
- Admin dashboard
- Shopping cart functionality
- Fixed/Percentage coupon
- Order management
- Stripe payment integration
- Mailchimp integration
- Live search functionality
- Product Availability
- Product Reviews
- Product Ratings
- Product Wishlist
- Cross-sells
- Up-sells
- Product Attributes
- Guest Checkout
- User profile management
- Advanced Product Filtering


## DEMO

http://larasport.corellonmedia.com

Admin credentials:
- Email: markopetrovic91@gmail.com
- Password: test
