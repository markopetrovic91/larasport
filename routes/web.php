<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Apply coupon in the cart page
use App\Coupon;
use App\Http\Controllers\User\UserController;
use App\PercentOffCoupon;

// Coupon routes
Route::post('apply-coupon', 'Admin\CouponsController@applyCoupon')->name('applyCoupon');
Route::delete('/removeCoupon', 'Admin\CouponsController@removeCoupon')->name('removeCoupon');

// Admin routes
Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/index', 'AdminController@admin')->name('admin');
    Route::resource('/products', 'ProductsController');
    Route::get('/reviews', 'ReviewsController@index')->name('reviews.index');
    Route::get('/reviews/{id}/edit', 'ReviewsController@edit')->name('reviews.edit');
    Route::delete('/reviews/{id}', 'ReviewsController@destroy')->name('reviews.destroy');
    Route::get('/reviews/approved/{approved}', 'ReviewsController@approved')->name('reviews.approved');
    Route::post('/reviews/{id}', 'ReviewsController@update')->name('reviews.update');
    Route::get('/orders', 'OrdersController@index')->name('orders.index');
    Route::get('/orders/{id}/edit', 'OrdersController@edit')->name('orders.edit');
    Route::delete('/orders/{id}', 'OrdersController@destroy')->name('orders.destroy');
    Route::put('/orders/{id}', 'OrdersController@update')->name('orders.update');
    Route::resource('/categories', 'CategoriesController');
    Route::delete('/categories/product/{catid}/{proid}', 'CategoriesController@removeProductFromCategory')->name('category.product.remove');
    Route::resource('/attributes', 'AttributesController');
    Route::post('/attributevalues', 'AttributeValuesController@store')->name('attvalue.store');
    Route::delete('/attributevalues/{value}', 'AttributeValuesController@destroy')->name('attvalue.destroy');
    Route::get('/customers/subscribers', 'MailchimpController@index')->name('mailchimp.index');
    Route::resource('/coupons', 'CouponsController');
});

// Various pages routes
Route::get('/', 'PagesController@home')->name('home');
Route::get('/privacy-policy', 'PagesController@privacyPolicy');
Route::get('/terms-of-service', 'PagesController@termsOfService');
Route::get('/thank-you', 'Shop\ConfirmationController@index')->name('confirmation.index');

// Contact form
Route::get('contact', 'PagesController@getContact')->name('contact.index');
Route::post('sendcontact', 'PagesController@postContact')->name('contact.send');

// SHOP
Route::group(['prefix' => 'shop', 'namespace' => 'Shop'], function () {
    Route::get('/', 'ShopController@index')->name('shop.index');
    Route::get('/{product}', 'ShopController@show')->name('shop.show');
});

//Review
Route::resource('review', 'Shop\ReviewController');

Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart', 'CartController@store')->name('cart.store');
Route::delete('/cart/{product}', 'CartController@destroy')->name('cart.destroy');
Route::patch('/cart/{product}', 'CartController@update')->name('cart.update');

// Favorite - wishlist
Route::post('/favorite/{product}', 'Shop\ShopController@favoriteProduct');
Route::post('/unfavorite/{product}', 'Shop\ShopController@unFavoriteProduct');
Route::get('/wishlist', 'Shop\ShopController@getWishList');

// Checkout page
Route::get('checkout', 'Shop\CheckoutController@index')->name('checkout.index');
Route::post('checkout', 'Shop\CheckoutController@store')->name('checkout.store');

// User profile
Route::group(['prefix' => 'user', 'middleware' => 'auth', 'namespace' => 'User'], function () {
    Route::get('/{id}/index', 'UserController@index')->name('user.index');
    Route::get('/{id}/info', 'UserController@info')->name('user.info');
    Route::get('/{id}/addresses', 'UserController@addresses')->name('user.addresses');
    Route::get('/{id}/orders', 'UserController@orders')->name('user.orders');
    Route::get('/{id}/subscriptions', 'UserController@subscription')->name('user.subscriptions');
    Route::post('/{id}/subscriptions', 'UserController@subscriptionPost')->name('user.subscriptions');
    Route::get('/{id}/wishlist', 'UserController@wishlist')->name('user.wishlist');
    Route::delete('/{id}/wishlist/{product}/remove', 'UserController@removeFromWishlist')->name('user.removeWProduct');
    Route::delete('/{id}/wishlist/clear', 'UserController@clearWishlist')->name('user.clearWishlist');
    Route::get('/{id}/orders/{orderid}/reorder', 'UserController@quickReorder')->name('user.reorder');
    Route::post('/{id}/info/update', 'UserController@userUpdate')->name('user.update');
});

// Auth
Auth::routes();
Route::get('admin/logout', 'Auth\LoginController@logout')->name('admin.logout');

// Social auth routes
Route::get('/auth/facebook/redirect', 'Auth\SocialAuthController@fbRedirect')->name('fb.redirect');
Route::get('/auth/facebook/callback', 'Auth\SocialAuthController@fbCallback');
Route::get('/auth/twitter/redirect', 'Auth\SocialAuthController@twRedirect')->name('tw.redirect');
Route::get('/auth/twitter/callback', 'Auth\SocialAuthController@twCallback');
Route::get('/auth/linkedin/redirect', 'Auth\SocialAuthController@ldRedirect')->name('ld.redirect');
Route::get('/auth/linkedin/callback', 'Auth\SocialAuthController@ldCallback');
Route::get('/auth/google/redirect', 'Auth\SocialAuthController@ggRedirect')->name('gg.redirect');
Route::get('/auth/google/callback', 'Auth\SocialAuthController@ggCallback');
Route::post('/subscribe', 'Admin\MailchimpController@store')->name('subscribe');
Route::get('/search/{term?}', 'Shop\ShopController@getSearchResults')->name('search.results');

// 302 redirects
Route::get('/admin', function () {
    return redirect()->route('admin');
});
