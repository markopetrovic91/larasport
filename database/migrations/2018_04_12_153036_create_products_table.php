<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->unique();
            $table->string('name');
            $table->string('slug');
            $table->text('details')->nullable();
            $table->decimal('price', 6, 2);
            $table->decimal('sale_price', 6, 2)->nullable()->default(0.00);
            $table->text('description')->nullable();
            $table->integer('inventory');
            $table->integer('weight')->nullable();
            $table->boolean('featured')->default(true);
            $table->boolean('new')->default(true);
            $table->boolean('on_sale')->default(false);
            $table->boolean('outlet')->default(false);
            $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id')->references('id')->on('brands')
                ->onUpdate('cascade')->onDelete('set null');
            $table->string('product_image')->default('coming-soon.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
