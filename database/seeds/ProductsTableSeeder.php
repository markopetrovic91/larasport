<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'REEBOK PATIKE ZPRINT 3D WE',
            'sku' => 'reebok-z3dwe',
            'slug' => 'reebok-patike-zprint-3d-we',
            'details' => 'neki kratak description za REEBOK PATIKE ZPRINT 3D WE',
            'price' => 60.90,
            'sale_price' => 54.99,
            'description' => 'neki kratak description za REEBOK PATIKE ZPRINT 3D WE',
            'inventory' => 15,
            'weight' => 441,
            'featured' => false,
            'brand_id' => 3,
            'on_sale' => false,
            'new' => false,
            'product_image' => 'blaze-knit-low-puma-sikkerhedssko-ps-l.png'

        ]);
        Product::create([
            'name' => 'REEBOK LES MILLS PATIKE ULTRA 4.0',
            'sku' => 'reebok-Md-897',
            'slug' => 'reebok-les-mills-patike-ultra',
            'details' => 'neki kratak description za REEBOK LES MILLS PATIKE ULTRA 4.0',
            'price' => 70,
            'sale_price' => 64.50,
            'description' => 'neki kratak description za REEBOK LES MILLS PATIKE ULTRA 4.0',
            'weight' => 234,
            'inventory' => 10,
            'featured' => false,
            'brand_id' => 1,
            'on_sale' => true,
            'new' => true,
            'product_image' => 'neodyme-green-low-puma-sikkerhedssko-pm-l.png'
        ]);
        Product::create([
            'name' => 'REEBOK CLASSICS PATIKE ROYAL EC RIDE 2',
            'sku' => 'reebok-md-12',
            'slug' => 'reebok-clasics-patike-royal-ec-ride-2',
            'details' => 'neki kratak description za REEBOK CLASSICS PATIKE ROYAL EC RIDE 2',
            'price' => 69,
            'sale_price' => 59.99,
            'description' => 'neki kratak description za REEBOK CLASSICS PATIKE ROYAL EC RIDE 2',
            'weight' => 430,
            'inventory' => 12,
            'featured' => true,
            'brand_id' => 4,
            'on_sale' => true,
            'new' => true,
            'product_image' => 'blaze-knit-low-puma-sikkerhedssko-ps-l.png'
        ]);
        Product::create([
            'name' => 'REEBOK RUNNING PATIKE PRINT RUN NEXT',
            'slug' => 'redB-nMdi',
            'sku' => 'reebok-running-patike-print-run-next',
            'details' => 'neki kratak description za REEBOK RUNNING PATIKE PRINT RUN NEXT',
            'price' => 90,
            'sale_price' => 84.99,
            'description' => 'neki kratak description za REEBOK RUNNING PATIKE PRINT RUN NEXT',
            'weight' => 334,
            'inventory' => 18,
            'featured' => true,
            'brand_id' => 4,
            'on_sale' => true,
            'new' => true,
            'product_image' => 'neodyme-green-low-puma-sikkerhedssko-pm-l.png'
        ]);
        Product::create([
            'name' => 'REEBOK LES MILLS PATIKE ULTRA 4.0',
            'sku' => 'rej-d2',
            'slug' => 'reebok-les-mills-patike-ultra-4',
            'details' => 'neki kratak description za REEBOK LES MILLS PATIKE ULTRA 4.0',
            'price' => 85,
            'sale_price' => 74.99,
            'description' => 'neki kratak description za REEBOK LES MILLS PATIKE ULTRA 4.0',
            'weight' => 449,
            'inventory' => 19,
            'featured' => true,
            'brand_id' => 4,
            'on_sale' => true,
            'new' => true,
            'product_image' => 'neodyme-green-low-puma-sikkerhedssko-pm-l.png'
        ]);
        Product::create([
            'name' => 'REEBOK HAYASU PATIKE',
            'slug' => 'hayasu-d',
            'sku' => 'reebok-hayasu-patike',
            'details' => 'neki ratak description za REEBOK HAYASU PATIKE',
            'price' => 70,
            'sale_price' => 54.99,
            'description' => 'neki kratak description za REEBOK HAYASU PATIKE',
            'weight' => 340,
            'inventory' => 14,
            'featured' => false,
            'brand_id' => 4,
            'on_sale' => true,
            'new' => true,
            'product_image' => 'sierra-nevada-low-puma-sikkerhedssko-ps-l.png'
        ]);
        Product::create([
            'name' => 'REEBOK MAJICA STUDIO FAVORITES',
            'slug' => 'reebok-UHF4',
            'sku' => 'reebok-majica-studio-favorites',
            'details' => 'neki kratak description za REEBOK MAJICA STUDIO FAVORITES',
            'price' => 25,
            'sale_price' => 19.99,
            'description' => 'neki kratak description za REEBOK MAJICA STUDIO FAVORITES',
            'weight' => 0.5,
            'inventory' => 11,
            'featured' => false,
            'brand_id' => 2,
            'on_sale' => true,
            'new' => true,
            'product_image' => 'elevate-knit-black-low-puma-sikkerhedssko-ps-l.png'
        ]);
        Product::create([
            'name' => 'REEBOK ŽENSKA MAJICA ELLEMENTS LOGO',
            'slug' => 'rBc-JU7',
            'sku' => 'reebok-majica-ellements-t20',
            'details' => 'neki kratak description za REEBOK ŽENSKA MAJICA ELLEMENTS LOGO',
            'price' => 20,
            'sale_price' => 17.99,
            'description' => 'neki kratak description za REEBOK ŽENSKA MAJICA ELLEMENTS LOGO',
            'weight' => 519,
            'inventory' => 15,
            'featured' => false,
            'brand_id' => 3,
            'on_sale' => true,
            'new' => true,
            'product_image' => 'elevate-knit-black-low-puma-sikkerhedssko-ps-l.png'
        ]);
    }
}
