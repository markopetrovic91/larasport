<?php

use App\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $now = Carbon::now()->toDateTimeString();

//         Category::create([
//             'name' => 'Men',
//             'slug' => 'men',
//
//             'children' => [
//                 [
//                     'name' => 'Clothes',
//                     'slug' => 'men-clothes',
//
//                     'children' => [
//                         [ 'name' => 'Sweatshirts' , 'slug' => 'men-sweatshirts'],
//                         [ 'name' => 'T-shirts', 'slug' => 'men-t-shirts' ],
//                         [ 'name' => 'Sweatsuit', 'slug' => 'men-sweatsuit' ],
//                         [ 'name' => 'Shorts', 'slug' => 'men-shorts' ]
//                     ],
//                 ],
//                 [
//                     'name' => 'Footwear',
//                     'slug' => 'men-footwear',
//                     'children' => [
//                         ['name' => 'Sneakers', 'slug' => 'men-sneakers'],
//                         ['name' => 'Shoes', 'slug' => 'men-shoes'],
//                         ['name' => 'Boots', 'slug' => 'men-boots']
//                     ],
//                 ],
//                 [
//                     'name' => 'Accesories',
//                     'slug' => 'men-accesories',
//                     'children' => [
//                         ['name' => 'Socks', 'slug' => 'men-socks'],
//                         ['name' => 'Backpacks', 'slug' => 'men-backpacks'],
//                         ['name' => 'Belts', 'slug' => 'men-belts'],
//                     ],
//                 ],
//             ],
//         ]);

        Category::create([
            'name' => 'Women',
            'slug' => 'women',

            'children' => [
                [
                    'name' => 'Clothes',
                    'slug' => 'women-clothes',

                    'children' => [
                        [ 'name' => 'Sweatshirts' , 'slug' => 'women-sweatshirts'],
                        [ 'name' => 'T-shirts', 'slug' => 'women-t-shirts' ],
                        [ 'name' => 'Sweatsuit', 'slug' => 'women-sweatsuit' ],
                        [ 'name' => 'Shorts', 'slug' => 'women-shorts' ]
                    ],
                ],
                [
                    'name' => 'Footwear',
                    'slug' => 'women-footwear',
                    'children' => [
                        ['name' => 'Sneakers', 'slug' => 'women-sneakers'],
                        ['name' => 'Shoes', 'slug' => 'women-shoes'],
                        ['name' => 'Boots', 'slug' => 'women-boots']
                    ],
                ],
                [
                    'name' => 'Accesories',
                    'slug' => 'women-accesories',
                    'children' => [
                        ['name' => 'Socks', 'slug' => 'women-socks'],
                        ['name' => 'Backpacks', 'slug' => 'women-backpacks'],
                        ['name' => 'Belts', 'slug' => 'women-belts'],
                    ],
                ],
            ],
        ]);
    }
}
