<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'marko',
            'email' => 'markopetrovic91@gmail.com',
            'avatar' => '/images/marko-avatar.jpg',
            'password' => bcrypt('test'),
            'type' => 'admin'

        ]);
        User::create([
            'name' => 'nikola',
            'email' => 'nikola.stanic46@gmail.com',
            'password' => bcrypt('test'),
            'type' => 'member'

        ]);
    }
}
