<?php

use App\Brand;
use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create([
            'name' => 'Adidas',
            'image' => 'https://18.kerulet.ittlakunk.hu/files/ittlakunk/styles/large/public/upload/company/1256/adidas_logo.png',
        ]);
        Brand::create([
            'name' => 'Nike',
            'image' => 'https://media.cdn.gradconnection.com/uploads/ffa97309-df71-4bfa-96f8-b2765128409a-Nike_-_Logo.jpg',
        ]);
        Brand::create([
            'name' => 'Reebok',
            'image' => 'https://pha-cms.s3.amazonaws.com/image/139/square_small.jpg',
        ]);
        Brand::create([
            'name' => 'Puma',
            'image' => 'https://www.knijff.com/markmatters/wp-content/uploads/2017/05/puma_logo-300x300.jpg',
        ]);
    }
}
