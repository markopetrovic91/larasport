<?php

use App\Coupon;
use App\FixedValueCoupon;
use App\PercentOffCoupon;
use Illuminate\Database\Seeder;

class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fixed = FixedValueCoupon::create([
            'value' => 30
        ]);

        $percent = PercentOffCoupon::create([
            'percent_off' => 25
        ]);

        Coupon::create([
           'code' => 'abc123',
            'coupon_type' => 'App\FixedValueCoupon',
            'coupon_id' => $fixed->id
        ]);
        Coupon::create([
            'code' => 'def456',
            'coupon_type' => 'App\PercentOffCoupon',
            'coupon_id' => $percent->id
        ]);
    }
}
