/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');
import Axios from 'axios';
window.Vue = require('vue');


// import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
// import locale from 'element-ui/lib/locale/lang/en';
// Vue.use(ElementUI, {locale});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('modal', require('./components/Modal.vue'));
// Vue.component('multi', require('./components/Multiselect.vue'));
// Vue.component('sidebar', require('./components/Sidebar.vue'));


const dash = new Vue({
    el: '#admin-dashboard',
    data: {
        products: [],
        pro: [1, 2, 3],
        selected: undefined,
        showModal: false,
        dataFetched: false,
        rows: [],

    },
    methods: {
        showModalFunc(value) {
            this.showModal = true;
            this.fetchData(value);
        },
        closeModal() {
            this.showModal = false;
            this.dataFetched = false;
        },
        fetchData(value) {
            Axios.get('/api/products/basedoncategory/', {
                    ohoj: this.pro
                })
                .then(response => {

                    this.products = response.data;
                    this.dataFetched = true;
                }).catch(error => {
                    console.log(error.response);
                });
        },

    }
});

const suben = document.getElementsByClassName('sub-menu');
var arr = Array.from(suben);
arr.forEach(function (element) {
    element.addEventListener('click', function () {
        removeActiveClass();
        element.classList.toggle('selected');
    });
});

function removeActiveClass() {
    arr.forEach(function (el) {
        el.classList.remove('selected');
    })
}

function checkHiddenInput() {
    $hidden = $('.hidden-input input[type="checkbox"]');
    $($hidden).change(function () {
        if (this.checked) {
            console.log('chekirano');
        } else {
            console.log('nije cekirano');
        }
    });
}
const hiddenChekcbox = document.getElementById('hidden-checkbox');
const appendedInput = document.getElementById('appended-input');
if (hiddenChekcbox) {
    hiddenChekcbox.addEventListener('change', function (event) {
        checkCheckbox();
    });
}

function checkCheckbox() {
    if (hiddenChekcbox.checked) {
        appendedInput.style.display = 'block';
    } else {
        appendedInput.style.display = 'none';
    }
}
if (hiddenChekcbox) {
    checkCheckbox();
}
(function () {
    var num = 1;
    var create_element = document.getElementById('create-input-field');
    create_element.addEventListener('click', function (e) {
        // e.preventDefault();
        var container = document.getElementById("attribute-values");
        var li = document.createElement("LI");
        li.setAttribute('class', 'form-group');
        var input = document.createElement("input");
        var remove = document.createElement("BUTTON");
        input.setAttribute('id', 'value_' + num);
        var inputID = 'value_' + num;
        input.type = "text";
        input.setAttribute("Name", "value[]");
        input.setAttribute("Placeholder", "new value");
        remove.setAttribute('id', 'remove_' + num);
        remove.innerHTML = "remove";
        remove.addEventListener('click', function (e) {
            e.preventDefault();
            let el = document.getElementById(inputID);
            el.remove();
            remove.remove();
        });
        li.appendChild(input);
        li.appendChild(remove);
        container.appendChild(li);
        num++;
    });
})();
tinymce.baseURL = "http://larasport.local//tinymce";
tinymce.baseURI = new tinymce.util.URI(tinymce.baseURL);
tinymce.init({
    selector: 'textarea',
    inline: true
});
// $(document).ready(function(){
//     function createInputElement() {
//         var container = document.getElementById("attribute-values");
//         var input = document.createElement("input");
//         input.type = "text";
//         input.setAttribute("Name", "value_" + num);
//         container.appendChild(input);
//         container.appendChild(document.createElement("br"));
//         // }
//         num++;
//     }
// });