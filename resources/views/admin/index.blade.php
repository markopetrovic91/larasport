@extends('admin.layouts.master')

        @section('content')
            <div class="dash-content col-sm-12">
                <div class="col-sm-3">
                    <div class="card">

                        <div class="card-content left-side">
                            <h4>Total users</h4>
                            <p>{{ $users }}</p>
                        </div>
                        <div class="card-icon right-side"><i class="fas fa-users"></i></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card">

                        <div class="card-content left-side">
                            <h4>Total products</h4>
                            <p>{{ $totalProducts }}</p>
                        </div>
                        <div class="card-icon right-side"><i class="fab fa-product-hunt"></i></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-content left-side">
                            <h4>Total income</h4>
                            <p>{{ formatPrice($totalIncome) }}</p>
                        </div>
                        <div class="card-icon right-side"><i class="fas fa-money-bill-alt"></i></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-content left-side">
                            <h4>Total subscribers</h4>
                            <p>{{ $users }}</p>
                        </div>
                        <div class="card-icon right-side"><i class="fas fa-heart"></i></div>

                    </div>
                </div>
            </div>
        @endsection