@extends('admin.layouts.master', ['subtitle' => 'Edit Product', 'bodyClass' => 'product-edit'])

@section('content')
    <form action="{{ Route('products.update', $product->id) }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
    <section id="product-edit">
    <div class="row">

        <div class="col-sm-8 col-md-9">
        <div class="card card-big">
            <div class="card-body">
                <div class="top-button-actions">
                    <a href="{{ route('products.index') }}" class="btn btn-outline-gray"><i class="fas fa-arrow-left"></i>Back</a>
                    <a href="{{ route('shop.show', $product->slug) }}" target="_blank" class="btn btn-outline-gray"><i class="fas fa-eye"></i>View Product</a>
                </div>
                <h5 class="card-title">Edit Product <strong>{{ $product->name }}</strong></h5>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" placeholder="name" required value="{{ $product->name }}">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">SKU</label>
                                        <input type="text" name="sku" placeholder="sku" required value="{{ $product->sku }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="slug">Slug</label>
                                        <input type="text" name="slug" placeholder="slug" required value="{{ $product->slug }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Regular price</label>
                                        <input type="text" name="price" placeholder="price" required value="{{ $product->price }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" id="appended-input">
                                        <label for="sale_price">Sale price</label>
                                        <input type="text" name="sale_price" id="sale_price" placeholder="sale price" value="<?php if($product->sale_price) echo $product->sale_price; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>On Sale</h5>
                                <label class="switch hidden-input">
                                    <input id="hidden-checkbox" type="checkbox" name="on_sale" <?php if($product->on_sale) echo "checked" ?>>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inventory">Inventory</label>
                                        <input type="number" name="inventory" placeholder="inventory" required value="{{ $product->inventory }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="weight">Weight (Grams)</label>
                                        <input type="text" name="weight" id="weight" placeholder="Weight in grams" value="{{ $product->weight ? $product->weight : 0}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Long description</label>
                                <textarea name="description" id="description" cols="30" rows="10">{{ $product->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">Short description</label>
                                <textarea name="details" id="details" cols="30" rows="10">{{ $product->details }}</textarea>
                            </div>
                        <br><br>
                </div>

            </div>
        <section id="product-attributes">
                <div class="card card-big">
                    <div class="card-body">
                        <h5 class="card-title">ATTRIBUTES</h5>
                        <div class="product-attributes">
                            @if($atts->count() > 0)
                                @foreach($atts as $att)

                                    <div class="col-sm-3">
                                        <h5>{{ $att->name }}</h5>
                                        <select name="{{ str_replace(' ' , '_', strtolower($att->name)) }}[]" id="{{ $att->name }}" multiple="multiple">
                                            @if($proAtts->count() > 0)
                                                @php
                                                    $filtered = $proAtts->where('attribute_id', $att->id);
                                                @endphp
                                                @foreach($att->values()->get() as $attValue)
                                                    <option value="{{ $attValue->id }}"
                                                            @php
                                                                foreach($filtered as $key => $value) {
                                                                    if ($value->id == $attValue->id) {
                                                                        echo 'selected';
                                                                    }
                                                                };
                                                            @endphp
                                                    >{{ $attValue->value }}</option>
                                                @endforeach
                                            @else
                                                @foreach($att->values()->get() as $attValue)
                                                    <option value="{{ $attValue->id }}">{{ $attValue->value }}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>

                                @endforeach

                            @else
                                There are no attributes yet.
                            @endif

                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-sm-4 col-md-3">
            <div class="card card-big">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">

                        <h5 class="card-title">Featured</h5>
                        <div class="form-group">
                            <label class="switch">
                                <input type="checkbox" name="featured" <?php if($product->featured) echo "checked" ?>>
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <h5 class="card-title">New</h5>
                        <div class="form-group">
                            <label class="switch">
                                <input type="checkbox" name="new" <?php if($product->new) echo "checked" ?>>
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <h5 class="card-title">Outlet</h5>
                        <div class="form-group">
                            <label class="switch">
                                <input type="checkbox" name="outlet" <?php if($product->outlet) echo "checked" ?>>
                                <span class="slider round"></span>
                            </label>
                        </div>

                        </div>
                        <div class="col-sm-6">
                            <h5 class="card-title">Published At:</h5>
                            <span>{{ $product->created_at }}</span>
                            <h5 class="card-title">Updated At:</h5>
                            <span>{{ $product->created_at }}</span>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-big">
                <div class="card-body">
                    <div class="product-categories">
                    <h4 class="card-title">Product Categories</h4>
                    <div class="form-group">

                        @forelse ($categories as $cat)
                                <div class="root-level">
                                <input id="cat_{{ $cat->id }}" type="checkbox" name="cats[]" value="{{ $cat->id }}"
                                <?php
                                    foreach ($proCats as $pc){
                                        if($cat->id == $pc->id){
                                            echo 'checked';
                                        }
                                    }
                                ?>>
                                <label for="cat_{{ $cat->id }}">{{ $cat->name }}</label>

                            @foreach($cat->children as $ca)
                                    <div class="first-level">
                                    <input id="cat_{{ $ca->id }}" type="checkbox" name="cats[]" value="{{ $ca->id }}"
                                    <?php
                                        foreach ($proCats as $pc){
                                            if($ca->id == $pc->id){
                                                echo 'checked';
                                            }
                                        }
                                    ?>>
                                    <label for="cat_{{ $ca->id }}">{{ $ca->name }}</label>

                                @foreach($ca->children as $lastChild)
                                        <div class="second-level">
                                        <input id="cat_{{ $lastChild->id }}" type="checkbox" name="cats[]" value="{{ $lastChild->id }}"
                                        <?php
                                            foreach ($proCats as $pc){
                                                if($lastChild->id == $pc->id){
                                                    echo 'checked';
                                                }
                                            }
                                            ?>>
                                        <label for="cat_{{ $lastChild->id }}">{{ $lastChild->name }}</label>
                                        </div>
                                @endforeach
                                    </div>
                            @endforeach
                            </div>
                        @empty

                            There are no categories yet.

                        @endforelse
                    </div>
                    </div>
                </div>
            </div>
            <div class="card card-big">
                <div class="card-body">
                    <h4 class="card-title">Product Image</h4>
                    <div class="form-group">

                        <div class="image-preview" id="image-preview">
                            <img src="{{ getProductImage($product->product_image) }}" id="profile-img-tag"/>
                            <span id="remove-image" class="remove-image showed">remove</span>
                        </div>
                        <div id="file-choose" class="hide">
                            <label for="product_image">Choose image:</label>
                            <input type="file" name="product_image" id="product_image">
                        </div>

                    </div>
                </div>
            </div>
            <div class="card card-big">
                <div class="card-body">
                    <h4 class="card-title">Product Brands</h4>
                    <ul>
                    @foreach($brands as $brand)
                    <li>
                    <div class="form-group">
                        <input id="brand_{{ $brand->id }}" type="radio" name="brand_id" value="{{ $brand->id }}" <?php if($brand->id == $product->brand_id) echo 'checked';?>>
                        <label for="brand_{{ $brand->id }}">{{ $brand->name }}</label>
                    </div>
                    </li>
                    @endforeach
                    </ul>
                </div>
            </div>

        </div>

    </div>

    </section>

</form>
    <section id="product-reviews">
        <div class="card card-big">
            <div class="card-body">
                <h5 class="card-title">REVIEWS</h5>
                <div class="product-reviews">
                    @if($revs->count() > 0)
                        <table class="table list-reviews">
                            <thead>
                            <th>Rating</th>
                            <th>Nickname</th>
                            <th>Summary</th>
                            <th>Approved</th>
                            <th>Actions</th>
                            </thead>
                            <tbody>
                            @foreach($revs as $rev)
                                <tr>
                                    <td>
                                        <div class="review-ratings">
                                            <div class="rating-summary">
                                                <span class="rating-label"><span>Rating</span></span>
                                                <div class="star-rating" title="{{ ($rev->rating)/5 * 100 }}%">
                                                    <div class="back-stars">
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <div class="front-stars" style="width: {{ ($rev->rating)/5 * 100 }}%">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<span>{{ $rev->rating}}</span>--}}
                                    </td>
                                    <td><span>{{ $rev->nickname }}</span></td>
                                    <td><span>{{ $rev->summary }}</span></td>
                                    <td>
                                        @if($rev->approved)
                                            <span>Yes</span>
                                        @else
                                            <span class="box not-approved">No</span>
                                        @endif
                                    </td>

                                    <td class="table-actions">
                                        <a href="{{ route('reviews.edit', $rev->id) }}" class="btn-sm"><i class="fas fa-edit"></i></a>
                                        <form action="{{ route('reviews.destroy', $rev->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    @else
                        There are no reviews yet.
                    @endif

                </div>
            </div>
        </div>
    </section>

@endsection
@section('jsfooter')
        <script src="{{ asset('/js/admin/tinymce/tinymce.min.js') }}"></script>
        <script>tinymce.init({ selector:'textarea' });</script>
<script>
    const removeClick = document.getElementById('remove-image');
    const fileField = document.getElementById('file-choose');
    const imagePreview = document.getElementById('image-preview');

    removeClick.addEventListener('click', function () {
        fileField.classList.remove('hide');
        imagePreview.classList.add('hide');
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                imagePreview.classList.remove('hide');
                fileField.classList.add('hide');
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#product_image").change(function(){
        readURL(this);
    });
</script>
@endsection