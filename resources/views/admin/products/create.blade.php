@extends('admin.layouts.master', ['subtitle' => 'Create New Product', 'bodyClass' => 'product-create'])

@section('content')
    <form action="{{ Route('products.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <section id="product-create">
            <div class="row">

                <div class="col-sm-8 col-md-9">
                    <div class="card card-big">
                        <div class="card-body">
                            <div class="top-button-actions">
                                <a href="{{ route('products.index') }}" class="btn btn-outline-gray"><i class="fas fa-arrow-left"></i>Back</a>
                            </div>
                            <h5 class="card-title">Create Product</h5>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" placeholder="name" required value="{{ old('name') }}">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">SKU</label>
                                        <input type="text" name="sku" placeholder="sku" required value="{{ old('sku') }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="slug">Slug</label>
                                        <input type="text" name="slug" placeholder="slug" required value="{{ old('slug') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Regular price</label>
                                        <input type="text" name="price" placeholder="price" required value="{{ old('price') }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group" id="appended-input">
                                        <label for="sale_price">Sale price</label>
                                        <input type="text" name="sale_price" id="sale_price" placeholder="sale price" value="{{ old('price') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>On Sale</h5>
                                <label class="switch hidden-input">
                                    <input id="hidden-checkbox" type="checkbox" name="on_sale">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inventory">Inventory</label>
                                        <input type="number" name="inventory" placeholder="inventory" required value="{{ old('inventory') }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="weight">Weight (Grams)</label>
                                        <input type="text" name="weight" id="weight" placeholder="Weight in grams" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">Long description</label>
                                <textarea name="description" id="description" cols="30" rows="10">{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="name">Short description</label>
                                <textarea name="details" id="details" cols="30" rows="10">{{ old('details') }}</textarea>
                            </div>
                            <br><br>
                        </div>

                    </div>
                    <section id="product-attributes">
                        <div class="card card-big">
                            <div class="card-body">
                                <h5 class="card-title">ATTRIBUTES</h5>
                                <div class="product-attributes">
                                    @if($atts->count() > 0)
                                        @foreach($atts as $att)

                                            <div class="col-sm-3">
                                                <h5>{{ $att->name }}</h5>
                                                <select name="{{ strtolower($att->name) }}[]" id="{{ $att->name }}" multiple="multiple">
                                                    @foreach($att->values()->get() as $attValue)
                                                        <option value="{{ $attValue->id }}">{{ $attValue->value }}</option>
                                                    @endforeach

                                                </select>
                                            </div>

                                        @endforeach

                                    @else
                                        There are no attributes yet.
                                    @endif

                                </div>
                            </div>
                        </div>
                    </section>

                </div>
                <div class="col-sm-4 col-md-3">
                    <div class="card card-big">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">

                                    <h5 class="card-title">Featured</h5>
                                    <div class="form-group">
                                        <label class="switch">
                                            <input type="checkbox" name="featured" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <h5 class="card-title">New</h5>
                                    <div class="form-group">
                                        <label class="switch">
                                            <input type="checkbox" name="new" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <h5 class="card-title">Outlet</h5>
                                    <div class="form-group">
                                        <label class="switch">
                                            <input type="checkbox" name="outlet">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <h5 class="card-title">Active</h5>
                                    <div class="form-group">
                                        <label class="switch">
                                            <input type="checkbox" name="active" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Create</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-big">
                        <div class="card-body">
                            <div class="product-categories">
                                <h4 class="card-title">Product Categories</h4>
                                <div class="form-group">

                                    @forelse ($categories as $cat)
                                        <div class="root-level">
                                            <input id="cat_{{ $cat->id }}" type="checkbox" name="cats[]" value="{{ $cat->id }}">
                                            <label for="cat_{{ $cat->id }}">{{ $cat->name }}</label>

                                            @foreach($cat->children as $ca)
                                                <div class="first-level">
                                                    <input id="cat_{{ $ca->id }}" type="checkbox" name="cats[]" value="{{ $ca->id }}">
                                                    <label for="cat_{{ $ca->id }}">{{ $ca->name }}</label>

                                                    @foreach($ca->children as $lastChild)
                                                        <div class="second-level">
                                                            <input id="cat_{{ $lastChild->id }}" type="checkbox" name="cats[]" value="{{ $lastChild->id }}">
                                                            <label for="cat_{{ $lastChild->id }}">{{ $lastChild->name }}</label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    @empty

                                        There are no categories yet.

                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-big">
                        <div class="card-body">
                            <h4 class="card-title">Product Image</h4>
                            <div class="form-group ">

                                <div id="file-choose">
                                    <label for="product_image">Choose image:</label>
                                    <input type="file" name="product_image" id="product_image" required>
                                </div>
                                <div class="image-preview hide" id="image-preview">
                                    <img src="" id="profile-img-tag"/>
                                    <span id="remove-image" class="remove-image showed">remove</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="card card-big">
                        <div class="card-body">
                            <h4 class="card-title">Product Brands</h4>
                            <ul>
                                @foreach($brands as $brand)
                                    <li>
                                        <div class="form-group">
                                            <input id="brand_{{ $brand->id }}" type="radio" name="brand_id" value="{{ $brand->id }}">
                                            <label for="brand_{{ $brand->id }}">{{ $brand->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>

            </div>

        </section>

    </form>
@endsection
@section('jsheader')
    {{--<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">--}}
@endsection
@section('jsfooter')
            <script src="{{ asset('/js/admin/tinymce/tinymce.min.js') }}"></script>
{{--            <script src="{{ asset('/js/admin/dropzone.js') }}"></script>--}}
    {{--<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>--}}
    <script>tinymce.init({ selector:'textarea' });</script>
            <script>
                const removeClick = document.getElementById('remove-image');
                const fileField = document.getElementById('file-choose');
                const imagePreview = document.getElementById('image-preview');

                removeClick.addEventListener('click', function () {
                    fileField.classList.remove('hide');
                    imagePreview.classList.add('hide');
                });

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            imagePreview.classList.remove('hide');
                            fileField.classList.add('hide');
                            $('#profile-img-tag').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#product_image").change(function(){
                    readURL(this);
                });
            </script>

@endsection