@extends('admin.layouts.master', ['subtitle' => 'All Products'])

@section('content')
    <section id="products-list">
        <div class="card card-big">
            <a href="{{ route('products.create') }}" class="btn btn-primary btn-top"><i class="fas fa-plus"></i>New Product</a>
            <div class="card-body">
                {{--<h5 class="card-title">Products</h5>--}}
                <div class="table-responsive">
                    @if($products->count())
                        <table class="table center-aligned-table list">
                            <thead>
                            <tr>
                                <th>Thumb</th>
                                <th>Name</th>
                                <th>SKU</th>
                                <th>Stock</th>
                                <th>Sale</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($products as $product)
                                <tr>
                                    <td class="product-thumb">
                                        <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">
                                    </td>
                                    <td><span>{{ $product->name }}</span></td>
                                    <td><span>{{ $product->sku }}</span></td>
                                    <td class="stock">

                                            @if($product->inventory > 0)
                                                @if($product->inventory > 4)
                                                <span class="box green">In stock</span>
                                                @else
                                                    <span class="box orange">Low stock</span>
                                                @endif
                                            @else
                                                <span class="box red">Out of stock </span>
                                            @endif

                                    </td>
                                    <td>
                                        @if($product->on_sale)
                                            <span class="box red">Sale</span>
                                        @else
                                            <span>No</span>
                                        @endif
                                    </td>
                                    <td><span class="product-price">{{ formatPrice($product->price) }}</span></td>
                                    <td class="table-actions">
                                        <a href="{{ route('shop.show', $product->slug) }}" class="btn-sm btn-view" target="_blank"><i class="fas fa-eye"></i></a>
                                        <a href="{{ route('products.edit', $product->id) }}" class="btn-sm"><i class="fas fa-edit"></i></a>
                                        <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" onclick="return confirm('Are you sure?')"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty



                            @endforelse
                            </tbody>
                        </table>
                    @else
                        There is no products yet.
                    @endif
                </div>
                <div class="product-pagination">
                    {{ $products->links() }}
                </div>
            </div>

        </div>
    </section>

@endsection