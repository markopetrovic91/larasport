@extends('admin.layouts.master', ['subtitle' => 'Categories'])

@section('content')
<section id="product-list">
    <div class="card card-big">
        <div class="card-body">
            <h5 class="card-title">List of categories</h5>
            <div class="table-responsive">

                <table class="table center-aligned-table list-categories">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>No. of products</th>
                        <th>Slug</th>
                        <th>Depth</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php
                    $i = 1;
                    @endphp
                    @forelse ($categories as $cat)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $cat->name }}</td>
                            <td>{{ adminGetProductNumber($cat->id) }}</td>
                            <td>{{ $cat->slug }}</td>
                            <td><span class="badge badge-blue">Root</span></td>
                            <td class="table-actions">
                                <a href="{{ route('categories.edit', $cat->id) }}" class="btn-outline btn-sm"><i class="fas fa-edit"></i></a>
                                <form action="{{ route('categories.destroy', $cat->id)}}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    <button type="submit" onclick="return confirm('Are you sure for this action? All child categories will be deleted also.')"><i class="fas fa-trash-alt"></i></button>
                                    </form>
                            </td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                        @foreach($cat->children as $ca)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>- {{ $ca->name }}</td>
                                <td>{{ adminGetProductNumber($ca->id) }}</td>
                                <td>{{ $ca->slug }}</td>
                                <td><span>1st</span></td>
                                <td class="table-actions">
                                    <a href="{{ route('categories.edit', $ca->id) }}" class="btn-outline btn-sm"><i class="fas fa-edit"></i></a>
                                    <form action="{{ route('categories.destroy', $ca->id)}}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    <button type="submit" onclick="return confirm('Are you sure for this action? All child categories will be deleted also.')"><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @php
                                $i++;
                            @endphp
                            @foreach($ca->children as $lastChild)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>- - {{ $lastChild->name }}</td>
                                    <td>{{ adminGetProductNumber($lastChild->id) }}</td>
                                    <td>{{ $lastChild->slug }}</td>
                                    <td><span>2nd</span></td>
                                    <td class="table-actions">
                                        <a href="{{ route('categories.edit', $lastChild->id) }}" class="btn-outline btn-sm"><i class="fas fa-edit"></i></a>
                                        <form action="{{ route('categories.destroy', $lastChild->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        <button type="submit" onclick="return confirm('Are you sure for this action? All child categories will be deleted also.')"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        @endforeach
                    @empty

                        There is no categories yet.

                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

@endsection