@extends('admin.layouts.master', ['subtitle'=>'Edit category', 'bodyClass' => 'edit-category'])

@section('content')
        <div class="card card-big">
            <div class="card-body">
                <div class="top-button-actions">
                    <a href="{{ route('categories.index') }}" class="btn btn-outline-gray"><i class="fas fa-arrow-left"></i>Back</a>
                </div>
                <h5 class="card-title">Edit category: {{ $category->name }}</h5>
                <div class="col-sm-6">
                    <form action="{{ Route('categories.update', $category->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="name">Category Name</label>
                            <input type="text" name="name" placeholder="name" value="{{ $category->name }}">
                        </div>
                        <div class="form-group">
                            <label for="slug">Category slug</label>
                            <input type="text" name="slug" placeholder="slug" value="{{ $category->slug }}">
                        </div>
                        <div class="form-group">
                            <label for="parent_id">Parent category</label>
                            <select type="text" name="parent_id" id="select-id" disabled>

                                    @if(!$category->parent_id)
                                        <option value="" selected>{{$category->name}} is already root category</option>
                                    @endif
                                @foreach($categories as $cat)
                                {{-- remove current category from the list --}}
                                    @if($cat->id != $category->id)
                                            <option value="{{ $cat->id }}"
                                            @if($cat->id == $category->parent_id)
                                                selected="selected"
                                            @endif
                                            >{{ $cat->name }} [{{ $cat->slug }}]</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <input class="btn btn-primary" type="submit" value="Update" name="submit">
                    </form>
                    <form action="{{ route('categories.destroy', $category->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-outline btn-danger" onclick="return confirm('Are you sure?')"><i class="fas fa-trash-alt"></i> Remove</button>
                    </form>

                </div>
            </div>
        </div>
        <div class="card card-big">
            <div class="card-body">
                <h5 class="card-title">Category products</h5>
                <div class="table-responsive">
                    @if(count($products) > 0)

                    <table class="table center-aligned-table list">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumb</th>
                            <th>Name</th>
                            <th>SKU</th>
                            <th>Price</th>
                            <th>Remove</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($products as $pro)
                            <tr>
                                <td>{{ $i }}</td>
                                <td class="product-thumb">
                                    <img src="{{ getProductImage($pro->product_image) }}" alt="{{ $pro->name}}">
                                </td>
                                <td>{{ $pro->name }}</td>
                                <td>{{ $pro->sku }}</td>
                                <td>{{ formatPrice($pro->price) }}</td>
                                
                                <td class="table-actions">
                                    <form action="{{ route('category.product.remove', ['proId'=> $pro->id, 'catId' => $category->id]) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="" onclick="return confirm('Are you sure?')"><i class="fas fa-trash-alt"></i><span>remove</span></button>
                                        
                                    </form>
                                </td>
                            </tr>
                            @php
                                $i++;
                            @endphp

                            @endforeach

                        </tbody>
                    </table>
                    @else
                        <h3 class="no-items">There are no products in this category.</h3>
                    @endif

                </div>
            </div>
        </div>
    <div class="add-new-product">
        {{-- <button id="show-modal" class="btn btn-primary" @click="showModalFunc(11)"><i class="fas fa-plus"></i> Add New Product</button>
        <!-- use the modal component, pass in the prop -->
        <modal v-if="showModal" @close="closeModal" :products="products">

        </modal> --}}
    </div>

@endsection
