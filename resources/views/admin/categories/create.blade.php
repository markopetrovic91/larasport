@extends('admin.layouts.master')

@section('content')
    <section id="attribute-list">
        <div class="card card-big">
            <div class="card-body">
            <h5 class="cart-title">Add New Category</h5>
            <div class="row">
                <div class="col-sm-4">
                    <form action="{{ Route('categories.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Category Name</label>
                            <input type="text" name="name" placeholder="name" required value="{{ old('name')}}">
                        </div>
                        <div class="form-group">
                                <label for="name">Category Slug</label>
                                <input type="text" name="slug" placeholder="slug" required value="{{ old('slug')}}">
                            </div>
                        <div class="form-group">
                            <label for="name">Category parent</label>
                            <select name="parent_id" id="parent_id">
                                <option value="root">No parent (root)</option>
                                @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }} ({{ $category->slug }})</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary" type="submit" name="submit"><i class="fas fa-save"></i>Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection