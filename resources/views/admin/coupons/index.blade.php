@extends('admin.layouts.master', ['subtitle' => 'Coupons', 'bodyClass' => 'coupons'])

@section('content')
    <section id="orders-list">
        <div class="card card-big">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h5 class="card-title bottom-line">All Coupons</h5>
                        <div class="table-responsive">
                        @if($coupons->count())
                            <table class="table center-aligned-table list without-border">
                                    <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>Code</th>
                                                <th>Type</th>
                                                <th>Value</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse ($coupons as $cou)
                                                <tr>
                                                    <td>{{ $cou->id }}</td>
                                                    <td><span>{{ $cou->code }}</span></td>
                                                    <td>
                                                        @if($cou->coupon_type == 'App\FixedValueCoupon')
                                                            <span>Fixed value ($)</span>
                                                        @else
                                                            <span>Percentage (%) </span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $cou->couponValue() }}
                                                    </td>
                                                    <td class="table-actions">
{{--                                                            <a href="{{ route('coupons.edit', $cou->id) }}" class="btn-sm"><i class="fas fa-edit"></i></a>--}}
                                                        <form action="{{ route('coupons.destroy', $cou->id) }}" method="POST">
                                                            {{ csrf_field() }}
                                                            {{ method_field('DELETE') }}
                                                            <button type="submit" onclick="return confirm('Are you sure?')"><i class="fas fa-trash-alt"></i>remove</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @empty
                                                No coupons yet
                                            @endforelse
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                        <h5 class="card-title bottom-line">Create new Coupon</h5>
                        <form action="{{ route('coupons.store') }}" method="POST">
                            {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="code">Coupon code</label>
                                    <input type="text" id="code" name="code" placeholder="code" required>
                                </div>
                                <div class="form-group">
                                    <label for="code">Coupon type</label>
                                    <select name="type" id="type">
                                        <option value="fixed">Fixed value ($)</option>
                                        <option value="percentage">Percentage (%)</option>
                                    </select>
                                </div>
                            <div class="form-group">
                                <label for="code">Value</label>
                                <input type="text" id="value" name="value" placeholder="value" required>
                            </div>
                                                                
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i>Create</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection