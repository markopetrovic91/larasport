@extends('admin.layouts.master', ['subtitle' => 'Edit Order', 'bodyClass' => 'edit-order'])

@section('content')
    <section id="orders-list">
        <div class="card card-big">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                        <h5 class="card-title bottom-line">Order details</h5>
                        <div class="table-responsive">
                            <table class="table center-aligned-table list without-border">
                                <thead>
                                <tr>
                                    <td class="th">Order #</td>
                                    <td>{{ $order->id }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    @php
                                        $address = $order->address()->get()->first();
                                    @endphp
                                    <td class="th">Bill to Name</td>
                                    <td>{{ $address->first_name }} {{ $address->last_name }}</td>
                                </tr>
                                <tr>
                                    <td class="th">Payment method</td>
                                    <td>
                                        @if($order->payment_gateway == 'on_delivery')
                                            <span>On delivery</span>
                                        @else
                                            <span>Stripe</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="th">Shipped</td>
                                    <td>
                                        @if($order->shipped)
                                            <span>Yes</span>
                                        @else
                                            <span>No</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="th">Status</td>
                                    <td><span class="order-status">{{ $order->status }}</span></td>
                                </tr>
                                @if($order->billing_discount)
                                <tr>
                                    <td class="th">Discount</td>
                                    <td><span class="box red">- {{ formatPrice($order->billing_discount) }}</span></td>
                                </tr>
                                <tr>
                                    <td class="th">Voucher code</td>
                                    <td>["{{ $order->billing_discount_code }}"]</td>
                                </tr>
                                @endif
                                <tr>
                                    <td class="th">Total</td>
                                    <td><span class="order-total-big blue"><strong>{{ formatPrice($order->billing_total) }}</strong></span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <h5 class="card-title bottom-line">Address</h5>
                        <div class="table-responsive">
                            <table class="table center-aligned-table list without-border">
                                <thead>
                                <tr>
                                    <td class="th">Name</td>
                                    <td>{{ $address->first_name}} {{ $address->last_name }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="th">Phone</td>
                                    <td>{{ $address->phone}}</td>
                                </tr>
                                <tr>
                                    <td class="th">Country</td>
                                    <td><span>{{ ucfirst($address->country) }}</span></td>
                                </tr>
                                <tr>
                                    <td class="th">Address</td>
                                    <td>{{ $address->address }}</td>
                                </tr>
                                <tr>
                                    <td class="th">Zip</td>
                                    <td><span>{{ $address->zip}}</span></td>
                                </tr>
                                <tr>
                                    <td class="th">City/Town</td>
                                    <td><span>{{ $address->city }}</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <h5 class="card-title bottom-line">Actions</h5>
                        <form action="{{ route('orders.update', $order->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <h5>Shipped</h5>
                                    <label class="switch hidden-input">
                                        <input id="hidden-checkbox" type="checkbox" name="shipped" <?php if($order->shipped) echo "checked" ?>>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="form-group col-sm-6">
                                    @php
                                        $statuses = ['Pending', 'Shipped', 'Cancelled', 'Refunded', 'Disputed', 'Completed'];
                                    @endphp
                                    <h5>Status</h5>
                                        <select name="status" id="status">
                                            @foreach($statuses as $status)
                                                <option value="{{ $status }}" {{ $status==$order->status ? 'selected' : '' }}>{{ $status }}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary"><i class="fas fa-save"></i>Update</button>
                            </div>
                        </form>

                    </div>
                </div>
                @if($order->additional)
                    <div class="row">
                        <div class="col-sm-4">
                            <h5 class="card-title">Additional info</h5>
                            <span>nesto dodatno sto pise korisnik</span>

                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <section id="orders-list">
        <div class="card card-big">
            <div class="card-body">
                <h5 class="card-title">Order products</h5>
                <div class="table-responsive">
                    @if($pro->count())
                        <table class="table center-aligned-table list">
                            <thead>
                            <tr>
                                <th>Thumb</th>
                                <th>Name</th>
                                <th>SKU</th>
                                <th>Weight</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Attributes</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($pro as $product)
                                <tr>
                                    <td class="product-thumb">
                                        <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">
                                    </td>
                                    <td><span>{{ $product->name }}</span></td>
                                    <td><span>{{ $product->sku }}</span></td>
                                    <td class="stock">
                                        {{ $product->weight }}g
                                    </td>
                                    <td>
                                        <span class="quantity">
                                            {{ $product->pivot->quantity }}
                                        </span>

                                    </td>
                                    <td>
                                        @if($product->on_sale)
                                            <div class="sale_price_text">
                                                <span class="box red ">Sale price</span>
                                            </div>
                                        <span class="product-price">{{ formatPrice($product->sale_price) }}</span>
                                        @else
                                        <span class="product-price">{{ formatPrice($product->price) }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @php
                                            $attr = explode(',', $product->pivot->attributes);
                                        @endphp
                                        @if($attr)
                                            @foreach($attr as $att)
                                                <div><span class="attribute-list">{{ $att }}</span></div>
                                            @endforeach
                                        @endif


                                    </td>
                                </tr>
                            @empty



                            @endforelse
                            </tbody>
                        </table>
                    @else
                        There is no products yet.
                    @endif
                </div>
            </div>
        </div>
    </section>

@endsection