@extends('admin.layouts.master', ['subtitle' => 'All Orders'])

@section('content')
    <section id="orders-list">
        <div class="card card-big">
            <div class="card-body">
                {{--<h5 class="card-title">Products</h5>--}}
                <div class="table-responsive">
                    @if($orders->count())
                        <table class="table center-aligned-table list">
                            <thead>
                            <tr>
                                <th>Order #</th>
                                <th>Bill to Name</th>
                                <th>Payment method</th>
                                <th>Shipped</th>
                                <th>Status</th>
                                <th>Total</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($orders as $order)
                                @php
                                $address = $order->address()->get()->first();
                                @endphp
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td><span>{{ $address->first_name }} {{ $address->last_name }}</span></td>
                                    <td>
                                        @if($order->payment_gateway == 'stripe')
                                            <span>Stripe</span>
                                        @else
                                            <span>On delivery</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($order->shipped)
                                            <span class="box green">Yes</span>
                                        @else
                                            <span class="box red">No</span>
                                        @endif
                                    </td>
                                    <td>
                                        <span>{{ $order->status }}</span>
                                    </td>
                                    <td>
                                        <span>{{ formatPrice($order->billing_total) }}</span>
                                    </td>
                                    <td class="table-actions">
                                        <a href="{{ route('orders.edit', $order->id) }}" class="btn-sm"><i class="fas fa-edit"></i></a>
                                        <form action="{{ route('orders.destroy', $order->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" onclick="return confirm('Are you sure?')"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty



                            @endforelse
                            </tbody>
                        </table>
                    @else
                        There is no products yet.
                    @endif
                </div>
                <div class="product-pagination">
                    {{ $orders->links() }}
                </div>
            </div>

        </div>
    </section>

@endsection