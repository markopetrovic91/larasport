@extends('admin.layouts.master')

@section('content')


    <div class="row">
        <div class="container">
            <div class="col-sm-3"></div>
            <div class="col sm-6">
                <h3>Add Product</h3>
            <form action="{{ Route('products.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" placeholder="name">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" name="description" placeholder="description">css
                </div>
                <div class="form-group">
                    <label for="size">Size</label>
                    <select name="size" id="size">
                        <option value="small">Small</option>
                        <option value="medium">Medium</option>
                        <option value="large">Large</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="text" name="price" class="file-field">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="image" class="file-field">
                </div>
                <div class="form-group">
                    <label for="category_id">Select Category</label>
                    <select name="category_id" id="category_id">
                        <?php
                        foreach($categories as $key => $category){

                            ?>

                            <option value="<?php echo $key ?>"><?php echo $category ?></option>
                        <?php
                            }
                        ?>
                    </select>

                </div>
                <input class="btn btn-info" type="submit" value="Submit" name="submit">
            </form>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>
@endsection