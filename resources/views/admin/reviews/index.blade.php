@extends('admin.layouts.master', ['subtitle' => 'All Reviews'])

@section('content')
    <section id="reviews-list">
        <div class="card card-big">
            <div class="card-body">
                <a class="btn btn-outline-gray" href="{{ route('reviews.approved', 1) }}">Approved</a>
                <a class="btn btn-outline-gray" href="{{ route('reviews.approved', 0) }}">Not Approved</a>
                <div class="table-responsive">
                    @if($reviews->count())
                        <table class="table center-aligned-table list">
                            <thead>
                            <tr>
                                <th>Thumb</th>
                                <th>Product Name</th>
                                <th>Rating</th>
                                <th>Nickname</th>
                                <th>summary</th>
                                <th>Approved</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($reviews as $rev)
                                <tr>
                                    <td class="product-thumb">
                                    <img src="{{ getProductImage($rev->product()->get()->first()->product_image) }}" alt="{{ $rev->product()->get()->first()->name }}">
                                    </td>
                                    <td><span>{{ $rev->product()->get()->first()->name }}</span></td>
                                    <td>
                                        <div class="review-ratings">
                                            <div class="rating-summary">
                                                <div class="star-rating" title="{{ ($rev->rating)/5 * 100 }}%">
                                                    <div class="back-stars">
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <div class="front-stars" style="width: {{ ($rev->rating)/5 * 100 }}%">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td><span>{{ $rev->nickname }}</span></td>
                                    <td><span>{{ $rev->summary }}</span></td>
                                    <td>
                                        @if($rev->approved)
                                            <span class="smiley green"><i class="fas fa-smile"></i></span>
                                        @else
                                            <span class="smiley red"><i class="fas fa-frown"></i></span>
                                        @endif
                                    </td>

                                    <td class="table-actions">
                                        <a href="{{ route('reviews.edit', $rev->id) }}" class="btn-sm"><i class="fas fa-edit"></i></a>
                                        <form action="{{ route('reviews.destroy', $rev->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" onclick="return confirm('Are you sure?')"><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <br><br>
                        There are no reviews yet.
                    @endif
                </div>
            </div>
        </div>
    </section>

@endsection