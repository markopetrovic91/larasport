@extends('admin.layouts.master', ['subtitle' => 'Edit Review'])

@section('content')
    <section id="reviews-list">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-big">
                    <div class="card-body">
                        <a href="{{ route('reviews.index') }}" class="btn btn-outline-gray"><i class="fas fa-arrow-left"></i>Back</a>
                        <h5>Edit review of {{ $rev->nickname }}</h5>
                        <div class="table-responsive">
                            <table class="table center-aligned-table list">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Nickname</th>
                                    <th>Rating</th>
                                    <th>Summary</th>
                                    <th>Review</th>
                                </tr>
                                </thead>
                                <tbody>
                                <td class="product-thumb">
                                    <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">
                                </td>
                                <td><span>{{ $rev->nickname }}</span></td>
                                <td>
                                    <div class="review-ratings">
                                        <div class="rating-summary">
                                            <div class="star-rating" title="{{ ($rev->rating)/5 * 100 }}%">
                                                <div class="back-stars">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <div class="front-stars" style="width: {{ ($rev->rating)/5 * 100 }}%">
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td><strong>{{ $rev->summary }}</strong></td>
                                <td>{{ $rev->review }}</td>
                                </tbody>
                            </table>
                        </div>
                        <br><br>
                        <form action="{{ route('reviews.update', $rev->id) }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="switch">
                                    <input type="checkbox" name="approved" <?php if($rev->approved) echo "checked" ?>>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection