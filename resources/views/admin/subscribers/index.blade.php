@extends('admin.layouts.master', ['subtitle' => 'Subscribers'])

@section('content')

        <div class="card">
            <div class="card-body">
                <h5 class="card-title">MailChimp subscribers</h5>
                <div class="table-responsive">
                    <table class="table center-aligned-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Email</th>
                                <th>Is member</th>
                                <th>Is Buyer</th>
                                <th>Country</th>
                                {{--<th>Payment Status</th>--}}
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse ($subslist as $sub)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $sub["email_address"] }}</td>
                            <td>{{ $sub["isMember"]["isMember"] }}</td>
                            <td>{{ $sub["isMember"]["isBuyer"]  }}</td>
                            <td>{{ $sub["location"]["country_code"] }}</td>
                            {{--<td>--}}
                                {{--<span class="badge badge-teal">Approved</span>--}}
                            {{--</td>--}}
                            <td class="table-actions">
                                <a href="#" class="btn-sm"><i class="fas fa-edit"></i></a>
                                <a href="#" class=""><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        @empty

                            There are no subscribers yet.

                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


@endsection