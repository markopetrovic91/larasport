@extends('admin.layouts.master', ['subtitle' => 'Edit Attribute', 'bodyClass' => 'attribute-edit'])

@section('content')
    <section id="attribute-edit">
        <div class="card card-big">
            <div class="card-body">
                <a href="{{ route('attributes.index') }}" class="btn btn-outline-gray"><i class="fas fa-arrow-left"></i>Back</a>
                <h5 class="card-title">Edit Attribute {{ $attribute->name }}</h5>
                <div class="row">
                    <div class="col-sm-4">
                        <form action="{{ Route('attributes.update', $attribute->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <label for="name">Attribute Name</label>
                                <input type="text" name="name" placeholder="name" required value="{{ $attribute->name }}">
                            </div>

                            <div class="form-group">
                                <button class="btn btn-ok" type="submit" name="submit"><span><i class="fas fa-save"></i></span>Update Name</button>
                            </div>
                        </form>
                        @php
                            $atts = $attribute->values()->get();
                        @endphp
                        <br><br>
                        <hr>
                        <br>
                    <h5 class="card-title">Add New Value to {{ $attribute->name }} attribute</h5>

                        <form action="{{ Route('attvalue.store') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Attribute Value</label>
                                <input type="text" name="value" placeholder="new value" required>
                            </div>
                            <input type="hidden" value="{{ $attribute->id }}" name="attribute_id">

                            <div class="form-group">
                                <button class="btn btn-primary" type="submit" name="submit"><span><i class="fas fa-save"></i></span>Save value</button>
                            </div>
                        </form>

                    </div>
                    <div class="col-sm-2 col-md-2">

                    </div>
                    <div class="col-sm-4 col-md-3">
                        <table class="table list-attributes">
                            <thead>
                                <th>Name</th>
                                <th>Actons</th>
                            </thead>
                            <tbody>
                            @foreach($atts as $att)
                                <tr>
                                    <td>{{ $att->value }}</td>
                                    <td class="table-actions">
                                        <form action="{{ route('attvalue.destroy', $att->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit"><i class="fas fa-trash-alt"></i>Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>

                    </div>

                </div>
            </div>

        </div>
    </section>

@endsection
@section('jsheader')

@endsection