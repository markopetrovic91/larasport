@extends('admin.layouts.master', ['subtitle' => 'Attributes'])

@section('content')
    <section id="attribute-list">
        <div class="card card-big">
            <div class="card-body">
                <h5 class="card-title">Attributes</h5>
                <div class="table-responsive">

                    @if($attributes->count())
                    <table class="table center-aligned-table list-attributes">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Values</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($attributes as $att)
                            <tr>
                                <td><span>{{ $att->name }}</span></td>
                                <td class="value-items">

                                    @forelse($att->values()->get() as $val)
                                        <span class="value-item">
                                            {{ $val->value }}
                                        </span>
                                    @empty
                                        <span>There is not values for this attribute yet.</span>
                                    @endforelse
                                </td>
                                <td class="table-actions">
                                    <a href="{{ route('attributes.edit', $att->id) }}" class="btn-sm"><i class="fas fa-edit"></i></a>
                                    <form action="{{ route('attributes.destroy', $att->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit"><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty



                        @endforelse
                        </tbody>
                    </table>
                    @else
                        There is no attributes yet.
                    @endif
                </div>
            </div>
            <a href="{{ route('attributes.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i>Create new Attribute</a>
        </div>
    </section>

@endsection