@extends('admin.layouts.master', ['subtitle' => 'Create new Attribute', 'bodyClass' => 'attribute-create'])

@section('content')
    <section id="attribute-create">
        <div class="card card-big">
            <div class="card-body">
                <h5 class="card-title">Attributes</h5>
                <div class="row">
                <div class="col-sm-4">
                    <form action="{{ Route('attributes.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Attribute Name</label>
                            <input type="text" name="name" placeholder="name" required>
                        </div>
                        <div class="form-group">
                            {{--<label>Values:</label>--}}
                        </div>
                            <div id="attribute-values"></div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit" name="submit"><span><i class="fas fa-save"></i></span>Submit</button>
                        </div>
                    </form>
                </div>
                    <div class="col-sm-4">
                        <button id="create-input-field" class="btn btn-outline"><span><i class="fas fa-plus"></i></span>Add new value</button>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection
@section('jsheader')
{{--    <script src="{{ asset('js/admin/header.js') }}"></script>--}}
@endsection