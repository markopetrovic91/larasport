<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Larasport dashboard</title>
    <link rel="stylesheet" href="{{asset('css/admin/admin.css')}}">
    @yield('jsheader')
</head>
<body class="{{ $bodyClass or "page" }}">
<div id="admin-dashboard" class="page-content">
    @if(Session::has('message'))
        <div class="alert alert-info">
            <p>{{ Session::get('message') }}</p>
        </div>
    @endif
    {{--<div class="row">--}}
        @include('admin.layouts.includes.sidenav')
        <div class="display-area">
            @include('admin.layouts.includes.header')
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="content-box-large clearfix">
                @if(session()->has('success_message'))
                    <div class="alert alert-success" role="alert">
                        {{ session()->get('success_message') }}
                    </div>
                @endif
                @yield('content')
            </div>
        </div><!--/Display area after sidenav-->
    {{--</div>--}}
    

</div><!--/Page Content-->

<script src="/js/admin/admin.js"></script>
@yield('jsfooter')
</body>
</html>