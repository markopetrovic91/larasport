<div class="header">
    <div class="row">
        <div class="container-fluid">
            <div class="section-subtitle">
                <h3>{{ $subtitle or "Dashboard" }}</h3>
            </div>
            <nav class="navbar-right">
                <ul class="nav navbar-nav">
                    <li>
                        <div class="avatar"><img src="{{ auth()->user()->avatar }}" alt="" class="avatar"></div></li>
                    <li>{{ Auth::user()->name }}</li>
                    <li><a class="btn btn-primary" href="{{ route('admin.logout') }}"><i class="fas fa-sign-out-alt"></i>Logout</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>