<aside class="sidebar">
  @php
    $curl = url()->current();
  @endphp
  <div class="sidebar-head">
    <div id="logo">
      <a href="/admin"><img src="/images/logo-dash-light.png" alt="Logo"></a>
    </div>
  </div>
    <div id="leftside-navigation" class="nano">
  {{--<div>--}}
      <ul class="nano-content">
        <li class="<?php if(strpos($curl, 'index') !== false) echo "active" ?>">
          <a href="{{ Route('admin') }}">
              <i class="fas fa-home"></i><span>Dashboard</span>
          </a>
        </li>
        <li class="sub-menu <?php if(strpos($curl, 'products') !== false) echo "active selected" ?>">
          <a href="javascript:void(0);"><i class="fab fa-product-hunt"></i>
              <span>Products</span><i class="arrow fa fa-angle-right pull-right"></i>
          </a>
          <ul>
            <li><a href="{{Route('products.index')}}">All Products</a></li>
            <li><a href="{{Route('products.create')}}">Add New Product</a></li>

          </ul>
        </li>
        <li class="sub-menu <?php if(strpos($curl, 'categories') !== false) echo "active selected" ?>">
          <a href="javascript:void(0);"><i class="fa fa-table"></i><span>Categories</span><i class="arrow fa fa-angle-right pull-right"></i></a>
          <ul>
              <li><a href="{{ Route('categories.index') }}">All Categories</a></li>
              <li><a href="{{ Route('categories.create') }}">Add New Category</a></li>
          </ul>
        </li>
        <li class="sub-menu <?php if(strpos($curl, 'attributes') !== false) echo "active selected" ?>">
          <a href="javascript:void(0);"><i class="fa fa fa-tasks"></i><span>Attributes</span><i class="arrow fa fa-angle-right pull-right"></i></a>
          <ul>
            <li><a href="{{ route('attributes.index') }}">All Attributes</a>
            </li>
            <li>
              <a class="<?php if(strpos($curl, 'attributes/create') !== false) echo "active" ?>" href="{{ route('attributes.create') }}">Add New Attribute</a>
            </li>
          </ul>

        </li>
        <li class="sub-menu <?php if(strpos($curl, 'reviews') !== false) echo "active selected" ?>">
          <a href="javascript:void(0);"><i class="fa fa-envelope"></i><span>Reviews</span><i class="arrow fa fa-angle-right pull-right"></i></a>
          <ul>
            <li><a href="{{Route('reviews.index')}}">All Reviews</a></li>
            <li><a href="{{Route('reviews.approved', 1)}}">Approved</a></li>
            <li><a href="{{Route('reviews.approved', 0)}}">Not Approved</a></li>
          </ul>
        </li>
        <li class="sub-menu <?php if(strpos($curl, 'orders') !== false) echo "active selected" ?>">
          <a href="javascript:void(0);"><i class="fas fa-shopping-cart"></i><span>Sales</span><i class="arrow fa fa-angle-right pull-right"></i></a>
            <ul>
                <li><a href="{{ route('orders.index') }}">All Orders</a></li>
                {{-- <li><a href="{{ route('orders.index') }}">Invoices</a></li>
                <li><a href="{{ route('orders.index') }}">Return requests</a></li>
                <li><a href="{{ route('orders.index') }}">Gift cards</a></li>
                <li><a href="{{ route('orders.index') }}">Shipments</a></li> --}}
            </ul>
        </li>
        <li class="sub-menu">
          <a href="{{ route('coupons.index') }}"><i class="fas fa-tags"></i></i><span>Coupons</span></a>
        </li>
        <li class="sub-menu <?php if(strpos($curl, 'customers') !== false) echo "active selected" ?>">
          <a href="javascript:void(0);"><i class="fas fa-users"></i><span>Customers</span><i class="arrow fa fa-angle-right pull-right"></i></a>
            <ul>
                <li><a href="{{ route('mailchimp.index') }}"><span>Subscribers</span></a></li>
                {{-- <li><a href="{{ route('mailchimp.index') }}"><span>Customer Groups</span></a></li> --}}
                <li><a href="{{ route('mailchimp.index') }}"><span>Now Online</span></a></li>
            </ul>
        </li>
          <li class="sub-menu">
              <a href="{{ route('mailchimp.index') }}"><i class="fas fa-chart-line"></i><span>Reports</span></a>
          </li>
      </ul>

    </div>
  </aside>