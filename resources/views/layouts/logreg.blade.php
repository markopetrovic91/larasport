<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title or 'Larasport Login Page' }}</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('header')
</head>
<body class="{{ $bodyClass or "page" }}">
<header class="header">
    <a href="/" id="logo" class="logo">
        <img src="/images/logo.png" alt="logo">
    </a>
</header>



{{--@include('layouts.includes.header')--}}
<div class="page-wrapper" id="{{  $id or "app" }}">

    @yield('content')
</div>


<footer id="footer">
    <div class="copyright">
        Web design & development © 2018 <a href="https://corellonmedia.com" target="_blank">Corellon Media</a>
    </div>
</footer>
<!-- Scripts -->
@yield('footer')
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
