<header class="header">
    <div class="top-nav">
        <div class="container">
        <ul class="navbar-inline top-nav-left">
            <li><a href="#">FAQ</a></li>
            <li><a href="{{ route('contact.index') }}">Contact</a></li>
            <li><a href="#"><i class="fas fa-phone"></i> 062 84 29 318</a></li>
        </ul>
        <ul class="top-nav-right navbar-inline pull-right">
            @auth
        <li><a href="{{ route('user.wishlist', Auth::user()->id) }}">My Wishlist</a>
                <span class="wishlist_count"></span>
            </li>
            @endauth
            <li><a href="/help">Help</a></li>
                @guest
                    <li><a href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> Log In</a></li>
                    <li><a href="{{ route('register') }}"><i class="fas fa-user"></i> Register</a></li>
                @else
                    <li>
                    <a href="{{ route('user.index', Auth::user()->id) }}">
                            <i class="fas fa-user"></i>
                            {{ Auth::user()->name }}
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            {{ csrf_field() }}
                        </form>
                    </li>
                        
                    
                @endguest
        </ul>
    </div>
    </div>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Logo -->
                <a id="logo" class="logo" href="{{ url('/') }}">
                    <img src="/images/logo.png" alt="logo">
                </a>
                <ul class="navbar-inline nav-bottom-left">
                    <li>
                        <a href="{{ route('shop.index') }}">ALL</a>
                    </li>
                    <li>
                        <a href="{{ route('shop.index', ['category' => 1]) }}" class="search-button">MEN</a>
                    </li>                            
                    <li>
                        <a href="{{ route('shop.index', ['category' => 22]) }}">WOMEN</a>
                    </li>
                    <li>
                        <a href="{{ route('shop.index', ['on_sale' => 'yes']) }}">SALE</a>
                    </li>
                    <li>
                        <a href="{{ route('shop.index', ['new' => 1]) }}">NEW</a>
                    </li>
                    <li>
                        <a href="{{ route('shop.index', ['outlet' => 1]) }}">OUTLET</a>
                    </li>
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav-bottom-right navbar-inline pull-right">
                    <li id="product-search">
                        {{--<a href="#" class="search-button">--}}
                            {{--<i class="fas fa-search"></i>--}}
                        {{--</a>--}}
                        <search></search>
                    </li>                            
                    <li>
                        <a href="{{ route('cart.index') }}"><i class="fas fa-shopping-bag"></i>
                            @if(Cart::instance('default')->count() > 0)
                                <span class="cart-count">{{ Cart::instance('default')->count() }}</span>
                            @endif
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
</header>
@if(session()->has('success_message'))
    <div class="message-modal success-message-modal">
        <i class="fas fa-check-circle"></i>
        <p class="success-msg">
            {{ session()->get('success_message') }}
        </p>
        <a href="#" class="btn button-ok">OK</a>
    </div>
@endif
{{--@if(count($errors) > 0)--}}
    {{--<div class="message-modal error-message-modal">--}}
        {{--<i class="fas fa-exclamation-circle"></i>--}}
        {{--<p class="error-msg">--}}
            {{--{{ session()->get('success_message') }}--}}
        {{--</p>--}}
        {{--<a href="#" class="btn button-ok">OK</a>--}}
    {{--</div>--}}
    {{--<div class="alert alert-danger">--}}
        {{--<ul>--}}
            {{--@foreach($errors->all() as $error)--}}
                {{--<li>{{ $error }}</li>--}}
            {{--@endforeach--}}
        {{--</ul>--}}
    {{--</div>--}}
{{--@endif--}}
