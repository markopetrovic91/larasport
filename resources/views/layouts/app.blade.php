<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title or 'Larasport' }}</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('header')
</head>
<body class="{{ $bodyClass or "page" }}">
    @include('layouts.includes.header')
    <div class="page-wrapper" id="{{  $id or "app" }}">
        @yield('content')
    </div>
    @yield('above-footer')

<footer id="footer">
    <div class="container">
        <div class="col-sm-12">
            <div class="subscription">
                <h2>Subscription</h2>
                <p>By subscribing to this mailing list, you will get regular updates about our new products and discounts.</p>
                <form id="mailchimp-form" action="{{ route('subscribe') }}" method="post">
                    {{ csrf_field() }}
                    <div class="newsletter-form">
                        <input id="newsletterEmail" name="email" type="email" placeholder="Place your email here ..." required>
                        <button type="submit"> Subscribe
                            <svg width="20" height="11" id="arrwIcn" viewBox="0 0 21.5 11.5" width="100%" height="100%"><path d="M13.8,11.5c-0.2,0-0.5-0.1-0.6-0.3c-0.2-0.3-0.2-0.8,0.2-1l6.1-4.4l-5-3.5v2.5c0,0.4-0.3,0.8-0.8,0.8h-13 C0.3,5.5,0,5.2,0,4.8S0.3,4,0.8,4H13V0.8c0-0.3,0.2-0.5,0.4-0.7C13.7,0,14,0,14.2,0.1l7,5c0.2,0.1,0.3,0.4,0.3,0.6s-0.1,0.5-0.3,0.6 l-7,5C14.1,11.5,13.9,11.5,13.8,11.5z"></path></svg>
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="divider"></div>
<div class="container">
    <div class="row">
    <div class="footer-links-wrapper">
        <div class="col-sm-3 footer-links">
            <h3>CUSTOMER SERVICE</h3>
            <ul>
                <li><a href="#">Customer Service</a></li>
                <li><a href="#">Gift Cards</a></li>
                <li><a href="#">Check Gift Card Balance</a></li>
                <li><a href="#">Return Policy</a></li>
            </ul>

        </div>
        <div class="col-sm-3 footer-links">
            <h3>BRANDS</h3>
            <ul>
            <li><a href="{{ route('shop.index', ['brand' => 3]) }}">Reebok</a></li>
                <li><a href="{{ route('shop.index', ['brand' => 2]) }}">Nike</a></li>                
                <li><a href="{{ route('shop.index', ['brand' => 1]) }}">Adidas</a></li>
                <li><a href="{{ route('shop.index', ['brand' => 4]) }}">Puma</a></li>
            </ul>
        </div>
        <div class="col-sm-3 footer-links">
            <h3>FEATURED</h3>
            <ul>
                <li><a href="#">Women</a></li>
                <li><a href="#">Men</a></li>
                <li><a href="#">Oprema</a></li>
                <li><a href="#">Outlet</a></li>
            </ul>
            
        </div>
        <div class="col-sm-3 footer-links">

            <h3>E-SHOP</h3>
            <ul>
                <li><a href="#">About us</a></li>
                <li><a href="/contact">Contact</a></li>
                <li><a href="/terms-of-service/">Terms of service</a></li>
                <li><a href="privacy-policy">Privacy Policy</a></li>
            </ul>
        </div>
    </div>
    </div>
</div>
<div class="container below-footer">
    <div class="copyright">
        Web design & development © 2018 <a href="https://corellonmedia.com" target="_blank">Corellon Media</a>
    </div>

    <div class="social-links">
        <span>Follow Us:</span>
        <a href="#"><i class="fab fa-facebook-f"></i></a>
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
        <a href="#"><i class="fab fa-linkedin-in"></i></a>
    </div>
</div>
</footer>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('footer')

</body>
</html>
