@extends('layouts.app', ['title' => 'Member page', 'bodyClass' => 'member-page', 'id' => 'member-app'])

@section('content')
    <div class="container">
        {{-- <mapps></mapps> --}}
        @php
            $curl = url()->current();
        @endphp
        <div class="member-dash">
            <div class="member-sidebar">
                <ul class="side-items">
                    <li class="side-item <?php if(strpos($curl, 'index') !== false) echo "current" ?>"><a href="{{ route('user.index', Auth::user()->id) }}">My Account</a></li>
                    <li class="side-item <?php if(strpos($curl, 'info') !== false) echo "current" ?>"><a href="{{ route('user.info', Auth::user()->id) }}">Account Information</a></li>
                    <li class="side-item <?php if(strpos($curl, 'addresses') !== false) echo "current" ?>"><a href="{{ route('user.addresses', Auth::user()->id) }}">My Address</a></li>
                    <li class="side-item <?php if(strpos($curl, 'orders') !== false) echo "current" ?>"><a href="{{ route('user.orders', Auth::user()->id) }}">My Orders</a></li>
                    <li class="side-item <?php if(strpos($curl, 'subscriptions') !== false) echo "current" ?>"><a href="{{ route('user.subscriptions', Auth::user()->id) }}">Subscriptions</a></li>
                    <li class="side-item <?php if(strpos($curl, 'wishlist') !== false) echo "current" ?>"><a href="{{ route('user.wishlist', Auth::user()->id) }}">My Wishlist</a></li>
                </ul>
            </div>
            <div class="member-dashcontent">
                {{-- dash index --}}
                @if (strpos($curl, 'index') !== false)
                    <h1>My Dashboard</h1>
                    <div class="block mem-dashboard-orders">
                        <div class="block-title order">
                            <span class="block-title-big">Recent Orders</span>
                            <a class="action view" href="{{ route('user.orders', Auth::user()->id) }}">
                            <span>View All</span>
                            </a>
                        </div>
                            <div class="block-content">
                                @if(count($orders) > 0)
                                <div class="table-wrapper orders-recent">
                                    <table class="data table table-order-items recent" id="my-orders-table">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="col id">Order #</th>
                                                <th scope="col" class="col date">Date</th>
                                                <th scope="col" class="col shipping">Name</th>
                                                <th scope="col" class="col total">Order Total</th>
                                                <th scope="col" class="col status">Status</th>
                                                <th scope="col" class="col actions">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($orders as $order)
                                            <tr>
                                                <td data-th="Order #" class="col id">{{ $order->id }}</td>
                                                <td data-th="Date" class="col date">{{ $order->created_at }}</td>
                                                <td data-th="Ship To" class="col shipping">{{ $order->address->first_name }} {{ $order->address->last_name }}</td>
                                                <td data-th="Order Total" class="col total">
                                                    <span class="price">
                                                        {{ formatPrice($order->billing_total) }}
                                                    </span>
                                                </td>
                                                <td data-th="Status" class="col status">
                                                    <span class="order-status">{{ $order->status }}</span>
                                                </td>
                                                <td data-th="Actions" class="col actions">
                                                    <a href="{{ route('user.orders', Auth::user()->id) }}" class="action view">
                                                        <span>View</span>
                                                    </a> |
                                                    <a href="{{ route('user.reorder', [ 'id' => Auth::user()->id, 'orderid' => $order->id]) }}" class="action order">
                                                        <span>Reorder</span>
                                                    </a>
                                                </td>
                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @else
                                    <br>
                                    <div class="alert alert-danger">
                                        You have no orders yet.
                                    </div>
                                @endif
                                </div>
                        </div>
                    <div class="block block-dashboard-info">
                        <div class="block-title"><span class="block-title-big">Account Information</span></div>
                        <div class="block-content">
                            <div class="row">
                            <div class="col-sm-6 box box-information">
                                <strong class="box-title">
                                    <span>Contact Information</span>
                                </strong>
                                <div class="box-content">
                                    <p>
                                        {{ $user->name }}<br>
                                        {{ $user->email }}<br>
                                    </p>
                                </div>
                                <div class="box-actions">
                                    <a class="action edit" href="{{ route('user.info', Auth::user()->id) }}">
                                        <span>Edit</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-6 box box-newsletter">
                                <strong class="box-title">
                                    <span>Newsletters</span>
                                </strong>
                                <div class="box-content">
                                    @if($subscribed)
                                        <p>You are subscribed to our newsletter.</p>
                                    @else
                                        <p>You are not subscribed to our newsletter.</p>
                                    @endif
                                </div>
                                <div class="box-actions">
                                    <a class="action edit" href="{{ route('user.subscriptions', Auth::user()->id) }}"><span>Edit</span></a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                {{-- dash #info --}}
                @elseif(strpos($curl, 'info') !== false)
                <h1>Edit Account Information</h1>

                    <div class="block block-dashboard-info">

                        <div class="block-content">
                            <div class="col-sm-12">
                                <div class="row">

                                @if(Auth::user()->orders()->count() < 1)
                                    <div class="col-sm-6">
                                        <div class="box-content">
                                            <p>
                                                {{ $user->name }}<br>
                                                {{ $user->email }}<br>
                                            </p>
                                        </div>
                                    </div>
                                @else
                                    @php
                                        $address = auth()->user()->orders->last()->address;
                                    @endphp
                                    <div class="col-sm-6">
                                        <div class="block-title account-information">
                                            <span class="block-title-big">Account Information</span>
                                        </div>
                                    <form class="form-horizontal clearfix" method="POST" action="{{ route('user.update', Auth::user()->id) }}">
                                        {{ csrf_field() }}

                                        @if(count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <div class="row">
                                        <div class="col-sm-6">
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                                <label for="first_name" class="control-label">First name <span class="asterisk">*</span></label>
                                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $address->first_name }}" placeholder="First Name" required autofocus>

                                                @if ($errors->has('first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                                <label for="last_name" class="control-label">Last name <span class="asterisk">*</span></label>
                                                <input id="text" type="last_name" class="form-control" name="last_name" value="{{ $address->last_name }}" placeholder="Last Name" required>

                                                @if ($errors->has('last_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="control-label">E-Mail Address <span class="asterisk">*</span></label>
                                            <input id="email" type="email" class="form-control" name="email" value="{{ auth()->user()->email }}" placeholder="Email" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                            <label for="phone" class="control-label">Phone <span class="asterisk">*</span></label>
                                            <input id="phone" type="text" class="form-control" name="phone" value="{{ $address->phone }}" placeholder="Phone" required autofocus>

                                            @if ($errors->has('phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                            <label for="address" class="control-label">Address <span class="asterisk">*</span></label>
                                            <input id="address" type="text" class="form-control" name="address" value="{{ $address->address }}" placeholder="Address" required autofocus>

                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                                                <label for="zip" class="control-label">Zip <span class="asterisk">*</span></label>
                                                <input id="zip" type="number" class="form-control" name="zip" value="{{ $address->zip }}" placeholder="Zip" required>

                                                @if ($errors->has('zip'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('zip') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-sm-6 form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                                <label for="city" class="control-label">Town / city <span class="asterisk">*</span></label>
                                                <input id="city" type="text" class="form-control" name="city" value="{{ $address->city }}" placeholder="City" required>
                                                @if ($errors->has('city'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('city') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                            <label for="address" class="control-label">Country <span class="asterisk">*</span></label>
                                            <select name="country" id="country">
                                                <option value="serbia" <?php if($address->country == 'serbia'){ echo 'selected'; } ?>>Serbia</option>
                                                <option value="croatia" <?php if($address->country == 'croatia'){ echo 'selected'; } ?>>Croatia</option>
                                                <option value="bosnia" <?php if($address->country == 'bosnia'){ echo 'selected'; } ?>>Bosnia</option>
                                                <option value="macedonia" <?php if($address->country == 'macedonia'){ echo 'selected'; } ?>>Macedonia</option>
                                                <option value="slovenia" <?php if($address->country == 'slovenia'){ echo 'selected'; } ?>>Slovenia</option>
                                            </select>
                                            @if ($errors->has('country'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('country') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </form>
                                    </div>
                                @endif
                                    <div class="col-sm-6" >
                                        <div class="block-title account-information">
                                            <span class="block-title-big">Reset Password</span>
                                        </div>
                                        <a href="{{ route('password.request') }}" class="btn">Reset Password</a>
                                    </div>
                            </div>
                            </div>
                        </div>
                    </div>

                    </div>

            {{-- #address --}}
                @elseif(strpos($curl, 'addresses') !== false)

            @if($address = auth()->user()->orders->last())
                @php
                    $address = auth()->user()->orders->last()->address;
                @endphp
                <h1>My Address</h1>
                <div class="block mem-dashboard-orders">
                    <div class="block-title-subs address">
                        <span class="block-title-big">The address of the last order</span>
                    </div>
                    <div class="block-content">
                        <table class="my-address-table" id="my-address-table">
                            <thead class="order-head">
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Phone</th>
                                <th>Street</th>
                                <th>Zip</th>
                                <th>City</th>
                                <th>Country</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $address->first_name }}</td>
                                    <td>{{ $address->last_name }}</td>
                                    <td>{{ $address->phone }}</td>
                                    <td>{{ $address->address }}</td>
                                    <td>{{ $address->zip }}</td>
                                    <td>{{ $address->city }}</td>
                                    <td>{{ $address->country }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="col-sm-4">
                            <a href="{{ route('user.info', auth()->user()->id) }}">Edit</a>
                        </div>
                    </div>
                </div>
                @else
                <h1></h1>
                    <span>You have no addresses yet</span>
                @endif

                {{-- #orders --}}
                @elseif (strpos($curl, 'orders') !== false)
                <h1>My Orders</h1>
                    <div class="block mem-dashboard-orders">
                        <div class="block-title order">
                            <span class="block-title-big">Recent Orders</span>
                            <a class="action view" href="{{ route('user.orders', Auth::user()->id) }}"></a>
                        </div>
                        <div class="block-content">
                            @if(count($orders) > 0)
                            <div class="table-wrapper orders-recent">
                                <table class="data table table-order-items recent" id="my-orders-table">
                                    <thead>
                                    <tr>
                                        <th scope="col" class="col id">Order #</th>
                                        <th scope="col" class="col date">Date</th>
                                        <th scope="col" class="col shipping">Name</th>
                                        <th scope="col" class="col total">Order Total</th>
                                        <th scope="col" class="col status">Status</th>
                                        <th scope="col" class="col actions">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr class="order-head">
                                            <td data-th="Order #" class="col id">{{ $order->id }}</td>
                                            <td data-th="Date" class="col date">{{ $order->created_at }}</td>
                                            <td data-th="Ship To" class="col shipping">{{ $order->address->first_name }} {{ $order->address->last_name }}</td>
                                            <td data-th="Order Total" class="col total">
                                                    <span class="price">
                                                        {{ formatPrice($order->billing_total) }}
                                                    </span>
                                            </td>
                                            <td data-th="Status" class="col status">
                                                <span class="order-status">{{ $order->status }}</span>
                                            </td>
                                            <td data-th="Actions" class="col actions">
                                                <a href="{{ route('user.reorder', [ 'id' => Auth::user()->id, 'orderid' => $order->id]) }}" class="action order">
                                                    <span>Reorder</span>
                                                </a>

                                            </td>
                                        </tr>
                                        @foreach($order->products()->get() as $product)
                                        <tr>
                                            <td><img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}"></td>
                                            <td>{{ $product->sku }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{ formatPrice($product->price) }}</td>
                                            <td><strong>x</strong> {{ $product->pivot->quantity }}</td>
                                            <td></td>
                                            
                                        </tr>
                                        @endforeach

                                    {{--@empty--}}
                                        {{--You have no orders yet.--}}
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                                <br>
                                <div class="alert alert-danger">
                                    You have no orders yet.
                                </div>
                            @endif
                        </div>
                    </div>
                {{-- dash addresses --}}
                @elseif (strpos($curl, 'subscriptions') !== false)
                <h1>Newsletter Subscription</h1>
                    <div class="block-title-subs">
                        <span class="block-title-big">Subscription option</span>
                    </div>
                    <form class="form form-newsletter-manage" action="{{ route('user.subscriptions', Auth::user()->id) }}" method="post" id="subscription-form">
                        <div class="form-group">
                            {{ csrf_field() }}
                            <br>
                            <div class="field choice">
                                <input type="checkbox" name="is_subscribed" id="subscription" title="Newsletter Subscription" class="checkbox"
                                <?php
                                    if ($subscribed){
                                        echo 'checked';
                                    }
                                    ?>
                                >
                                <label for="subscription" class="label">
                                    <span>Newsletter Subscription</span>
                                </label>
                            </div>
                        </div>
                        <div class="actions-toolbar">
                            <div class="primary">
                                <button type="submit" title="Save" class="btn">
                                    <span>Save</span>
                                </button>
                            </div>
                        </div>
                    </form>
                {{-- dash wishlist --}}
                @elseif (strpos($curl, 'wishlist') !== false)
                <h1>My Wish List</h1>

                @if($wishlist->count() > 0)

                @forelse($wishlist->chunk(3) as $chunk)

{{--                        @forelse ($wishlist->chunk(3) as $chunk)--}}
                    @if($chunk)
                            <div class="product-grid-flex clearfix">
                                @foreach ($chunk as $product)

                                    <div class="col-sm-4 product">
                                        <div class="product-grid-item">
                                            <div class="product-img">
                                                <span class="new"></span>
                                                <span class="sale"></span>
                                                <a href="{{ route('shop.show', $product->slug) }}">
                                                    <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">
                                                </a>
                                            </div>
                                            <div class="product-desc">
                                                <h3><a href="{{ route('shop.show', $product->slug) }}">{{ $product->name }}</a></h3>
                                                @if($product->on_sale)
                                                    <p>
                                                        @if($product->new)
                                                            <span class="new-product">NEW</span>
                                                        @endif
                                                        <span class="vrnr">{{ formatPrice($product->sale_price) }}</span>
                                                        <span class="reduced">{{ formatPrice($product->price) }}</span>
                                                    </p>
                                                @else
                                                    <p>
                                                        @if($product->new)
                                                            <span class="new-product">NEW</span>
                                                        @endif
                                                        <span class="vrnr regular-price">{{ formatPrice($product->price) }}</span>
                                                    </p>
                                                @endif
                                                    <a href="{{ route('shop.show', $product->slug) }}" class="btn red">Shop Now</a>
                                            </div>
                                            <span class="product-fav">
                                                <form action="{{ route('user.removeWProduct', ['id' => Auth::user()->id, 'product' => $product->id]) }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button type="submit" name="remove_from_wishlist"><i class="fas fa-times"></i></button>
                                                </form>
                                    </span>
                                        </div>
                                    </div>

                                @endforeach
                                
                            </div>

                            
                        @endif
                @empty
                    
                @endforelse
                    <div class="wishlist-bottom-actions">
                        <form action="{{ route('user.clearWishlist', Auth::user()->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn default">Clear My Wishlist</button>
                        </form>
                    </div>
                @else
                    <span>You have no items in your wish list.</span>
                    
                @endif
                @endif

            </div>


        </div>
    </div>
@endsection