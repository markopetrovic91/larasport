@extends('layouts.app', ['bodyClass' => 'terms-of-service'])

@section('content')
    <div class="heading-title">
        <div class="heading-title-wrapper">
            <h1>Terms of service</h1>
        </div>
    </div>
    <div class="container terms-container">
        <h3>1. INTRODUCTION</h3>
        <p>The Larasport Online Shop is operated by PUMA Europe GmbH (PUMA Way 1, 91074 Herzogenaurach, Germany).
            The Larasport Europe GmbH is registered at Amtsgericht Fürth, HRB 14539.
            Managing Directors: Martyn Bowen, Georg Stammen </p>
<p>Only these terms and conditions apply to the sale of merchandise by us.
            Any other terms and conditions will only apply if they have been accepted by us.</p>

        <p>We sell merchandise solely to end users and only in normal household volumes.</p>

        <h3>2. Contract Closure</h3>
        <h4>2.1 contract closure regarding standard products</h4>
        <p>The presentation of our merchandise invites you to submit a purchase offer in the form of an order. Clicking the "Send order" button in the last stage of the ordering process issues a binding order for the products you have placed in the shopping bag. As soon as your order has been received by us, you will receive an automatically generated order confirmation by email. This confirmation does not yet represent a binding purchase contract. The purchase contract only comes into effect with the delivery of the merchandise.</p>
        <h4>2.2 contract closure regarding personalized products</h4>
        <p>After you have placed your order for a personalized product, we will send an order confirmation to you by E-Mail. When you receive this order confirmation, your order is accepted and a binding contract of sale is concluded between you and us.</p>


        <h3>3. Accepting the Order</h3>

        <p>An order that is binding for you comes into effect when all of the data necessary to complete the contract has been entered, you have confirmed acceptance of these terms and conditions and you have clicked the "Send order" button. Up until the moment you click this button, you can change your order and the data associated with your order at any time.</p>

        <p>We reserve the right to accept an order. The decision is made at the discretion of PUMA Europe GmbH. If an order is not executed, we will inform you immediately by email. The confirmation of receipt does not constitute a purchase contract; the confirmation of receipt only documents that your order has been received by us.</p>
        <p>We currently offer online sales in the following countries: <br>
            Austria, Belgium, Czech Republic, Denmark (except Greenland, Faroes Island), Estonia, Finland, France (except overseas departments / DOM, French Guiana, Monaco), Germany (except Island of Helgoland, Büsingen am Hochrhein), Great Britain (except Channel Islands, Isle of Man, Orkney Islands, Hebrides, Shetland Islands), Greece, Hungary, Ireland, Italy, Latvia, Luxembourg, the Netherlands (except ABC Islands), Poland, Portugal (except Azores and Madeira), Slovakia, Slovenia, Spain (except Canary Islands, Ceuta and Melilla), Sweden and Switzerland.</p>


        <h3>4. Accepting the Order</h3>
        <p>Orders may only be placed for normal household volumes.</p>

        <h3>5. Specifics of Personalized Products</h3>
        <p>When offered in our Online Shop, you can personalize products to your own preference: <br>
            For jerseys you can either choose a player name, along with his jersey number, or add a name in the text field with any number up to 2 digits.
            <br>
            Please note that some special characters may not be supported by our system.</p>
        <p>
            You may not use, upload, copy, submit or publish any names or other text which <br>
            <li>- incite violence, are threatening, defamatory, obscene, discriminatory, inflammatory, sexually explicit or otherwise offensive or unlawful;</li>
            <li>- consist of/contain content protected by law, including the name of products, services, companies, organizations or events which belong to a third party;</li>
            <li>- infringe third party trademarks or other intellectual property rights of third parties.</li>
        </p>
        <p>We reserve the right to reject any personalization as whole or in parts if they fall into one of the above categories or if we deem it unacceptable.
            <br>
            We may do so directly during the personalization process or following your submittance of the order by cancelling your order.</p>
        <p>By placing your order for your personalized Product, you represent and warrant that any names or text you submit to us does not fall into any of the above categories.
            <br>
            You agree to indemnify us and our affiliated companies against all costs, expenses, damages, losses and liabilities incurred or suffered by us or our affiliated companies as a result of the use of any names or text used, uploaded, copied, submitted, or otherwise published by you.
            <br>
            You grant us and our affiliated companies a non-exclusive, irrevocable, royalty-free, worldwide, sub-licensable right to use, reproduce, disclose and modify the names and texts submitted by you for the purposes of carrying out your order.
        </p>
        <p>To personalized products the right of recovation and our voluntary right of 30 day return do not apply. You staturoy rights remain uneffected.</p>
        <p>Please note: our custom jersey tool shows you a digital representation of your requested name/number combination.  Depending on your computer and browser, it may not look perfect on the screen.  Be assured, that the ordered jersey will be true to the original.</p>



        <h3>6. Delivery and Delivery Charges</h3>
        <p>The delivery of the ordered merchandise will be made to the delivery address specified by the ordering party.</p>
        <p>If one or more of the items you have ordered are sold out at the time of ordering, we will not be able to accept your order. We will then inform you immediately by email if this should be the case. Delivery will be made by DHL, DHL Express or UPS.</p>
        <p>We offer free standard shipping for all orders </p>



        <h3>7. Payment</h3>
        <p>All prices specified for merchandise in the Online Shop already contain the relevant valid VAT.</p>
        <p>Please note that agreements you may have with your credit institute or other institutions may result in additional costs for electronic transfers, holding an account, etc.</p>

        <h3>8. Retention of Title</h3>
        <p>Until such time as full payment has been received, the merchandise remains our property.</p>
        <h3>9. Returns</h3>
        <p>We strongly advise you to use the following method of returning items. (You are not legally obligated comply with this method):
            <br>
        <ul>
            <li>DHL: To return goods free of charge, use the supplied package return sticker. The package can be handed in at the nearest post office.</li>
            <li>UPS: To return goods free of charge, use the supplied package return sticker and contact UPS directly to arrange a pick up time. Please ensure that the merchandise is packed and ready for dispatch and use the supplied UPS package return sticker.</li>
            <li>DHL Express: In order to receive a free return label please contact our customer service with reference to the order number by phone or E-Mail.</li>
        </ul>
        </p>
        <p>If you choose another method of dispatch for your returns, you normally are responsible for the costs.</p>

        <h3>10. Guarantee</h3>
        <p>Your legal guarantee claim period is 2 years and starts with receipt of the merchandise. During this time, we will eliminate all faults that fall under the legal guarantee obligation. You can demand either the elimination of the fault, or the delivery of a fault-free item, although the right to choose may be limited by the existence of legal prerequisites. If it is not possible to provide a replacement, you may choose to withdraw from the sales contract or to accept a discount. By the way, the legal claim for damages only applies to the replacement of damages not compensation/replacement for any failed attempts on your part to repair item. In the event of a fault occurring within 6 months of delivery, it is probable that the goods were already faulty when purchased. If the fault shows more than 6 months after delivery, the purchaser is responsible for proving that the fault was already inherent when the purchased object was delivered.</p>

        <h3>11. Privacy Policy</h3>
        <p>All personal data is fundamentally treated in the strictest of confidence. Your personal data is used to enable the optimum handling of the order, to deliver the merchandise, and to provide services and to handle payments. For this purpose, we pass your data on to authorised service providers and, if necessary, to associated companies.</p>
        <p>For the purpose of payment processing the necessary data is stored both on our server and on the servers of the companies we have authorized to handle payment processing.</p>
        <p>In addition, data, for example, name, date-of-birth and address will be passed to financial information services for the purpose of checking your credit and credit worthiness.</p>
        <p>See our <a href="/privacy-policy/">Privacy Policy</a> for additional data protection information. The Privacy Policy is an integral part of these General Terms and Conditions.</p>

        <h3>12. Applicable law</h3>
        <p>German law applies subject to any mandatory provisions of the applicable law of your place of residence. The UN convention is thus excluded.</p>


        <h3>13. Online Dispute Resolution</h3>
        <P>The European Commission provides an online dispute resolution platform, which can be found at http://ec.europa.eu/consumers/odr/. Larasport Europe GmbH is not obliged and willing to participate in dispute settlement proceedings before a consumer arbitration board.</P>


        <h3>14. Provider Identification Details</h3>
        <p>Laravel Europe GmbH <br>
            91074 Herzogenaurach <br>
            Germany </p>
<p>
            Registered Office: Herzogenaurach, Germany</p>
<p>
            Registered at Amtsgericht Fürth, HRB 14539</p>
<p>
            Managing Directors: Martyn Bowen, Georg Stammen</p>

        <h3>CUSTOMER SERVICE</h3>
        <p><strong>Email:</strong> service@larasport.com</p>
        <p><strong>Hotline:</strong> (+44) 02 - 033 265 481</p>
    </div>
@endsection