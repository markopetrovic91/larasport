@extends('layouts.app', ['title' => 'Checkout Page', 'bodyClass' => 'page-checkout', 'id' => 'checkout'])

@section('content')
<div class="heading-title">
    <div class="row_triangle row_triangle_top triangle_bkg"></div>
    <div class="heading-title-wrapper">
        <h1>Checkout Page</h1>
        <p>Just one more step to finish your order</p>
    </div>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">

<div class="checkout-order-summary">
@if(Cart::instance('default')->count() > 0)
        @guest
        <span class="returning-customer">Returning customer?</span> <a href="/login">Click here to login</a>
        @endguest

<form action="{{ route('checkout.store') }}" method="post" id="payment-form" >
            {{ csrf_field() }}
    <div class="row">
        {{-- billing details --}}
        <div class="col-sm-6 billing-details">
            <h4>BILLING DETAILS</h4>
            {{--@if(Auth::user()->orders()->count())--}}
{{--                {{ var_dump(Auth::user()->orders()) }}--}}

            {{--@endif--}}
            <div class="billing-form-wrapper clearfix">

                    <div class="row">
                        <div class="col-sm-6 field-group">
                            <label for="first_name">First Name <span class="asterisk">*</span></label>
                            <input type="text" name="first_name" class="first_name" value="{{ old('first_name') }}" required>
                        </div>
                        <div class="col-sm-6 field-group">
                            <label for="last_name">Last Name <span class="asterisk">*</span></label>
                            <input type="text" name="last_name" class="last_name" value="{{ old('last_name') }}" required>
                        </div>
                    </div>

                <div class="col-sm-12 field-group">
                    <div class="row">
                        <label for="email">Email <span class="asterisk">*</span></label>
                        <input type="email" name="email" class="email" value="{{ old('email') }}" required>
                    </div>
                </div>
                <div class="col-sm-12 field-group">
                    <div class="row">
                        <label for="phone">Phone <span class="asterisk">*</span></label>
                        <input type="tel" name="phone" class="phone" value="{{ old('phone') }}" required>
                    </div>
                </div>

                <div class="col-sm-12 field-group">
                    <div class="row">
                        <label for="country">Country <span class="asterisk">*</span></label>
                        <select name="country" id="country">
                            <option value="serbia">Serbia</option>
                            <option value="croatia">Croatia</option>
                            <option value="bosnia">Bosnia</option>
                            <option value="macedonia">Macedonia</option>
                            <option value="slovenia">Slovenia</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 field-group">
                    <div class="row">
                        <label for="address">Address <span class="asterisk">*</span></label>
                        <input type="text" id="address" name="address" class="address" placeholder="House number and street name" value="{{ old('address') }}" required>
                    </div>
                </div>
                <div class="col-sm-12 field-group">
                    <div class="row">
                        <label for="city">Town / City <span class="asterisk">*</span></label>
                        <input type="text" id="city" name="city" class="city" value="{{ old('city') }}" required>
                    </div>
                </div>
                <div class="col-sm-12 field-group">
                    <div class="row">
                        <label for="city">Post code / ZIP <span class="asterisk">*</span></label>
                        <input type="text" name="zip" class="zip" id="zip" value="{{ old('zip') }}" required>
                    </div>
                </div>
                <div class="col-sm-12 additional-info-wrapper">
                    <div class="row">
                        <h5>Order notes</h5>
                        <textarea name="additional" class="additional" id="additional" cols="30" rows="7" value="{{ old('additional') }}"></textarea>
                    </div>
                </div>
            </div>
            
        </div>

        {{-- order detaills and payment gateway --}}
        <div class="col-sm-6 order-details">
            <h4>YOUR ORDER</h4>
            <div id="order_review" class="checkout-review-order">
	<table class="checkout-table review-order-table">
	<thead>
		<tr>
			<th class="product-name">Product</th>
			<th class="product-total">Total</th>
		</tr>
	</thead>
	<tbody>
        @foreach(Cart::content() as $item)
        <tr class="cart_item">
            <td class="product-name">
                {{ $item->name }}&nbsp;<strong class="product-quantity">× {{ $item->qty }}</strong>
            </td>
            <td class="product-total">
                <span class="Price-amount amount">{{ formatPrice($item->total) }}</span>
            </td>
        </tr>
        @endforeach
    </tbody>
	<tfoot>
        @if(session()->has('coupon'))
        <tr class="cart-discount">
            <th>Discount</th>
            <td>
                <span class="Price-amount amount">
                    -{{ formatPrice($discount) }}
                </span>
            </td>
        </tr>
        @endif
		<tr class="cart-subtotal">
			<th>Subtotal</th>
			<td>
                <span class="Price-amount amount">
                    {{ formatPrice($newSubtotal) }}
                </span>
            </td>
		</tr>
        <tr class="tax-total">
            <th>Tax</th>
            <td><span class="Price-amount amount">
                <span class="Price-currencySymbol"></span>{{ formatPrice($tax) }}</span>
            </td>
        </tr>
		<tr class="order-total">
			<th>Total</th>
			<td><strong><span class="Price-amount amount">
                <span class="Price-currencySymbol"></span>{{ formatPrice($newTotal) }}</span></strong>
            </td>
		</tr>
	</tfoot>
</table>

{{-- payment gateways --}}
<div id="payment" class="checkout-payment">
    <h4>Payment methods</h4>
    <ul class="payment_methods methods">
        <li class="payment_method_stripe">
            <input id="payment_method_stripe" type="radio" class="input-radio payment_method" name="payment_method" value="stripe" checked="checked" @click="selected = 1">

            <label for="payment_method_stripe">
                Via Stripe 	</label>
                <div class="payment_box">
                    <div id="payment-details"> 
                            {{-- <h4>PAYMENT DETAILS</h4> --}}
                            <div class="stripe-form-wrapper">
                                <div class="row">
                                    <div class="col-sm-12 field-group">
                                        <label for="name_on_card">Name on card</label>
                                        <input type="text" name="name_on_card" class="form-control" id="name_on_card" placeholder="your name on card">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="field-group col-sm-12">
                                        <div class="form-row">
                                            <label for="card-element">
                                                Credit or debit card
                                            </label>
                                            <div id="card-element">
                                                <!-- A Stripe Element will be inserted here. -->
                                            </div>
                        
                                            <!-- Used to display Element errors. -->
                                            <div id="card-errors" role="alert"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
        </li>
        <li class="payment_method_cheque">
            <input id="payment_method_on_delivery" type="radio" class="input-radio payment_method" name="payment_method" value="on_delivery" @click="selected = 2">

            <label for="payment_method_on_delivery">
                On delivery 	</label>
                <div class="payment_box" style="display: none;">
                    <p>Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
                </div>
        </li>
	</ul>
	
	<div class="form-row place-order">

		<input type="submit" class="button" name="checkout_place_order" id="place_order" value="Place order">
        {{--<p class="form-row terms">--}}
            {{--<label for="terms" class="checkbox">I’ve read and accept the <a href="/terms-and-conditions" target="_blank">terms &amp; conditions</a></label>--}}
            {{--<input type="checkbox" class="input-checkbox" name="terms" id="terms">--}}
        {{--</p>--}}
        <div class="checkbox">

            <input id="terms" type="checkbox" name="terms" {{ old('terms') ? 'checked' : '' }}>
            <label for="terms" class="checkbox">I’ve read and accept the <a href="/terms-and-conditions" target="_blank">terms &amp; conditions</a></label>
            <p class="terms-errors"></p>
        </div>
	</div>
</div>
{{-- end payment gateways --}}
                </div>
        </div>
        {{-- end order detaills --}}
    </div>
</form>

@else
        <div class="no-items-in-cart">
            <h3>You have no items in your Cart</h3>
            <a href="{{ route('shop.index') }}" class="btn">Continue shopping</a>
        </div>
@endif
</div>
</div>
@endsection
@section('footer')
<script src="https://js.stripe.com/v3/"></script>
    <script>
        // (function(){
            // Create a Stripe client.
            // var stripe = Stripe('pk_test_KaazNYX9HTdcb1F9KjdUXVRo');
            var stripe = Stripe('pk_test_KaazNYX9HTdcb1F9KjdUXVRo');
            // Create an instance of Elements.
            var elements = stripe.elements();
            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    color: '#32325d',
                    fontSize: '16px',
                }
            };
            // Create an instance of the card Element.
            var card = elements.create('card', {
                    style: style,
                    hidePostalCode: true
                });
            // Add an instance of the card Element into the `card-element` <div>.
            card.mount('#card-element');
            // Handle real-time validation errors from the card Element.
            card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
            });
            // Handle form submission.
            var form = document.getElementById('payment-form');
            var radioondelivery = document.getElementById('payment_method_on_delivery');
            var termscond = document.getElementById('terms');

            form.addEventListener('submit', function(event) {
                event.preventDefault();


                if(!termscond.checked){
                    document.getElementsByClassName('terms-errors')[0].innerHTML = "You have to agree with terms and contitions";
                    return false;
                }

                if(radioondelivery.checked){
                //                Disable the submit button to prevent repeated clicks
                    document.getElementById('place_order').disabled = true;
                    form.submit();
                }
                //                Disable the submit button to prevent repeated clicks
                document.getElementById('place_order').disabled = true;

                var options = {
                    name: document.getElementById   ('name_on_card').value,
                    address_line1: document.getElementById('address').value,
                    address_city: document.getElementById('city').value,
                    address_state: document.getElementById('country').value,
                    address_zip: document.getElementById('zip').value
                }
                stripe.createToken(card, options).then(function(result) {
                    if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
//                    Enable the submit button
                    document.getElementById('place_order').disabled = false;
                    } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                    }
                });
            });
            function stripeTokenHandler(token) {
                // Insert the token ID into the form so it gets submitted to the server
                var form = document.getElementById('payment-form');
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);
                // Submit the form
                form.submit();
            }
        // })();
    </script>
@endsection