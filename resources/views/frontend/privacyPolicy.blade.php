@extends('layouts.app', ['bodyClass' => 'privacy-policy'])

@section('content')
    <div class="heading-title">
        <div class="heading-title-wrapper">
            <h1>Privacy Policy</h1>
        </div>
    </div>
    <div class="container terms-container">
        <p>This privacy policy provides information on the processing of your personal data whenever you visit websites of Larasport Europe GmbH (hereinafter “Larasport”, “we” or “us”), and when purchasing goods from said websites in accordance with the EU General Data Protection Regulation (hereinafter “GDPR”).</p>


    <h3>1. Scope, data controller, data protection officer and definitions</h3>
        <h4>1.1. Scope of this privacy policy</h4>
        <ol>
            <li>This privacy policy applies to the use of the websites of Larasport, the online shop and any measures connected with purchasing for the implementation and annulment of the contract, the creation of a customer account, the dispatch of the newsletter, further informative e-mails and customer services, and for our marketing activities on third-party websites and in social networks. You can call up, store and print out this privacy policy at any time and free of charge via the link on our websites http://larasport.com/privacy-policy.</li>
            <li>This privacy policy concerns only the processing of personal data on the websites http://larasport.com, as well as any marketing activities undertaken on third-party websites and in social networks. Other websites are not covered by this privacy policy.</li>
        </ol>

        <h4>1.2. The controller for the processing of your personal data</h4>
        <p>Unless otherwise specified in this privacy policy, the controller for the processing of your personal data is:</p>

        <p>Laravel Europe GmbH <br>
            91074 Herzogenaurach <br>
            Germany </p>
        <p>
            Registered Office: Herzogenaurach, Germany</p>
        <p>
            Registered at Amtsgericht Fürth, HRB 14539</p>
        <p>
            Managing Directors: Martyn Bowen, Georg Stammen</p>

        <h4>1.3 Definitions</h4>
        <p>This privacy policy is based on the following terms under data protection law, which we have defined to make them easier to understand.</p>

        <ol>
            <li>The GDPR is the EU General Data Protection Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC).</li>
            <li>Recipient is a natural or legal person, public authority, agency or another body, to which the personal data is disclosed, whether a third party or not this involves a third party. However, public authorities which may receive personal data in the framework of a particular enquiry in accordance with Union or Member State law shall not be regarded as recipients; the processing of such data by the aforementioned public authorities shall be carried out in compliance with the applicable data protection rules according to the purposes of the processing; Recipients of your personal data, depending on the chosen method of payment in the event of a sale, can be banks, for example, or the postal / dispatch service, through which we deliver your goods to you.</li>
            <li>The Larasport Group includes all enterprises that are affiliated with Larasport Europe GmbH pursuant to Section 15 Aktiengesetz [German Stock Corporation Act].</li>
            <li>Personal data refers to all information that concerns an identified or identifiable natural person, i.e. the data subject. An identifiable natural person is one who can be identified, directly or indirectly, in particular through association with an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of said natural person. Personal data may be, for example, name, contact data, user behaviour or bank details.</li>
            <li>Controller is the natural or legal person, public authority, agency, or other body that decides, by itself or jointly with others, on the purposes and means of processing of personal data. Where the purposes and means of such processing are determined by Union or Member State law, the controller or the specific criteria for its nomination may be provided for by Union or Member State law. For the data processing activities described in this privacy policy, the controller is Larasport Europe GmbH (sub-section 1.2.).</li>
            <li>Processing is any operation performed with or without automated procedures, or any series of such operations, in connection with personal data, such as the collection, recording, organisation, structuring, storage, adaptation or modification, the reading out, query, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination. restriction, erasure or destruction. Typical processing activities can include the collection and use of your ordering data when you make a purchase.</li>
        </ol>

    <h3>2. Purposes, legal bases as well as data categories, with regard to the processing of your personal data</h3>
        <h4>2.1. Processing of your data when you visit our websites within the scope of marketing activities on third-party websites and in social networks</h4>

    <p>If you call up our websites in order to find out about products and services, without registering for a customer account, purchasing products in our online shop or otherwise actively transferring information to us (purely for informational purposes), we process your personal data. In addition, we process your personal data within the scope of marketing activities on third-party websites and in social networks. Your personal data is processed for the following purposes and by virtue of the following legal basis:</p>


        <h5>2.1.1. Processing for the purposes of IT security</h5>
        <ol>
            <li>When you visit our website, we process your personal data that is technically necessary to allow us to
                provide our websites to you and to guarantee stability and security when you visit our websites. For
                this purpose, we process the following personal data:
                <ul>
                    <li>IP address</li>
                    <li>type and version of browser</li>
                    <li>operating system and platform</li>
                    <li>the complete Uniform Resource Locator (URL)</li>
                </ul>
            </li>
            <li>We process your personal data based on our legitimate interest to guarantee IT security for you when you visit our websites, based on Art. 6.1(f) GDPR.</li>

        </ol>
        <h5>2.1.2. Processing for the provision of localised websites</h5>
        <p>When you visit our website, we process your personal data that is technically necessary to allow us to provide you with a localised version of the websites, in particular with regard to the language and culture. We process your personal data for the purpose of carrying out a contract with you, based on Art. 6.1(b) GDPR, and based on our legitimate interest in adapting our website to your needs, based on Art. 6.1(f) GDPR.</p>

        <h5>2.1.3. Processing for analytical purposes</h5>
        <p>1. For the analysis, on our websites we use <strong>Google Analytics</strong>, a web analysis service of Google Inc. (“Google”). Google Analytics uses cookies that allow an analysis to be made on the way our websites are used. The information generated by the cookie on the use of our websites is, as a rule, transferred to a server of Google in the USA, where it is stored. Should the IP anonymisation be enabled on our websites, however, Google will shorten the IP address within member States of the European Union or in other States party to the Agreement on the European Economic Area. Only in exceptional cases is the full IP address transferred to a server of Google in the USA, and shortened there. On behalf of Larasport, Google will use this information in order to evaluate the use of the websites, to prepare reports on the website activities, and to provide further services to Larasport connected with the use of the websites and of the Internet.</p>
        <p>The IP address transferred from the user's browser with regard to Google Analytics is not merged with other data by Google.</p>
        <p>You can prevent cookies from being stored by appropriately setting your browser software; we must point out, however, that in this case you may not be able to use all functions of our websites to their full extent.</p>
        <p>In addition, you may prevent any data that is generated by the cookie and relates to your use of the website (incl. your IP address) being recorded by Google, as well as the processing of this data by Google, by downloading and installing the browser plug-in available under the following link: https://tools.google.com/dlpage/gaoptout</p>
        <p>An opt-out cookie is set that prevents your data from being recorded when you next visit these websites.</p>
        <p>Our websites use Google Analytics with the extension “_anonymizelp()”. In this way, IP addresses are further processed in an abridged form; this excludes any direct association with any specific person. </p>
        <p>Google Analytics is used in compliance with the pre-requisites agreed on by and between the German data protection authorities and Google.</p>
        <p>You can find further information on the terms of use and data protection under:</p>
        <ul>
            <li><a href="https://policies.google.com/terms" target="_blank">https://policies.google.com/terms</a></li>
            <li><a href="https://policies.google.com/privacy" target="_blank">https://policies.google.com/privacy</a></li>
        </ul>
        <p>We process your personal data based on our legitimate interest to carry out analyses and, based on these analyses, to improve our website as well as our products and services, and to prevent fraud, on the basis of Art. 6.1(f) GDPR.</p>
        <p>2. For the statistical evaluation of our websites, use also use the technologies of Adobe Systems Incorporated and Mapp Digital US LLC, with the aim of constantly improving and optimising our the products and services we offer.</p>
        <p>Using the services of Adobe Analytics (Omniture) and Mapp, we collect statistical data on the use of our websites. We use this data in order to constantly improve and optimise the Larasport online shop and the products and services we offer, and thus make it more interesting for you to visit our websites.</p>
        <p>When you use our websites, information that is transferred by your browser is collected and evaluated. This is achieved using a cookie technology and Pixel, that are embedded on each website. The following data is collected: Request (name of the requested file), browser type/version, browser language, operating system, inner resolution of the browser window, screen resolution, java script enabling, Java on/off, cookies on/off, colour depth, referrer URL, IP address (is collected exclusively in an anonymised form and deleted once more immediately after use), time of access, clicks, order values, shopping carts and anonymised form contents (for example, whether or not a phone number has been provided).</p>
        <p>None of this data may be directly allocated. The thus collected data serves to create anonymous or pseudonymised user profiles that form the basis for web statistics. The data collected by means of cookies from Adobe Analytics and Mapp are never used to personally identify the user to our websites, nor are they mixed together with personal data of the profile owner, unless the user has given his/her express consent thereto in advance.</p>
        <p>Consent for the collection and storage or data by Adobe Analytics and Mapp can be withdrawn at any time with effect for the future. For this purpose, please use the following links:</p>
        <ul>
            <li>Adobe Analytics: <a href="https://www.adobe.com/privacy/opt-out.html" target="_blank">https://www.adobe.com/privacy/opt-out.html</a></li>
            <li>Mapp: <a href="http://go.flx1.com/opt-out" target="_blank">http://go.flx1.com/opt-out</a></li>
        </ul>
        <p>Follow the above links to set an opt-out cookie in each case. This opt-out cookie has a validity of at least five years. Please note that if all cookies are deleted on your computer, this opt-out cookie will likewise be deleted. This means that if you still want to oppose the anonymised or pseudonymised collection of data by Adobe Analytics and/or Mapp, you will have to reset an opt-out cookie.</p>
        <p>We process your personal data based on our legitimate interest to carry out analyses and, based on these analyses, to improve our website as well as our products and services, and to prevent fraud, on the basis of Art. 6.1(f) GDPR.</p>
        <p>3. After due consent has been given, we will combine your pseudonymised user profiles in the data management platform (DMP) with personal data from complex event processing (CEP). The usage data collected on the websites by means of Mapp system cookies, as well as data from existing (pseudonymised) user profiles from the system, your user data and any additional information on your browsing habits on Larasport's websites will be gathered for advertising purposes, such as behavioural targeting, targeted marketing in the sports attire sector, sport gear as well as merchandising items. In so doing, we process your personal data for marketing purposes based on your consent within the meaning of Art. 6.1(a) GDPR.</p>
        <p>4. When you visit our websites, we observe and record how you use our websites, including the flow of the visit to our websites, the heat maps as well as the analysis of the forms. For this purpose, we process the following personal data:</p>
        <ul>
            <li>IP address including all connected data</li>
            <li>We also use cookies or another anonymous identifier to identify you anonymously, through the generation of an unambiguous ID in order to track your behaviour.</li>
            <li>Technical information on your computer, such as browser type, system language, operating system version, type of phone, time zone, headers, and screen resolution.</li>
            <li>The sites that you have visited and URLs from which you have reached our website.</li>
            <li>Your interaction with the website including all mouse or touch movements, scrolling, mouse clicks, screen buttons or zoom information</li>
            <li>The time you have spent on the respective website</li>
            <li>Any errors you have come across on the website</li>
            <li>Information that you have provided to us on our websites</li>
            <li>The HTML code of the website that you have visited</li>
        </ul>
        <p>We process your personal data based on our legitimate interest to carry out analyses and, based on these analyses, to improve our website as well as our products and services, on the basis of Art. 6.1(f) GDPR.</p>

        <h5>2.1.4. Processing for the purpose of individual recommendations on our websites</h5>
        <ol>
            <li>When you visit our web pages, we observe and record your user behaviour, in order to show you individual
                recommendations on our websites based on this data. For this purpose, we process the following personal
                data and/or product data, the relevance of which is derived from the actual order you make:
                <ul>
                    <li>IP address</li>
                    <li>Information on your browser and access device</li>
                    <li>title, first name(s), surname</li>
                    <li>e-mail address</li>
                    <li>delivery and billing address</li>
                    <li>phone number</li>
                    <li>shopping cart ID / order ID</li>
                    <li>sub-total of purchase</li>
                    <li>title</li>
                    <li>currency</li>
                    <li>discounts</li>
                    <li>dispatch [mode]</li>
                    <li>article(s)</li>
                    <li>product ID</li>
                    <li>URL</li>
                    <li>price of article</li>
                    <li>category of article</li>
                    <li>tax</li>
                </ul>
            </li>
            <li>We process the above using Google Adwords. For further details on the processing of data by Google Adwords, please refer to the corresponding privacy policy under: https://policies.google.com/privacy</li>
            <li>We process your personal data based on our legitimate interest to make product recommendations and carry out marketing activities, on the basis of Art. 6.1(f) GDPR.</li>
        </ol>

        <h5>2.1.5. Recommendation of the most suitable product size</h5>
        <p>Based on the factors you have provided, we recommend the most suitable size. We store this personal data for 40 days. We store the size you have ordered with the order information for 18 months. We process your personal data for the purpose of carrying out a contract with you, based on Art. 6.1(b) GDPR.</p>
        <h5>2.1.6. Processing of your data for advertising purposes and retargeting on third-party websites and in social networks</h5>
        <ol>
            <li>When you visit our website, we process your personal data for advertising activities in the form of remarketing and display advertising.</li>
            <li>For remarketing purposes, we use the services of Google Analytics (sub-section 2.1.3.) and Google Adwords (sub-section 2.1.4.). We process your personal data based on our legitimate interest to advertise our products and services, on the basis of Art. 6.1(f) GDPR.</li>
            <li>With regard to display advertising, we conduct various marketing campaigns using tags (Pixel) and cookie of our retargeting providers. When our websites are called up, tags and cookies are set by retargeting providers engaged by us, and are associated with products you have called up or purchased. In this way, you can be shown individual contents on Larasport products can be shown. We process your personal data based on our legitimate interest to advertise our products and services, using targeted marketing activities, on the basis of Art. 6.1(f) GDPR. Should you not wish the retargeting function, you can disable this at any time by making the appropriate settings (opt-out function) under https://adssettings.google.com.</li>
            <p>For further details on the processing of data by retargeting providers engaged by us, please refer to the corresponding privacy policy under: https://policies.google.com/privacy</p>
        </ol>

        <h5>2.1.7. Use of cookies</h5>
        <ol>
            <li>When you use our websites, cookies are stored on your computer. Cookies are small text files that are stored on your hard disk and allocated to the browser you have used. Using cookies, certain information flows to the location that sets the cookie. In this way, our websites can be made more user friendly and effective. Cookies cannot execute any programs or transfer any viruses to your computer.</li>
            <li>For the use of cookies on the websites of our online shop, the cookie privacy policy of Larasport applies (retrievable under this link).</li>
        </ol>
        <h5>2.1.8. Customer care</h5>
        <ol>
            <li>If you have any questions relating to products, purchase thereof, your customer account as well as other products and services of Larasport, if you would like to exercise your rights under this privacy policy or make a complaint, you can contact our customer service [link].</li>
            <li>Depending on the subject matter of your request, we will rely on your personal data that has been stored within the scope of other data processing activities in our systems (e.g. data that you have provided during a purchase, or your score value that we have received from the credit agency as part of the credit assessment process. If and to the extent that this is necessary to answer your query, we will also collect data from external sources (e.g. query with dispatch service provider as part of a shipment tracking or an investigation order).</li>
            <li>We process your personal data for the purpose of carrying out a contract with you, based on Art. 6.1(b) GDPR. If you wish to exercise your rights against us, we will process your personal data for the purpose of fulfilling a legal obligation on the basis of Art. 6.1(c) GDPR. If you would like to receive information or complain about our products and services, we will process your personal data based on our legitimate interest to carry out marketing activities and respond to your complaint, on the basis of Art. 6.1(f) GDPR.</li>
        </ol>


        <h4>2.2. Informative e-mails and newsletters</h4>
        <ol>
            <li>When you visit our websites for purely informational purposes (sub-section 2.1.) or if you set up a customer account (sub-section 2.4.), you can register to receive our newsletter (at the same time providing your consent). Through the newsletter, we will inform you about our products and services and advertise these. In so doing, we will use your data for advertising purposes, with the objective of sending you information about products from our range. These may also be selected on the basis of your most recent orders. In so doing, we process your personal data for marketing purposes based on your consent within the meaning of Art. 6(1a) GDPR.</li>
            <li>After the purchase of goods, we may send you individual newsletters, on similar products, services and promotions. We process your personal data based on our legitimate interest to advertise our products and services and to conduct marketing activities, on the basis of Art. 6.1(f) GDPR.</li>
            <li>The data required for sending newsletters and product recommendations or for advertising with regard to existing customer relationships, will be transferred to the enterprise engaged by Larasport to dispatch these. From these partners, Larasport in turn receives acknowledgements that e-mails have been received and read, information about computers and Internet access, the operating system and the platform, date and time of the visit to our websites, and on products that you have viewed.</li>
        </ol>
        <h4>2.3. Product reviews on the Larasport website</h4>
        <ol>
            <li>We offer you the chance to review any (purchased) products on our websites. Your feedback helps other customers to make the right purchasing decision and enables us to improve our products constantly. If you would like to submit a review for one of our products, we will process your e-mail address, your nickname and also the content of your review (e.g. reviewed product, star rating, title and text of review, recommendation). Your e-mail address will be processed in order to check and establish your identity. It may also be used by our customer service in order to reply to your feedback by e-mail. If, before submitting your review, you have duly given your consent, your email address will also be processed in order to send you a notification when your review is published. Ad your (star) rating and the content of your review will be published alongside your nickname on our websites, please ensure that you do not give any personal information in this.</li>
            <li>We will process your personal data based on your consent within the meaning of Art. 6.1(a) GDPR, based on our legitimate interest, namely that of customer service and marketing, based on Art. 6.1(f) GDPR, and for the purpose of fulfilling and processing the contract with you based on Art. 6.1(b) GDPR.</li>
        </ol>
        <h4>2.4. Registration and setting up a customer account</h4>
        <ol>
            <li>When you visit our websites, you can set up a customer account. You may use this to purchase our products and to obtain information about our products. You can take part in exclusive pre-sales, subscribe to newsletter and exploit many other advantages in relation to your purchase.</li>
            <li>The registration and use of the customer account requires you to provide personal data. Mandatory fields are marked accordingly in the input mask.</li>
            <li>We will process your personal data in order to allow you to set up and use your customer account, to allow you to purchase our products, and, based on our legitimate interest, to inform you about our products and services. Data is processed on the basis of Art. 6.1(b) and (f) GDPR.</li>
        </ol>

        <h4>2.5. Data processing in the case of orders in the online shop</h4>
        <p>In addition, we process your personal data in connection with the purchase of goods in our online shop.</p>

        <h5>2.5.1. Purchase and payment of goods in the online shop</h5>
        <ol>
            <li>We purchase your personal data if you purchase goods in the online shop on our websites (retrievable under http://larasport.com), and in so doing provide your personal data. Data is processed for the purpose of fulfilling and processing the contract with you on the basis of Art. 6.1(b) GDPR.</li>
            <li>If you purchase goods for another person (third party), we process the personal data of the third party provided by you (name and any contact details) for the dispatch of the goods to the third party. This data is processed for the purpose of fulfilling and processing the contract with you on the basis of Art. 6.1(b) GDPR. If, when making a purchase, you provide data of a third party, please ensure that the third party has been sufficiently informed by you about the processing of his data at Larasport, and that you are authorised to provide such data.</li>
        </ol>

        <h5>2.5.2. Shopping cart bounce e-mails</h5>
        <p>If you have started an ordering process in our online shop on our websites but have not concluded this, we will send you a reminder e-mail to the e-mail address stored in the customer account regarding the purchasing process initiated by you. We process your personal data based on our legitimate interest to remind you of any purchasing processes that you have not yet completed, on the basis of Art. 6.1(f) GDPR.</p>

        <h5>2.5.3. Cancellation of purchase</h5>
        <p>If you cancel your order, exercise your right to cancel the purchase contract concluded with us, or to withdrawn from the contract you have concluded with us, we will process your personal data for the purpose of cancelling your order and/or purchase, for returning the goods and refunding the purchase price based on Art. 6.1(b) GDPR.</p>

        <h5>2.5.4. Invitation e-mail for product assessment via Bazaarvoice</h5>
        <p>We would like to know if you are satisfied with the purchased goods. For this purpose, we process your e-mail and purchasing data (e.g. goods purchased and date of purchase), in order to send you an e-mail within one month after the purchase, inviting you to rate your purchase (further information on data processing in connection with the submission of product reviews can be found in the section “Customer reviews”). We process your personal data based on our legitimate interest of customer service and marketing, on the basis of Art. 6.1(f) GDPR.</p>
        <p>You can object to the receipt of this e-mail inviting you to evaluate the product from the start, by clicking on the opt-out button under the following link (<a
                    href="https://www.bazaarvoice.com/legal/privacy-policy/#opting-out" target="_blank">https://www.bazaarvoice.com/legal/privacy-policy/#opting-out</a>). A persistent opt-out cookie will then be stored in your browser, preventing your purchase data from being transferred to our evaluation service provider, Bazaarvoice.</p>
        <p>If you have already received an e-mail inviting you to evaluate the product, you can revoke the future receipt of such e-mails, likewise by clicking on the “opt-out” button in the invitation e-mail.</p>

        <h5>2.5.5. Dunning process, collection and enforcement and/or defence of legal claims</h5>
        <ol>
            <li>In the case of outstanding amounts owed to us, we will advise you accordingly by e-mail, sms, letter or by phone, and under certain circumstances will send you a dunning letter. If and to the extent that you still fail to make the payment, we will initiate a credit collection procedure.</li>
            <li>The credit collection procedure will be carried out by a credit collection agency engage by us. If necessary to carry out the credit collection procedure, the credit collection agency will carry out address enquiries, thereby availing itself of public registers in order to locate you as a debtor.</li>
            <li>We process your personal data for the purpose of fulfilling and processing the contract with you, based on Art. 6.1(b) GDPR, and based on our legitimate interest in preventing the abuse of our services and enforcing our legal rights, on the basis of Art. 6.1(f) GDPR.</li>
            <li>In the case of a legal dispute with you, we will process your personal data to enforce and/or defend our rights. If and to the extent necessary for the legal dispute, we will thereby rely also on data for other sources (e.g. public registers). We process your personal data based on a legal obligation, on the basis of Art. 6.1(c) GDPR, and based on our legitimate interest to safeguard, enforce and/or defend our legal interests on the basis of Art. 6.1(f) GDPR.</li>
        </ol>
        <h3>Other agreements</h3>

        <h4>2.6.1. Performance of internal audits</h4>
        <ol>
            <li>Within the scope of audits within the Larasport Group both at home and abroad, your personal data may be processed. In this process we rely also on data from other sources, depending on the case (e.g. credit agencies).</li>
            <li>Your data may, under certain circumstances, be appropriately processed in order to identify and rectify misconduct within the enterprise, and to implement compliance programmes and compliance measures.</li>
            <li>We process your personal data for the purpose of meeting our legally prescribed obligations (e.g. under the Stock Corporate Act), on the basis of Art. 6.1(c) GDPR. In addition, we process your personal data based on our legitimate interest to check the processes and efficiency in the group of undertakings, to rectify misconduct and cases of fraud, to enforce and/or defend our rights, as the case may be, and to uncover any criminal offences, on the basis of Art. 6.1(f) GDPR.</li>
        </ol>

        <h4>2.6.2. Issuing of analyses</h4>
        <p>On the basis of your data, which we process within the meaning of sub-section 2 of this privacy policy, we may issue analyses. These serve as a basis for our business decisions, to improve our products and services, to adapt to the needs of our customers and to carry out marketing activities. We process your personal data based on our legitimate interest to improve the products and services we offer and carry out marketing activities, on the basis of Art. 6.1(f) GDPR. The analyses issued on this basis no longer have any personal reference, which means it is no longer possible to trace them back to you.</p>

        <h3>3. Retention and erasure of your personal data</h3>
        <ol>
            <li>We keep your personal data for as long and to the extent required for the purposes named in this privacy policy (section 2).</li>
            <li>As soon as the data for the purposes named in section 2. is no longer required, we keep your personal data for the length of time in which you can assert claims against us, we can claim them against you (the statutory period of limitations is generally three years, starting with the end of the year in which the claim arises, e.g. the end of the year of purchase).</li>
            <li>In addition to this, we store your personal data for as long and to the extent we are obliged to do so by law. Corresponding obligations of proof and storage can be found, inter alia, from the German Commercial Code, the Tax Code and the Money Laundering Act. The retention periods may accordingly last up to ten years.</li>
        </ol>
        <h3>4. Transfer of personal data and the categories of recipients</h3>
        <ol>
            <li>When you visit our website and purchase goods, we transfer your personal data, within the scope of an internal, collaborative process in the group, to enterprises within the Larasport Group. Data is transferred based on our legitimate interest to run our administration activities efficiently and collaboratively, and to improve our products and services on the basis of Art. 6.1(f) GDPR.</li>
            <li>In addition, we transfer your personal data to our processors, who process data on our behalf:
                <ul>
                    <li>IT Service providers who prepare the platforms, databases and tools for our products and services (e.g. our website, the sale of goods, the dispatch of newsletters and informative e-mails), issue analyses on user habits on our websites, carry out marketing campaigns, and process your personal data during the purchasing process on our behalf.</li>
                    <li>In connection with the use of Google Analytics and Google Adwords, including tags and cookies, your personal data may be transferred to the USA. In the case of the USA, the EU Commission has not decided that there is an adequate level of data protection within the meaning of the GDPR; such an adequacy decision (Art. 45 GDPR) has not been made. Google LLC is, however, subject to the EU-U.S. Privacy Shield. This means that adequate protection of your personal data is guaranteed. You can retrieve the full text of the US Privacy Shield Framework under the following link: https://www.privacyshield.gov/servlet/servlet.FileDownload?file=015t00000004qAg</li>
                    <li>For the analyses of your behaviour on our website, we transfer your personal data to specialised service providers. Data is transferred to third countries. In order to guarantee adequate protection of your personal data, EU standard contract clauses within the meaning of Art. 46.5 (2) GDPR. You can find information on EU standard contract clauses under http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2010:039:0005:0018:DE:PDF.</li>
                    <li>In order to offer you localised versions of our websites, we transfer your personal data to a third-party service provider in the USA. In the case of the USA, the EU Commission has not decided that there is a suitable level of data protection within the meaning of the GDPR; such an adequacy decision (Art. 45 GDPR) has not been made. The third-party service provider is, however, subject to the EU-U.S. Privacy Shield. This means that adequate protection of your personal data is guaranteed. You can retrieve the full text of the US Privacy Shield Framework under the following link: https://www.privacyshield.gov/servlet/servlet.FileDownload?file=015t00000004qAg</li>
                    <li>In order to carry out the credit and fraud check, we transfer your personal data to specialised service providers, generally to infoscore Consumer Data GmbH. In certain circumstances, data may be transferred to the USA. In the case of the USA, the EU Commission has not decided that there is a suitable level of data protection within the meaning of the GDPR; such an adequacy decision (Art. 45 GDPR) has not been made. In order to guarantee adequate protection of your personal data, EU standard contractual clauses within the meaning of Art. 46.5 (2) GDPR. You can find information on EU standard contract clauses under http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2010:039:0005:0018:DE:PDF.</li>
                    <li>If you purchase or evaluate products on our websites, we will forward your personal data to our third-party service provider of customer feedback platforms, located in the USA. In the case of the USA, the EU Commission has not decided that there is a suitable level of data protection within the meaning of the GDPR; such an adequacy decision (Art. 45 GDPR) has not been made. In order to guarantee adequate protection of your personal data, we have used EU standard contractual clauses within the meaning of Art. 46.5 (2) GDPR. You can find information on EU standard contract clauses under http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2010:039:0005:0018:DE:PDF.</li>
                    <li>If we send products to you by mail, we transfer your personal data to dispatch agencies.</li>
                </ul>
            </li>
            <li>In addition, we transfer your personal data to third parties:
                <ul>
                    <li>If you purchase products on our websites, we offer you various payment methods. In order to process a payment and, if necessary, a refund of the purchase price, we transfer your personal data, depending on the chosen payment method, to banks, payment service providers, financial service providers and credit card companies. We process your personal data for the purpose of fulfilling a contract with you, based on Art. 6.1(b) GDPR. In certain circumstances, data may be transferred to the USA. In the case of the USA, the EU Commission has not decided that there is a suitable level of data protection within the meaning of the GDPR; such an adequacy decision (Art. 45 GDPR) has not been made. In order to guarantee adequate protection of your personal data, EU standard contractual clauses within the meaning of Art. 46.5 (2) GDPR. You can find information on EU standard contract clauses under http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2010:039:0005:0018:DE:PDF.</li>
                    <li>If you choose “PayPal” as the payment method for a purchase, we transfer your personal data to the USA: Your personal data is processed on the basis of your chosen payment method and to process the payment with you. In the case of the USA, the EU Commission has not decided that there is a suitable level of data protection within the meaning of the GDPR; such an adequacy decision (Art. 45 GDPR) has not been made. We process your personal data for the purpose of fulfilling a contract with you, based on Art. 6.1(b) in conjunction with Art. 49.1 (b) and (c) GDPR.</li>
                    <li>Should you be unable to meet your payment obligations, we transfer your personal data to collection agencies that carry out the collection procedure on our behalf. Your personal data is transferred for the purpose of fulfilling a contract with you, based on Art. 6.1(b) GDPR.</li>
                    <li>In the case of legal disputes, we transfer your data to the competent court and, if you have engaged a lawyer, to the latter, in order to conduct the legal dispute. We process your personal data based on a legal obligation, on the basis of Art. 6.1(c) GDPR, and based on our legitimate interest to safeguard, enforce and/or defend our legal interests on the basis of Art. 6.1(f) GDPR.</li>
                    <li>In order to deliver the products ordered in our online shop (including notifications of the order delivery status), we transfer your personal data to the fulfilment and dispatch service providers engaged by us (e.g. DHL, UPS, Hermes, etc.). This transfer takes place for the purpose of fulfilling a contract with you, based on Art. 6.1(b) GDPR.</li>
                </ul>
            </li>
            <li>In addition to this, we only transfer your personal data if we are legally obliged to forward such data. Your personal data is transferred for the purpose of fulfilling a contract with you, based on Art. 6.1(c) GDPR (e.g. to the police authorities within the scope of criminal investigations or to the data protection supervisory authorities).</li>
        </ol>
        <h3>5. Legitimate interests in data processing and cancellation</h3>
        <ol>
            <li>We process your personal data within the meaning of sub-section 2, based on our legitimate interest, in particular to guarantee IT security on our websites, to adapt our website to your needs, to carry out analyses and marketing activities, to inform you about our products and services, to remind you of any purchasing processes that have not yet been completed, to increase the coverage of our products and marketing activities, to prevent fraud and abuse, to avoid payment defaults, to look after our customers, to safeguard, enforce and defend our legal interests (also before the courts, as necessary), and to carry out our internal management efficiently and collaboratively. For information about the consideration of interests made, please contact datenschutz@larasport.com.</li>
            <li>If we process your personal data based on our legitimate interest (Art. 6.1(f) GDPR), you can object at any time to the data processing. We will meet your demand for cancellation, unless important reasons stand in the way of this within the meaning of Art. 21 GDPR. Please send your request to privacy-support@larasport.com.</li>
            <li>If you object to the processing of your data, in this respect we will process any collected personal data in order to respond to your request. Your personal data is processed to fulfil a legal obligation based on Art. 6.1(c) GDPR.</li>
        </ol>
        <h3>6. Consent and revocation of your consent</h3>
        <ol>
            <li>If you have consent to us processing your personal data, you can revoke this at any time. The revocation of your consent will have effect for the future. The legitimacy of the processing of your personal data up to the time of revocation remains unaffected. Please address your revocation to privacy-support@larasport.com.</li>
            <li>If you revoke your consent, in this respect we process your data in connection with collected personal data in order to respond to your request. We process your personal data for fulfilling a legal obligation based on Art. 6.1(c) GDPR.</li>
        </ol>

        <h3>7. Your rights</h3>
        <ol>
            <li>In accordance with the GDPR, you may demand at any time that we
                <ul>
                    <li>provide you with information on your personal data that we process (Art. 15 GDPR),</li>
                    <li>that we rectify any of your personal data (Art. 16 GDPR) and/or</li>
                    <li>that we erase (Art. 17 GDPR), block (Art. 18 GDPR) and/or surrender (Art. 20 GDPR) any of your personal data stored on our systems (Art. 17 GDPR).</li>
                </ul>
            </li>
            <li>Please send your request
                <ul>
                    <li>by e-mail to privacy-support@larasport.com or</li>
                    <li>in writing, to Larasport Online Shop, Post box 201101, 48092 Münster, Germany.</li>
                </ul>
            </li>
            <li>If you assert your rights against us, in this respect we will process any collected personal data in order to respond to your request. Your personal data is processed to fulfil a legal obligation based on Art. 6.1(c) GDPR.</li>
            <li>Irrespective of your rights under sub-section 7, you can submit a complaint at a supervisory authority for data protection, if you are of the opinion that the processing of your personal data by Larasport violates the GDPR (Art. 77 GDPR).</li>
        </ol>

        <h3>8. Changes to this privacy policy</h3>
        <ol>
            <li>The provisions of this privacy policy (which may be retrieved free-of-charge under http://larasport.com/privacy-policy) apply, including the cookie data protection information of Larasport Europe GmbH (retrievable free-of-charge under this link) in the version in force at the time the online shop is used.</li>
            <li>We reserve the right to supplement and modify the content of the privacy policy. The updated privacy policy applies from the time in which it was published on our websites.</li>
            <li>We will inform you promptly of these additions and modifications on our websites and, if we already have your contact information on file, will inform you via e-mail or by post. You have the opportunity to inspect, print out and store the modified privacy policy at any time.</li>
        </ol>

    </div>
@endsection