<section id="featured-products">
        <div class="container">
            <div class="row">
            
    <h2>Featured <span>Products</span></h2>
    <a href="/shop" class="all-products btn default">All Products 
        <span>
            <svg width="20" height="11"> 
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrwIcn">
                    <svg id="arrwIcn" viewBox="0 0 21.5 11.5" width="100%" height="100%"><path d="M13.8,11.5c-0.2,0-0.5-0.1-0.6-0.3c-0.2-0.3-0.2-0.8,0.2-1l6.1-4.4l-5-3.5v2.5c0,0.4-0.3,0.8-0.8,0.8h-13 C0.3,5.5,0,5.2,0,4.8S0.3,4,0.8,4H13V0.8c0-0.3,0.2-0.5,0.4-0.7C13.7,0,14,0,14.2,0.1l7,5c0.2,0.1,0.3,0.4,0.3,0.6s-0.1,0.5-0.3,0.6 l-7,5C14.1,11.5,13.9,11.5,13.8,11.5z"></path></svg>
                </use> 
            </svg>
        </span>
    </a>
    <div class="featured-product-wrapper">
        @foreach ($featured as $product)
            <div class="col-sm-4 featured-product product">
                <div class="featured-product-img">
                    <a href="{{ route('shop.show', $product->slug) }}">
                        <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">
                    </a>
                    @if($product->inventory = 0)
                        <div class="sold-out">
                            <img src="{{ asset('/images/sold-out.png') }}" alt="sold out">
                        </div>

                    @endif
                </div>
                <div class="featured-product-desc">
                    <a href="{{ route('shop.show', $product->slug) }}"><h3>{{ $product->name }}</h3></a>
                    {{--<p><span class="vrnr">{{ $product->presentPrice() }}</span>--}}
                    @if($product->on_sale)
                    <p>
                        @if($product->new)
                            <span class="new-product">NEW</span>
                        @endif
                        <span class="vrnr">{{ formatPrice($product->sale_price) }}</span>
                        <span class="reduced">{{ formatPrice($product->price) }}</span>
                    </p>
                    @else
                        <p>
                            @if($product->new)
                                <span class="new-product">NEW</span>
                            @endif
                            <span class="vrnr regular-price">{{ formatPrice($product->price) }}</span>
                        </p>
                    @endif
                        <a href="{{ route('shop.show', $product->slug) }}" class="btn red">Shop Now</a>
                </div>
                <span class="product-fav">
                    <favorite
                            :product={{ $product->id }}
                            :favorited={{ $product->favorited() ? 'true' : 0 }}>
                    </favorite>
                </span>
            </div>
        @endforeach
    </div>

</div>
</div>
</section>