<section id="above-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 three-col-icon">
                    <div class="icon"><i class="fab fa-cc-stripe"></i></div>
                    <div class="three-col-icon-text">
                        <h4>PAYMENT METHODS</h4>
                        <p>Credit cards, through the account, on delivery</p>
                    </div>
                </div>
                <div class="col-sm-4 three-col-icon">
                    <div class="icon"><i class="fas fa-truck"></i></div>
                    <div class="three-col-icon-text">
                        <h4>FREE SHIPPING</h4>
                        <p>Delivery is free in the territory of Serbia</p>
                    </div>
                </div>
                <div class="col-sm-4 three-col-icon">
                    <div class="icon"><i class="fas fa-users"></i></div>
                    <div class="three-col-icon-text">
                        <h4>LOYALTY PROGRAM</h4>
                        <p>For whole Larasport community</p>
                    </div>
                </div>
            </div>
        </div>
    </section>