<div class="might-also-like">
    <h2 class="font-graphik-black">Might also <span>like </span></h2>
    @forelse($migtAlsoLike->chunk(4) as $chunk)

        {{--                        @forelse ($wishlist->chunk(3) as $chunk)--}}
        <div class="product-grid-flex clearfix">
            @foreach ($chunk as $product)

                <div class="col-sm-3 product">
                    <div class="product-grid-item">
                        <div class="product-img">
                            <a href="{{ route('shop.show', $product->slug) }}">
                                <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">
                            </a>
                        </div>
                        <div class="product-desc">
                            <h3><a href="{{ route('shop.show', $product->slug) }}">{{ $product->name }}</a></h3>
                            @if($product->on_sale)
                                <p>
                                    @if($product->new)
                                        <span class="new-product">NEW</span>
                                    @endif
                                    <span class="vrnr">{{ formatPrice($product->sale_price) }}</span>
                                    <span class="reduced">{{ formatPrice($product->price) }}</span>
                                </p>
                            @else
                                <p>
                                    @if($product->new)
                                        <span class="new-product">NEW</span>
                                    @endif
                                    <span class="vrnr regular-price">{{ formatPrice($product->price) }}</span>
                                </p>
                            @endif

                            <a href="{{ route('shop.show', $product->slug) }}" class="btn red">Shop Now</a>
                        </div>
                        <span class="product-fav">
                            <favorite
                                    :product={{ $product->id }}
                                    :favorited={{ $product->favorited() ? 'true' : 0 }}>
                            </favorite>
                        </span>
                    </div>
                </div>

            @endforeach
        </div>
        {{--@endforeach--}}
    @empty
        <span>You have no items in your wish list.</span>
    @endforelse

</div>