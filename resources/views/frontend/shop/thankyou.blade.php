@extends('layouts.app', ['title' => 'Thank you page', 'bodyClass' => 'thank-you-page'])
@section('content')
    @if(session()->has('order_success'))

        <h2>{!! session()->get('order_success') !!} </h2>
        <a href="/" class="btn">CONTINUE SHOPPING</a>
    @else
        <h2>You're on the wrong page.</h2>
        <a href="/" class="btn">CONTINUE SHOPPING</a>
    @endif

@endsection