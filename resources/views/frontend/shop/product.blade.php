@extends('layouts.app', ['title' => $product->name, 'bodyClass' => 'single-product'])

@section('content')
    <div class="heading-title">
        <div class="row_triangle row_triangle_top triangle_bkg"></div>
        <div class="heading-title-wrapper">
            <h1>{{ $product->name }}</h1>
            <p></p>
        </div>
    </div>
    <div class="cart-messages container">
        {{--@if(session()->has('success_message'))--}}
            {{--<div class="alert alert-success">--}}
                {{--{{ session()->get('success_message') }}--}}
            {{--</div>--}}
        {{--@endif--}}
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
<section id="single-product-page">
    <div class="container">
        <div class="row">
            <div class="product-wrapper clearfix">
                <div class="product-info-main">
                    <form action="{{ route('cart.store') }}" method="POST">
                        {{ csrf_field() }}
                        @php
                        $avg = $reviews->avg('rating') * 20;
                        @endphp
                    <h1 class="product-name">{{ $product->name }}</h1>
                    <div class="product-reviews-summary">
                        <div class="rating-summary">
                            <div class="star-rating" title="{{ $avg }}%">
                                <div class="back-stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <div class="front-stars" style="width: {{ $avg }}%">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="reviews-actions">
                            <a class="action view" href="#"><span>{{ $reviews->count() }}</span>&nbsp;<span>Reviews</span></a>
                            <a class="action add" href="#">Add Your Review</a>
                        </div>
                    </div>
                    <div class="product-info-price">
                        <div class="price-box">
                            @if($product->on_sale)
                                <span class="price-reduced">{{ formatPrice($product->sale_price) }}</span>
                                <span class="price-regular faded">{{ formatPrice($product->price) }}</span>
                            @else
                                <span class="price-regular">{{ formatPrice($product->price) }}</span>
                            @endif
                        </div>
                        <div class="product-info-stock-sku">
                            <div class="stock" title="Availability">
                                @if($product->inventory > 0)
                                <span>In stock</span>
                                @else
                                <span class="not-in-stock">Out of stock</span>
                                @endif
                            </div>

                            <div class="sku">
                                <span class="type">SKU#: {{ $product->sku }}</span>

                            </div>
                        </div>
                    </div>
                    <div class="available-colors-sizes inline-block clearfix">
                        <div class="av-colors">
                            <h3>AVAILABLE COLORS</h3>
                            <div id="color" class="wrapper">
                                @php
                                    $filtered = $atts->where('attribute_id', 1);
                                @endphp
                                @if($product->inventory > 0)
                                    @forelse($filtered as $key=> $value)
                                        <div>
                                            <input type="radio" name="radio_color" id="radio{{$key}}" class="radio-color {{ $value->value }}" value="{{ $value->value }}" {{ $key == 0 ? 'checked' : '' }}/>
                                            <label for="radio{{$key}}">&nbsp;</label>
                                        </div>
                                    @empty
                                        <span>Sorry, but we don't have information about colors for this product.</span>
                                    @endforelse
                                @else
                                    <span>Sorry, but we don't have information about colors for this product.</span>
                                @endif

                            </div>

                        </div>
                        <div class="av-sizes">
                            <h3>AVAILABLE SIZES</h3>
                            <div id="sizes" class="wrapper">

                                @php
                                $sizes = $atts->where('attribute_id', '<>', 1);
                                $i = 0;
                                @endphp
                                @if($product->inventory > 0)
                                    @forelse($sizes as $key=> $value)
                                        <div>
                                            <input id="radio-size{{$key}}" type="radio" name="radio_size" class="radio-size" value="{{ $value->value }}" {{ $i == 0 ? 'checked' : '' }}/>
                                            <label for="radio-size{{$key}}">{{ $value->value }}</label>
                                        </div>
                                        @php
                                        $i++;
                                        @endphp
                                    @empty
                                        <span>Sorry, but we don't have information about sizes for this product.</span>
                                    @endforelse
                                @else
                                    <span>Sorry, but we don't have information about sizes for this product.</span>
                                @endif

                                <div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="product-size-qty clearfix">
                        <div class="field qty">
                            <h3>QUANTITY</h3>
                            <div class="control">
                                <qtyy :numqty={{ 1 }}
                                        :inventory={{ $product->inventory ? $product->inventory : 0}}>
                                </qtyy>
                                {{--<input type="number" name="qty" id="qty" maxlength="12" value="1" title="Qty" class="input-text qty">--}}
                                <span class="product-fav">
                                <favorite
                                        :product={{ $product->id }}
                                                :favorited={{ $product->favorited() ? 'true' : 0 }}>
                                </favorite>
                            </span>
                            </div>
                        </div>
                    </div>

                      <div class="box-tocart-share">
                            <div class="actions">
                                <input type="hidden" name="id" value="{{ $product->id }}">
                                <input type="hidden" name="name" value="{{ $product->name }}">
                                @if($product->on_sale)
                                    <input type="hidden" name="price" value="{{ $product->sale_price }}">
                                    {{--<input type="hidden" name="old_price" value="{{ $product->price }}">--}}
                                @else
                                    <input type="hidden" name="price" value="{{ $product->price }}">
                                @endif
                                <input type="hidden" name="inv" value="{{ $product->inventory }}">
                                @if($product->inventory > 0)
                                    <button type="submit" title="Add to Cart" class="btn action tocart" id="addtocart-button" {{ $product->inventory == 0 ? 'disabled' : '' }}>Add to Cart</button>
                                @endif
                            </div>
                            {{-- product fav --}}
                            <div class="product-social-links">
                                <div class="social-links">
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?{{ url()->current() }}"><i class="fab fa-facebook-f"></i></a>
{{--                                    <a target="_blank" href="https://www.facebook.com/dialog/share?app_id=404221186424524&display=popup&href={{url()->current()}}&title={{$product->name}}&description={{$product->details}}"><i class="fab fa-facebook-f"></i></a>--}}
                                    <a target="_blank" href="https://twitter.com/intent/tweet?text={{$product->name}}&url={{url()->current()}}&related="><i class="fab fa-twitter"></i></a>
{{--                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ url()->current() }}&title={{ $product->name }}&summary={{ $product->details }}"><i class="fab fa-linkedin-in"></i></a>--}}
                                    <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url={{url()->current()}}"><i class="fab fa-linkedin-in"></i></a>
                                </div>
                            </div>
                      </div>
                </form>
                    </div>

                <div class="product-media">
                    <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">

                </div>
            </div>

            <div class="clearfix"></div>

            <div class="tabs-section">
                <div class="cd-tabs js-cd-tabs">
                    <nav>
                        <ul class="cd-tabs__navigation js-cd-navigation">
                            <li><a data-content="desc" class="cd-selected" href="#0">Description</a></li>
                            <li><a data-content="info" href="#0">More Information</a></li>
                            <li><a data-content="reviews" href="#0">Customer Reviews ({{ $reviews->count() }})</a></li>
                        </ul> <!-- cd-tabs__navigation -->
                    </nav>
                    <ul class="cd-tabs__content js-cd-content">
                        <li data-content="desc" class="cd-selected">
                            <p>{!! $product->details !!} </p>

                            <p></p>
                        </li>
                        <li data-content="info">
                            <p>{!! $product->description !!}</p>
                        </li>
                        <li data-content="reviews" class="reviews">

                            @if($reviews->count() > 0)
                            <h2>Customer Reviews</h2>
                            <div class="customer-review-list">
                                @foreach($reviews as $review)

                                <div class="review-item">
                                    <div class="review-title"><strong>{{ $review->summary }}</strong></div>
                                    <div class="review-ratings">
                                        <div class="rating-summary">
                                            <span class="rating-label"><span>Rating</span></span>
                                            <div class="star-rating" title="{{ ($review->rating)/5 * 100 }}%">
                                                <div class="back-stars">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <div class="front-stars" style="width: {{ ($review->rating)/5 * 100 }}%">
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                        <i class="fas fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="review-content">
                                        <p>{{ $review->review }}</p>

                                        <div class="review-details">
                                            <p class="review-author">By <strong class="review-details-value">{{ $review->nickname }} </strong>
                                                <time class="review-date">{{ $review->created_at->format('m/d/Y') }}</time>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                @endforeach

                            </div>
                            @endif
                            <div class="review-form-wrapper">
                                <span class="you-reviewing">You're reviewing:</span>
                                <h4>{{ $product->name }}</h4>
                                <br>
                                <div class="row">
                                    <div class="col-sm-6">
                                    <form action="{{ route('review.store') }}" method="POST" class="review-form" id="review-form">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">

                                        <div class="form-group{{ $errors->has('nickname') ? ' has-error' : '' }}">
                                            <div class="radio-rating-group">
                                            <label for="rating" class="control-label">Your Rating <span class="asterisk">*</span></label>
                                            <fieldset>
                                                <span class="star-cb-group">
                                                  <input type="radio" id="rating-5" class="rating-radio" name="rating" value="5" /><label for="rating-5" class="fas">5</label>
                                                  <input type="radio" id="rating-4" class="rating-radio" name="rating" value="4" checked="checked" /><label for="rating-4" class="fas">4</label>
                                                  <input type="radio" id="rating-3" class="rating-radio" name="rating" value="3" /><label for="rating-3" class="fas">3</label>
                                                  <input type="radio" id="rating-2" class="rating-radio" name="rating" value="2" /><label for="rating-2" class="fas">2</label>
                                                  <input type="radio" id="rating-1" class="rating-radio" name="rating" value="1" /><label for="rating-1" class="fas">1</label>
                                                </span>
                                            </fieldset>
                                            </div>

                                            @if ($errors->has('nickname'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('nickname') }}</strong>
                                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('nickname') ? ' has-error' : '' }}">
                                            <label for="nickname" class="control-label">Nickname <span class="asterisk">*</span></label>
                                            <input id="nickname" type="test" class="form-control" name="nickname" value="" placeholder="Nickname" required>

                                            @if ($errors->has('nickname'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('nickname') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('summary') ? ' has-error' : '' }}">
                                            <label for="summary" class="control-label">Summary <span class="asterisk">*</span></label>
                                            <input id="summary" type="text" class="form-control" name="summary" value="" placeholder="Summary" required>

                                            @if ($errors->has('summary'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('summary') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('Review') ? ' has-error' : '' }}">
                                            <label for="review" class="control-label">Review <span class="asterisk">*</span></label>
                                            <textarea rows="8" id="review" class="form-control" name="review" value="" placeholder="Review" required></textarea>

                                            @if ($errors->has('review'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('review') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn default">Submit Review</button>
                                        </div>

                                    </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul> <!-- cd-tabs__content -->
                </div>
            </div> <!-- cd-tabs -->

            <div class="clearfix"></div>

            @include('frontend.shop.partials.might-also-like')


        </div>
    </div>
</section>
<section id="category-slider">

</section>

@endsection
@section('above-footer')
    @include('frontend.shop.partials.above-footer')
@endsection
@section('footer')
    <script>
        (function(){
            // Responsive Tabbed Navigation - by CodyHouse.co
            function TabbedNavigation( element ) {
                this.element = element;
                this.navigation = this.element.getElementsByTagName("nav")[0];
                this.navigationElements = this.navigation.getElementsByClassName("js-cd-navigation")[0];
                this.content = this.element.getElementsByClassName("js-cd-content")[0];
                this.activeTab;
                this.activeContent;
                this.init();
            };

            TabbedNavigation.prototype.init = function() {
                var self = this;
                //listen for the click on the tabs navigation
                this.navigation.addEventListener("click", function(event){
                    event.preventDefault();
                    if(event.target.tagName.toLowerCase() == "a" && !hasClass(event.target, "cd-selected")) {
                        self.activeTab = event.target;
                        self.activeContent = self.content.querySelectorAll("[data-content="+self.activeTab.getAttribute("data-content")+"]")[0];
                        self.updateContent();
                    }
                });

                //listen for the scroll in the tabs navigation
                this.navigation.addEventListener('scroll', function(event){
                    self.toggleNavShadow();
                });
            };

            TabbedNavigation.prototype.updateContent = function() {
                var actualHeight = this.content.offsetHeight;
                //update navigation classes
                removeClass(this.navigation.querySelectorAll("a.cd-selected")[0], "cd-selected");
                addClass(this.activeTab, "cd-selected");
                //update content classes
                removeClass(this.content.querySelectorAll("li.cd-selected")[0], "cd-selected");
                addClass(this.activeContent, "cd-selected");
                //set new height for the content wrapper
                (!window.requestAnimationFrame)
                    ? this.content.setAttribute("style", "height:"+this.activeContent.offsetHeight+"px;")
                    : setHeight(actualHeight, this.activeContent.offsetHeight, this.content, 200);
            };

            TabbedNavigation.prototype.toggleNavShadow = function() {
                //show/hide tabs navigation gradient layer
                this.content.removeAttribute("style");
                var navigationWidth = Math.floor(this.navigationElements.getBoundingClientRect().width),
                    navigationViewport = Math.ceil(this.navigation.getBoundingClientRect().width);
                ( this.navigation.scrollLeft >= navigationWidth - navigationViewport )
                    ? addClass(this.element, "cd-tabs--scroll-ended")
                    : removeClass(this.element, "cd-tabs--scroll-ended");
            };

            var tabs = document.getElementsByClassName("js-cd-tabs"),
                tabsArray = [],
                resizing = false;
            if( tabs.length > 0 ) {
                for( var i = 0; i < tabs.length; i++) {
                    (function(i){
                        tabsArray.push(new TabbedNavigation(tabs[i]));
                    })(i);
                }

                window.addEventListener("resize", function(event) {
                    if( !resizing ) {
                        resizing = true;
                        (!window.requestAnimationFrame) ? setTimeout(checkTabs, 250) : window.requestAnimationFrame(checkTabs);
                    }
                });
            }

            function checkTabs() {
                tabsArray.forEach(function(tab){
                    tab.toggleNavShadow();
                });
                resizing = false;
            };

            function setHeight(start, to, element, duration) {
                var change = to - start,
                    currentTime = null;

                var animateHeight = function(timestamp){
                    if (!currentTime) currentTime = timestamp;
                    var progress = timestamp - currentTime;
                    var val = Math.easeInOutQuad(progress, start, change, duration);
                    element.setAttribute("style", "height:"+val+"px;");
                    if(progress < duration) {
                        window.requestAnimationFrame(animateHeight);
                    }
                };

                window.requestAnimationFrame(animateHeight);
            }

            Math.easeInOutQuad = function (t, b, c, d) {
                t /= d/2;
                if (t < 1) return c/2*t*t + b;
                t--;
                return -c/2 * (t*(t-2) - 1) + b;
            };

            //class manipulations - needed if classList is not supported
            function hasClass(el, className) {
                if (el.classList) return el.classList.contains(className);
                else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
            }
            function addClass(el, className) {
                var classList = className.split(' ');
                if (el.classList) el.classList.add(classList[0]);
                else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
                if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
            }
            function removeClass(el, className) {
                var classList = className.split(' ');
                if (el.classList) el.classList.remove(classList[0]);
                else if(hasClass(el, classList[0])) {
                    var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                    el.className=el.className.replace(reg, ' ');
                }
                if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
            }
        })();
    </script>
@endsection