@extends('layouts.app', ['bodyClass' => 'shop-page'])

@section('content')
<div class="heading-title">
    <div class="row_triangle row_triangle_top triangle_bkg"></div>
    <div class="heading-title-wrapper">
        <h1>Shop</h1>
        <p></p>
    </div>
</div>
<section id="shop-page">
    <div class="container">
        <div class="row">
            <div class="shop-page-container">
                <div class="product-filters">

{{--                    @if(app('request')->input() && (!app('request')->input('page')))--}}
                    @if(app('request')->input())
                        <h3>Applied filters:</h3><br>
                        <div class="applied-filters">
                        {{--@if(app('request')->input('min_price'))--}}
                            {{--@foreach(array_except(request()->query(), ['min_price', 'max-price']) as $key => $value)--}}
                                {{--{{ var_dump(array_except(request()->query(), ['min_price', 'max-price'])) }}--}}
                                {{--<a href="{{ route('shop.index', app('request')->except($key)) }}"><i class="fas fa-times"></i></a> <strong>{{ ucfirst($key) }}</strong> :--}}
                                {{--@foreach($filters->firstWhere('name', $key)['data'] as $filter)--}}
                                    {{--@if($filter->id == $value)--}}
                                        {{--{{ $filter->name }}--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                                {{--<br>--}}
                            {{--@endforeach--}}
                            {{--<a href="{{ route('shop.index', app('request')->flashOnly(['min_price', 'max-price'])) }}"><i class="fas fa-times"></i></a>--}}
                                {{--<strong>Price</strong> : ${{request('min_price')}} - ${{ request('max_price') }}--}}

                        {{--@else--}}
                            {{--@foreach(app('request')->input() as $key => $value)--}}
                            @foreach(array_except(request()->query(), ['page']) as $key => $value)

                                @if($key == 'on_sale')
                                        <a href="{{ route('shop.index', array_except(request()->query(), [$key, 'page'])) }}"><i class="fas fa-times"></i></a> <strong>Sale</strong>
                                        <br>
                                @elseif($key == 'new')
                                    <a href="{{ route('shop.index', array_except(request()->query(), [$key, 'page'])) }}"><i class="fas fa-times"></i></a> <strong>New</strong>
                                    <br>
                                @elseif($key == 'outlet')
                                    <a href="{{ route('shop.index', array_except(request()->query(), [$key, 'page'])) }}"><i class="fas fa-times"></i></a> <strong>Outlet</strong>
                                    <br>
                                @else
                                    <a href="{{ route('shop.index', array_except(request()->query(), [$key, 'page'])) }}"><i class="fas fa-times"></i></a> <strong>{{ ucfirst($key) }}</strong> :
                                {{-- dispaly the name of the applied filter --}}
                                    @foreach($filters->firstWhere('name', $key)['data'] as $filter)
                                        @if($filter->id == $value)
                                            {{ $filter->name }}
                                        @endif
                                    @endforeach
                                    <br>
                                @endif
                            @endforeach
                        {{--@endif--}}
                        </div>
                        <br>
                        <a href="{{ route('shop.index') }}">Clear all filters</a>
                        <br>
                    @else
                        <h3>Filters</h3>
                    @endif
                    <section class="filter-type different">
                        <div class="tab">
                            <input id="brands-filter" type="checkbox" name="brands-filter" checked>
                            <label for="brands-filter" class="filter-title">Various filters</label>
                            <ul class="tab-content">
                                <li class="checkbox-filter {{ app('request')->input('new') ? 'hidden' : ''}}">
                                    <a href="{{ route('shop.index', array_merge(request()->query(), ['new' =>  1, 'page' => 1])) }}">NEW</a>
                                </li>
                                <li class="checkbox-filter {{ app('request')->input('on_sale') ? 'hidden' : ''}}">
                                    <a href="{{ route('shop.index', array_merge(request()->query(), ['on_sale' =>  'yes', 'page' => 1])) }}">SALE</a>
                                </li>
                                <li class="checkbox-filter {{ app('request')->input('outlet') ? 'hidden' : ''}}">
                                    <a href="{{ route('shop.index', array_merge(request()->query(), ['outlet' =>  1, 'page' => 1])) }}">OUTLET</a>
                                </li>
                            </ul>
                        </div>
                    </section>
                    <section class="filter-type categories <?php if (app('request')->input('category')){ echo 'hidden'; } ?>">
                        <div class="tab">
                            <input id="cats-filter" type="checkbox" name="cats-filter" checked>
                            <label for="cats-filter" class="filter-title">Filter by Categories</label>
                            <ul class="tab-content">
                                @foreach($categories as $cat)
                                    @if($cat->products()->count() > 0)
                                    <li class="checkbox-filter">
                                        <a href="{{ route('shop.index', array_merge(request()->query(), ['category' =>  $cat->id, 'page' => 1])) }}">{{ $cat->name }}</a>
                                    </li>
                                    @endif
                                    @foreach($cat->children as $ca)
                                        @if($ca->products()->count() > 0)
                                            <li class="checkbox-filter">
                                                <a href="{{ route('shop.index', array_merge(request()->query(), ['category' =>  $ca->id, 'page' => 1])) }}">{{ $ca->name }}</a>
                                            </li>
                                        @endif
                                            @foreach($ca->children as $lastChild)
                                                @if($lastChild->products()->count() > 0)
                                                        <li class="checkbox-filter">
                                                            <a href="{{ route('shop.index', array_merge(request()->query(), ['category' =>  $lastChild->id, 'page' => 1])) }}">{{ $lastChild->name }}</a>
                                                        </li>
                                                @endif
                                            @endforeach
                                    @endforeach
                                @endforeach
                            </ul>
                        </div>
                    </section>

                    <section class="filter-type brands {{ app('request')->input('brand') ? 'hidden' : ''}}">
                        <div class="tab">
                            <input id="brands-filter" type="checkbox" name="brands-filter" checked>
                            <label for="brands-filter" class="filter-title">Filter by Brand</label>
                            <ul class="tab-content">
                                @foreach($brand as $bra)
                                    @if($bra->products()->get()->count() > 0)
                                    <li class="checkbox-filter">
                                        <a href="{{ route('shop.index', array_merge(request()->query(), ['brand' =>  $bra->id, 'page' => 1])) }}">{{ $bra->name }}</a>
                                    </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </section>
                    {{--<section class="filter-type price-range {{ app('request')->input('min_price') ? 'hidden' : ''}}">--}}
                        {{--<div class="tab">--}}
                            {{--<input id="price-filter" type="checkbox" name="brands-filter" checked>--}}
                            {{--<label for="price-filter" class="filter-title">Price range</label>--}}
                            {{--<ul class="tab-content">--}}
                                {{--<li class="checkbox-filter">--}}
                                    {{--<a href="{{ route('shop.index', array_merge(request()->query(), ['min_price' =>  0, 'max_price' => 39])) }}">0 - $39</a>--}}
                                {{--</li>--}}
                                {{--<li class="checkbox-filter">--}}
                                    {{--<a href="{{ route('shop.index', array_merge(request()->query(), ['min_price' =>  40, 'max_price' => 89])) }}">$40 - $89</a>--}}
                                {{--</li>--}}
                                {{--<li class="checkbox-filter">--}}
                                    {{--<a href="{{ route('shop.index', array_merge(request()->query(), ['min_price' =>  90, 'max_price' => 150])) }}">$90 - $150</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</section>--}}

                </div>
                <div class="product-grid-wrapper">
                    {{-- <h2>Products</h2> --}}
                    <div class="product-grid-header">
                        <label for="sorting">Sort by:</label>
                        <select class="form-control" id="shop_sort">
                            <option disabled="disabled" selected="">Select sort option</option>
                            <option value="low_high_price"><a href="{{ route('shop.index') }}">Low - High Price</a></option>
                            <option value="high_low_price">High - Low Price</option>
                            <option value="a_z_order">A - Z Order</option>
                            <option value="z_a_order">Z - A Order</option>
                            {{-- <option value="average_rating">Average Rating</option> --}}
                        </select>
                        <span>Showing: &nbsp;<span class="product-grid-header-span"> page {{ $products->currentPage() }} of {{ $products->lastPage() }}</span></span><span class="float-right total-products product-grid-header-span">{{ $products->total() }} products</span>
                    </div>
                    @forelse ($products->chunk(3) as $chunk)
                        <div class="product-grid-flex">
                            @foreach ($chunk as $product)

                            <div class="col-sm-4 product">
                                <div class="product-grid-item">
                                    <div class="product-img">
                                        <span class="new"></span>
                                        <span class="sale"></span>
                                        <a href="{{ route('shop.show', $product->slug) }}">
                                            <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">
                                        </a>
                                        @if($product->inventory == 0)
                                            <div class="sold-out">
                                                <img src="{{ asset('/images/sold-out.png') }}" alt="sold out">
                                            </div>

                                        @endif
                                    </div>
                                    <div class="product-desc">
                                        <h3><a href="{{ route('shop.show', $product->slug) }}">{{ $product->name }}</a></h3>
                                        @if($product->on_sale)
                                            <p>
                                                @if($product->new)
                                                    <span class="new-product">NEW</span>
                                                @endif
                                                <span class="vrnr">{{ formatPrice($product->sale_price) }}</span>
                                                <span class="reduced">{{ formatPrice($product->price) }}</span>
                                            </p>
                                        @else
                                            <p>
                                                @if($product->new)
                                                    <span class="new-product">NEW</span>
                                                @endif
                                                <span class="vrnr regular-price">{{ formatPrice($product->price) }}</span>
                                            </p>
                                        @endif

                                        <a class="btn red" href="{{ route('shop.show', $product->slug) }}">Shop Now</a>
                                    </div>
                                    <span class="product-fav">
                                        <favorite
                                            :product={{ $product->id }}
                                            :favorited={{ $product->favorited() ? 'true' : 0 }}>
                                        </favorite>
                                    </span>
                                </div>
                            </div>

                            @endforeach
                        </div>
                    @empty
                        <h3>No products found.</h3>
                    @endforelse
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="product-pagination">
                                    {{--{{ $products->links() }}--}}
                                {{ $products->appends(request()->query())->links() }}

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section id="category-slider">

</section>

@endsection
@section('above-footer')
@include('frontend.shop.partials.above-footer')
@endsection