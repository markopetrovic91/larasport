@extends('layouts.app', ['bodyClass' => 'page-home'])

@section('content')
<section id="hero-home">
    
</section>

@include('frontend.shop.partials.featured-products')
<section id="discount-home">
        <h3>20% DISCOUNT</h3>
        <p>Uz prvu kupovinu dobijate <strong>20%</strong> popusta na sve proizvode iz naše kolekcije!</p>
    </section>
<section id="categories-home">
    <div class="container">
        <h5 class="categories-home-h5">CATEGORIES</h5>
    </div>
    
    <div class="container">
    <div class="row">
        <div class="col-sm-4 home-cat home-cat-sale">
            <a href="{{ route('shop.index', ['on_sale' => 1]) }}">
            <div class="home-cat-wrap">
                <h5>25% SALE</h5>
                <div class="home-cat-overlay"></div>
            </div>
            </a>
        </div>
        <div class="col-sm-4 home-cat home-cat-women">
            <a href="{{ route('shop.index', ['category' => 22]) }}">
            <div class="home-cat-wrap">
                <h5>WOMEN</h5>
                <div class="home-cat-overlay"></div>
            </div>
            </a>
        </div>
        <div class="col-sm-4 home-cat home-cat-men">
            <a href="{{ route('shop.index', ['category' => 1]) }}">
            <div class="home-cat-wrap">
                <h5>MEN</h5>
                <div class="home-cat-overlay"></div>
            </div>
            </a>
        </div>
        <div class="col-sm-12 home-cat home-cat-summer">
            <div class="home-cat-wrap">
                <h5>SUMMER COLLECTION 2018</h5>
                <p>Lorem ipsum dolor sit amet consectetur.</p>
                <div class="home-cat-overlay"></div>
                <a href="{{ route('shop.index', ['new' => 1]) }}" class="btn default light">View More
                    <span><svg width="20" height="11"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrwIcn"><svg id="arrwIcn" viewBox="0 0 21.5 11.5" width="100%" height="100%"><path d="M13.8,11.5c-0.2,0-0.5-0.1-0.6-0.3c-0.2-0.3-0.2-0.8,0.2-1l6.1-4.4l-5-3.5v2.5c0,0.4-0.3,0.8-0.8,0.8h-13 C0.3,5.5,0,5.2,0,4.8S0.3,4,0.8,4H13V0.8c0-0.3,0.2-0.5,0.4-0.7C13.7,0,14,0,14.2,0.1l7,5c0.2,0.1,0.3,0.4,0.3,0.6s-0.1,0.5-0.3,0.6 l-7,5C14.1,11.5,13.9,11.5,13.8,11.5z"></path></svg></use></svg></span>
                </a>
            </div>
        </div>
    </div>
    </div>
</section>
<section class="home-product">
    <div class="container">
        <div class="index-flex-motion-wrap"> 
            <div class="index-flex-motion-img"> 
                <picture> 
                    <source srcset="/images/reebok-home-1130x680.jpg" media="(min-width: 1200px)">
                        <source srcset="/images/reebok-home-1130x680.jpg" media="(min-width: 1024px)">
                        <source srcset="/images/reebok-home-1130x680.jpg" media="(min-width: 768px)">
                        <source srcset="/images/reebok-home-1130x680.jpg" media="(min-width: 540px)">
                        <source srcset="/images/reebok-home-1130x680.jpg" media="(min-width: 412px)">
                            <img src="/images/reebok-home-1130x680.jpg" alt="Crossfit Nano 4">
                </picture> 
            </div> 
            <div class="index-flex-motion-info"> 
                <h2>CROSSFIT NANO<sup>™</sup></h2>
                <p>Performance, durability and comfort are packed into the latest evolution of the Nano training shoe.</p>
                <a class="btn default" href={{ route('shop.index').'/reebok-crossfit-nano-4-0' }}>View More</a>
            </div>
            
        </div>
    </div>
</section>

<section class="home-product two">
        <div class="container">
            <div class="index-flex-motion-wrap"> 
                <div class="index-flex-motion-img"> 
                    <picture> 
                        <source srcset="/images/nano-7-ropeclimb-main.jpg" media="(min-width: 1200px)">
                            <source srcset="/images/nano-7-ropeclimb-main.jpg" media="(min-width: 1024px)">
                            <source srcset="/images/nano-7-ropeclimb-main.jpg" media="(min-width: 768px)">
                            <source srcset="/images/nano-7-ropeclimb-main.jpg" media="(min-width: 540px)">
                            <source srcset="/images/nano-7-ropeclimb-main.jpg" media="(min-width: 412px)">
                                <img src="/images/nano-7-ropeclimb-main.jpg" alt="Corssfit Nano 7">
                    </picture> 
                </div> 
                <div class="index-flex-motion-info"> 
                    <h2>Nano 7<sup>™</sup></h2>
                    <p> Introducing the latest addition to your gym bag: the Nano 7.0. This next generation of our CrossFit shoe keeps the natural shape and stability athletes covet</p>
                    <a class="btn default" href="{{ route('shop.index').'/reebok-crossfit-nano-7' }}">View More</a>
                </div>
                
            </div>
        </div>
    </section>
<section id="popular-brands">
    @if($brands)
        <h2>Popular <span>Brands</span></h2>
        <div class="brands-wrapper">
            <ul class="brand-list clearfix">
                @foreach($brands as $brand)
                    <li>
                        <a href="{{ route('shop.index', ['brand' => $brand->id]) }}">
                            <img src="{{ $brand->image }}" alt="{{ $brand->name }}">
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
</section>


@endsection

@section('above-footer')
    @include('frontend.shop.partials.above-footer')
@endsection