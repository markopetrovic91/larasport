@extends('layouts.app', ['bodyClass' => 'page-cart'])

@section('content')
<div class="heading-title">
    <div class="row_triangle row_triangle_top triangle_bkg"></div>
    <div class="heading-title-wrapper">
        <h1>Cart Page</h1>
        <p><a href="{{ route('shop.index') }}" class="btn default light">Continue shopping</a></p>
    </div>
</div>
<div class="container cart-container">
        <div class="cart-messages container">
                {{--@if(session()->has('success_message'))--}}
                {{--<div class="alert alert-success">--}}
                    {{--{{ session()->get('success_message') }}--}}
                {{--</div>--}}
                {{--@endif--}}
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            
            @if(Cart::count() > 0)

                <table class="table cart-table" cellspacing="0">
                        
                    <thead>
                        <tr>
                            <th class="product-remove">&nbsp;</th>
                            <th class="product-thumbnail">&nbsp;</th>
                            <th class="product-name">Product</th>
                            <th class="product-price">Price</th>
                            <th class="product-price">Size/Color</th>
                            <th class="product-quantity">Quantity</th>
                            <th class="product-subtotal">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(Cart::content() as $item)

                            <tr class="cart_item cart-table-row">


                                <td class="product-remove">
                                    <form action="{{ route('cart.destroy', $item->rowId) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button href="" class="remove" title="Remove this item">×</button>
                                    </form>
                                </td>
                
                                    <td class="product-thumbnail">
                                        @php
                                            $product = findProductById($item->id);
                                        @endphp
                                        <a href="{{ route('shop.show', $item->model->slug) }}">
                                            <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">
                                        </a>
                                    </td>
                
                                    <td class="product-name">
                                        <a href="{{ route('shop.show', $item->model->slug) }}">{{ $item->name }} </a>
                                    </td>
                
                                    <td class="product-price">
                                        <span class="amount">{{ formatPrice($item->price) }}</span>
                                    </td>

                                <td class="product-size-price">
                                        @php

                                        foreach ($item->options as $key => $value) {
                                            if (($key == 'inventory') || ($key == 'Old price')) {
                                                continue;
                                            }else{
                                                echo '<div>'.ucfirst($value).'</div>';
                                            }
                                        }
                                        @endphp
                                </td>
                
                                    <td class="product-qty" data-id="{{ $item->rowId }}">
                                    <qtyy :numqty={{ $item->qty }}
                                          :inventory={{ $item->options->inventory ? $item->options->inventory : 0}}>
                                    </qtyy>
                                    </td>
                
                                    <td class="product-subtotal">
                                        <span class="amount">{{ formatPrice($item->total) }}</span>
                                    </td>
                            </tr>
                        @endforeach
                
                    </tbody>
                </table>
                <div class="actions">
                    <div class="coupon">
                        @if(! session()->has('coupon'))
                        <form action="{{ route('applyCoupon') }}" method="post">
                            {{ csrf_field() }}
                        <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code"> 
                        <input type="submit" class="button" name="apply_coupon" value="Apply Coupon">
                        </form>
                        @endif
                    </div>




                    <div class="update-checkout-cart">
                        <input type="submit" class="button" name="update_cart" id="update-cart" value="Update Cart" onclick="updateCart()">
                        <input type="submit" class="checkout-button button alt wc-forward" name="proceed" value="Proceed to Checkout" onclick="goToCheckout()">
                    </div>
                </div>
                <div class="cart-collaterals clearfix">
                    <div class="cart_totals ">
                    <h3>Cart Totals</h3>
                    <table cellspacing="0" class="table-cart-subtotal">
                    <tbody>
                        <tr class="cart-subtotal">
                            <th>Cart Subtotal</th>
                            <td><span class="amount">{{ formatPrice(Cart::subtotal()) }}</span></td>
                        </tr>
                        @if(session()->has('coupon'))
                        <tr class="cart-discount">
                            <th>Discount <span>({{ session()->get('coupon')['name'] }})</span>
                                <span>
                                    <form action="{{ route('removeCoupon') }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('delete') }}
                                    <button type="submit"><i class="fas fa-times"></i></button>
                                    </form>
                                </span>
                            </th>
                            {{--<td>-{{ session()->get('coupon')['discount'] }}</td>--}}
                            <td>-{{ formatPrice($discount) }}</td>

                        </tr>

                        <tr class="new-subtotal">
                            <th>Subtotal</th>
                            <td>{{ formatPrice($newSubtotal) }}</td>
                        </tr>
                        @endif
                        <tr class="shipping">
                            <th>Shipping</th>
                            <td>Free Shipping
                                <input type="hidden" name="shipping_method[0]" data-index="0" id="shipping_method_0" value="free_shipping" class="shipping_method">
                            </td>
                        </tr>
                        <tr class="order-total">
                            <th>Order Total</th>
{{--                            <td><strong><span class="amount">{{ Cart::total() }}</span></strong> </td>--}}
                                <td><strong><span class="amount">{{ formatPrice($newTotal) }}</span></strong> </td>

                        </tr>
                    </tbody>
                </table>
  
                </div>
                
                
                </div>
                @else
                <div class="no-items-in-cart">
                    <h3>No items in Cart</h3>
                    <a href="{{ route('shop.index') }}" class="btn">Continue shopping</a>
                </div>
                @endif

@if(Auth::user())
@if (Auth::user()->favorites->count() )
    <div class="wishlist-table">
        <h3 class="font-graphik-black">Maybe want to add some products from your wishlist?</h3>
        <br> <br>
            @foreach ($wishlists as $product)
            <div class="col-sm-4 featured-product product">
                <div class="featured-product-img">
                    <a href="{{ route('shop.show', $product->slug) }}">
                        <img src="{{ getProductImage($product->product_image) }}" alt="{{ $product->name }}">
                    </a>
                    @if($product->inventory = 0)
                        <div class="sold-out">
                            <img src="{{ asset('/images/sold-out.png') }}" alt="sold out">
                        </div>

                    @endif
                </div>
                <div class="featured-product-desc">
                    <a href="{{ route('shop.show', $product->slug) }}"><h3>{{ $product->name }}</h3></a>
                    {{--<p><span class="vrnr">{{ $product->presentPrice() }}</span>--}}
                    @if($product->on_sale)
                        <p>
                            @if($product->new)
                                <span class="new-product">NEW</span>
                            @endif
                            <span class="vrnr">{{ formatPrice($product->sale_price) }}</span>
                            <span class="reduced">{{ formatPrice($product->price) }}</span>
                        </p>
                    @else
                        <p>
                            @if($product->new)
                                <span class="new-product">NEW</span>
                            @endif
                            <span class="vrnr regular-price">{{ formatPrice($product->price) }}</span>
                        </p>
                    @endif
                    <a href="{{ route('shop.show', $product->slug) }}" class="btn red">Shop Now</a>
                </div>
                <span class="product-fav">
                    <favorite
                            :product={{ $product->id }}
                                    :favorited={{ $product->favorited() ? 'true' : 0 }}>
                    </favorite>
                </span>
            </div>
            @endforeach
    </div>
@endif
@endif

</div> <!-- end container -->
@endsection
@section('footer')
    <script>

//        window.onload = function () {
//            document.getElementById("cart-form").addEventListener('click', function(event){
//                event.preventDefault();
//            });
//        }

        function updateCart(){
            var kolicine = document.querySelectorAll('.product-qty');
            kolicine.forEach(function (item, index) {
                var a = item.getAttribute('data-id');
                var b = item.getElementsByClassName('qty')[0].value

                axios.patch('/cart/'+a, {
                    quantity: b
                })
                    .then(function (response) {
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            });
        window.location.href = '{{ route('cart.index') }}'
        }
        function goToCheckout(){
//                document.getElementById("cart-form").addEventListener('click', function(event){
//                    event.preventDefault();
//                });
            window.location.href = "/checkout";
        }
    </script>
@endsection