@extends('layouts.app', ['title' => 'Contact Page', 'bodyClass' => 'contact-page'])

@section('content')
<div class="heading-title">
    <div class="row_triangle row_triangle_top triangle_bkg"></div>
    <div class="heading-title-wrapper">
        <h1>Contact Page</h1>
        <p>You can complete our online form to send an email to our Customer Service team. <br>
            We aim to respond within 2 business days of receiving your email.</p>
    </div>
</div>
    <div class="container contact-form-container">
        <div class="col-sm-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-sm-4">
                <p><i class="fas fa-home"></i> Street: Way 1</p>
                <p style="padding-left: 22px;">Zip Code: 91074 </p>
                <p style="padding-left: 22px;">City: Herzogenaurach </p>
                <p style="padding-left: 22px;">Country: Germany</p>
                <p><i class="fas fa-phone"></i> Telephone: +321 5441600</p>
            </div>
            <div class="col-sm-8">
            <form action="{{ route('contact.send') }}" method="post">
                {{ csrf_field() }}
                <div class="form-field">
                    <input type="text" name="name" placeholder="Your name" value="{{ old('name') }}">
                </div>
                <div class="form-field">
                    <input type="email" name="email" placeholder="Your Email" value="{{ old('email') }}">
                </div>
                <div class="form-field">
                    <input type="text" name="subject" placeholder="Subject" value="{{ old('subject') }}">
                </div>
                <div class="form-field">
                    <textarea name="message" id="message" cols="20" rows="10" placeholder="Message...">
                        {{ old('message') }}
                    </textarea>
                </div>
                <div class="form-field">
                    <input class="btn btn-submit" type="submit" name="submit" value="Send">
                </div>
            </form>

            </div>
        </div>
    </div>
@endsection