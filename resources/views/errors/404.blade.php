@extends('layouts.app', ['title' => 'Page 404']) 

@section('content')
<div class="container page-404">
    <p class="pnf-heading">404</p>
    <div class="text-wrap">
        <h1>sorry page not found</h1>
        <p>The page are looking for is not available or has been removed.</p>
        <p>Try going to
            <a href="/">home page</a> by using the button below</p>
    </div>
    <p class="home-link">
        <a href="/">
            <span class="icon-home">go to home</span>
        </a>
    </p>
</div>
@endsection