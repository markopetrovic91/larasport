<?php

namespace App;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Database\Eloquent\Model;

class PercentOffCoupon extends Model
{

    protected $fillable = ['percent_off'];

    public function discount()
    {
        return Cart::subtotal() * ($this->percent_off / 100);
    }

    public function couponValue()
    {
        return $this->percent_off;
    }
}
