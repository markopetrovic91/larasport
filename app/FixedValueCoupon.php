<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FixedValueCoupon extends Model
{
    protected $fillable = ['value'];

    public function discount()
    {
        return $this->value;
    }
    public function couponValue()
    {
        return $this->value;
    }
}
