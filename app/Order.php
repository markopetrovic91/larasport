<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'address_id', 'additional', 'name_on_card', 'billing_discount', 'billing_discount_code', 'billing_subtotal', 'tax', 'billing_total',
        'payment_gateway', 'shipped', 'error'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('quantity', 'attributes');
    }

    public function address()
    {
        return $this->belongsTo('App\Address');
    }
}
