<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 25.8.18.
 * Time: 19.19
 */

namespace App\Filters\Product;


use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class BrandFilter extends FilterAbstract
{

    public function mappings()
    {
        return [];
    }
    public function filter(Builder $builder, $value)
    {
        if ($value === null) {
            return $builder;
        }

        if (is_array($value)) {
            return $builder->whereIn('brand_id', $value);
        }
        return $builder->where('brand_id', $value);
    }
}
