<?php

namespace App\Filters\Product;


use App\Filters\FiltersAbstract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Filters\Product\{BrandFilter, CategoryFilter};

class ProductFilters extends FiltersAbstract
{
    protected $filters = [
        'brand' => BrandFilter::class,
        'category' => CategoryFilter::class,
        'on_sale' => OnSaleFilter::class,
        'new' => NewFilter::class,
        'outlet' => OutletFilter::class
    ];
    public static function mappings()
    {
        $map = [
            'brand' => [
                'adidas' => 'Adidas',
                'puma' => 'Puma',
            ],
            'difficulty' => [
                'beginner' => 'Beginner',
                'intermediate' => 'Intermediate',
                'advanced' => 'Advanced',
            ],
            'type' => [
                'snippet' => 'Snippet',
                'project' => 'Project',
                'theory' => 'Theory',
            ],
//            'subjects' => Subject::get()->pluck('name', 'slug')->toArray()
        ];

        if (auth()->check()) {
            $map = array_merge($map, [
                'started' => [
                    'true' => 'Started',
                    'false' => 'Not started',
                ]
            ]);
        }

        return $map;
    }
}