<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 25.8.18.
 * Time: 21.47
 */

namespace App\Filters\Product;


use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class OnSaleFilter extends FilterAbstract
{

    public function mappings()
    {
        return [
            'yes' => 1
        ];
    }

    /**
     * Apply filter.
     *
     * @param  Builder $builder
     * @param  mixed $value
     *
     * @return Builder
     */
    public function filter(Builder $builder, $value)
    {
        $value = $this->resolveFilterValue($value);

        if ($value === null){
            return $builder;
        }
        return $builder->where('on_sale', $value);
    }
}