<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 4.9.18.
 * Time: 00.45
 */

namespace App\Filters\Product;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;
class OutletFilter extends FilterAbstract
{

    public function mappings()
    {
        return [
            1 => 1
        ];
    }
    /**
     * Apply filter.
     *
     * @param  Builder $builder
     * @param  mixed $value
     *
     * @return Builder
     */
    public function filter(Builder $builder, $value)
    {
        $value = $this->resolveFilterValue($value);

        if ($value === null){
            return $builder;
        }

        return $builder->where('outlet', $value);
    }
}