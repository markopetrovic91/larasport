<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 25.8.18.
 * Time: 20.15
 */

namespace App\Filters\Product;


use App\Category;
use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class CategoryFilter extends FilterAbstract
{

    public function mappings()
    {
        return [
            'men' => 1,
            'women' => 22
        ];
    }

    /**
     * Apply filter.
     *
     * @param  Builder $builder
     * @param  mixed $value
     *
     * @return Builder
     */
    public function filter(Builder $builder, $value)
    {
        $cat = Category::find($value);
        $products = $cat->products()->get();
        $arr = array();

        foreach ($products as $product) {
            $arr[] = $product->id;
        }
        if (is_array($arr)) {
            return $builder->whereIn('id', $arr);
        }
        return $builder->where('id', $arr);
    }
}
