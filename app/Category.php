<?php

namespace App;

use App\Product;
use Kalnoy\Nestedset\NodeTrait;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use NodeTrait;
    protected $fillable = ['name', 'parent_id'];

    public function products()
    {
        return $this->belongsToMany('App\Product')->withTimestamps();
    }
}
