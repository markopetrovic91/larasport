<?php

namespace App\Http\Controllers\Admin;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewsController extends Controller
{
    public function index()
    {
        $reviews = Review::orderByDesc('created_at')->get();

        return view('admin.reviews.index', compact('reviews'));
    }

       public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rev = Review::find($id);
        $product = $rev->product()->get()->first();
        return view('admin.reviews.edit', compact(['rev', 'product']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rev = Review::find($id);
        if ($request->approved == 'on'){
            $rev->approved = true;
        }else{
            $rev->approved = false;
        }
        $rev->save();

        return redirect()->back()->withSuccessMessage('Review updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rev = Review::find($id);
        $rev->delete();
        return redirect()->back()->withSuccessMessage('You\'ve successfully deleted '.$rev->nickname.'\'s review');
    }

    public function approved($approved)
    {
        $reviews = Review::where('approved', $approved)->orderByDesc('created_at')->get();
        return view('admin.reviews.index', compact('reviews'));
    }
}
