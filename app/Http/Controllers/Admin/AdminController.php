<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct(){
        // $this->middleware('admin');
    }

    public function admin (){
        $users = count(User::all());
        $totalProducts = count(Product::all());
        $totalIncome = Order::all()->sum('billing_total');
        return view('admin.index', compact(['users', 'totalProducts', 'totalIncome']));
    }
}
