<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Spatie\Newsletter\NewsletterFacade as Newsletter;
use Illuminate\Http\Request;

class MailchimpController extends Controller
{
    public function index()
    {
        $memlist = Newsletter::getMembers();
        $subslist = [];
        foreach ($memlist["members"] as $sub) {
            $sub["isMember"] = $this->isMember($sub["email_address"]);
            $subslist[] = $sub;
        }
        return view('admin.subscribers.index', compact('subslist'));
    }
    public function store(Request $request)
    {

        if (Newsletter::isSubscribed($request->email)) {
            return redirect()->back()->withSuccessMessage("You're already subscribed.");
        }
        Newsletter::subscribePending($request->email);
        return redirect()->back()->withSuccessMessage("We sent you a confirmation email with a link to activate your subscription. Thanks!");
    }

    protected function isMember($email)
    {
        $user = User::where('email', $email)->first();
        $member = [];
        if ($user) {
            $member["isMember"] = "Yes";
            $member["isBuyer"] = $this->doesUserHaveOrder($user);
            return $member;
        }
        $member["isMember"] = "No";
        $member["isBuyer"] = "No";
        return $member;
    }
    private function doesUserHaveOrder($user)
    {
        if ($user->orders()->get()->count() > 0) {
            return "Yes";
        }
        return "No";
    }
}
