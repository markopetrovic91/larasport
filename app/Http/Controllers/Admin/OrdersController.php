<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::orderByDesc('id')->paginate(10);
        return view('admin.orders.index', compact('orders'));
    }

    public function edit($id){
        $order = Order::find($id);
        $pro = $order->products()->get();
        return view('admin.orders.edit', compact(['order', 'pro']));

    }

    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();
        return redirect()->back()->withSuccessMessage('Order #'.$id.' has been successfully delete!');
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        if ($request->shipped == 'on'){
            $order->shipped = true;
        }else{
            $order->shipped = false;
        }

        $order->status = $request->status;

        $order->save();

        return redirect()->back()->withSuccessMessage('Order #'.$id.' has been updated');
    }
}
