<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use App\FixedValueCoupon;
use App\PercentOffCoupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponsController extends Controller
{
    /**
     * Display a listing of coupons.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::all();
        return view('admin.coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new coupon.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupons.create');
    }

    /**
     * Store a newly created coupon in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string|max:255',
            'type' => 'required',
            'value' =>  'required|numeric'
        ]);
        $couponType = $request->type;
        if ($couponType == 'percentage') {
            $coup = PercentOffCoupon::create([
                'percent_off' => $request->value
            ]);
        } elseif ($couponType == 'fixed') {
            $coup = FixedValueCoupon::create([
                'value' => $request->value
            ]);
        }
        $coupon = new Coupon();
        $coupon->code = $request->code;
        $kupon = $coupon->coupon()->associate($coup);

        $kupon->save();

        return redirect()->back()->withSuccessMessage('Coupon is successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $code
     */

    public function applyCoupon(Request $request)
    {
        $coupon = Coupon::findByCode($request->coupon_code);
        if (!$coupon) {
            return redirect()->route('cart.index')->withErrors('Invalid coupon code. Please try again.');
        }

        session()->put('coupon', [
            'name' => $coupon->code,
            'discount' => $coupon->discount()
        ]);

        return redirect()->route('cart.index')->with('success_message', 'Coupon has been applied!');
    }

    /**
     * Remove the specified coupon from the cart.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeCoupon()
    {
        session()->forget('coupon');
        return redirect()->route('cart.index')->with('success_message', 'Coupon has been removed.');
    }
}
