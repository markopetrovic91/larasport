<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mockery\Exception;

class CategoriesController extends Controller
{
    /**
     * Display a listing of categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = collect(Category::all()->toTree());
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = collect(Category::all());
        return view('admin.categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|string|unique:categories|max:255',
            'parent_id' => 'required'
        ]);
        if ($request->parent_id == 'root') {
            $node = new Category();
            $node->name = $request->name;
            $node->slug = $request->slug;
            $node->save();
            return redirect()->route('categories.index')->withSuccessMessage('Category has been sucessfully saved!');
        }
        $parent = Category::find($request->parent_id);
        $node = new Category();
        $node->name = $request->name;
        $node->slug = $request->slug;
        $node->appendToNode($parent)->save();

        return redirect()->route('categories.index')->withSuccessMessage('Category has been sucessfully saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Category::find($id)->products;
        $categories = Category::all();
        return view('admin.categories.index', compact(['products', 'categories']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Category::find($id)->products;
        $category = Category::find($id);
        $categories = Category::all();
        return view('admin.categories.edit', compact(['products', 'category', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
            //            'slug' => 'required|unique:categories',
            'slug' => 'required',
        ]);

        $cat = Category::find($id);
        $cat->name = $request->name;
        $cat->slug = $request->slug;
        $cat->save();
        return redirect()->back()->with('success_message', 'Category has been successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = Category::find($id);

        if ($cat->delete()) {
            $categories = collect(Category::all()->toTree());
            return redirect()->route('categories.index')->with('categories', $categories)->withSuccessMessage('Category ' . $cat->name . ' has been removed');
        }
        $categories = collect(Category::all()->toTree());
        return redirect()->route('categories.index')->with('categories', $categories)->withErrors('Some errors has appear! Please try again.');
    }
    /**
     * Remove the specified product from the category.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeProductFromCategory($proid, $catid)
    {
        $cat = Category::find($catid);
        try {
            $cat->products()->detach($proid);
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
        return redirect()->back()->with('success_message', 'Product has been removed');
    }
}
