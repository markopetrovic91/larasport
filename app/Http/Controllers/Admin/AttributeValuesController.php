<?php

namespace App\Http\Controllers\Admin;

use App\AttributeValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttributeValuesController extends Controller
{
    public function index()
    {

    }
    public function store(Request $request)
    {
        AttributeValue::create([
            'value' => $request->value,
            'attribute_id' => $request->attribute_id
        ]);

        return redirect()->back()->withSuccessMessage('The new value ('.$request->value.') has been added!');
    }

    public function destroy($id)
    {
        $att = AttributeValue::find($id);
        $att->delete();
        return redirect()->back()->withSuccessMessage('The attribute '.$att->value.' has been deleted!');
    }
}
