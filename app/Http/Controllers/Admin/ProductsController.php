<?php

namespace App\Http\Controllers\Admin;

use App\Attribute;
use App\Brand;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderByDesc('id')->paginate(10);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::all();
        $atts = Attribute::all();
        $categories = collect(Category::all()->toTree());
        return view('admin.products.create', compact('brands', 'atts', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  validation
        $this->validate($request, [
            'name' => 'required',
            'sku' => 'required|max:255',
            'slug' => 'required|max:255',
            'price' => 'required|numeric',
            'inventory' => 'required|numeric',
            'weight' => 'numeric',
            'details' => 'string|nullable',
            'description' => 'string|nullable',
            'image' => 'image|mimes:png,jpg,gif,jpeg|max:2048'
        ]);
        //  Image upload
        $product = new Product();

        if ($request->hasFile('product_image')) {
            $image = $request->file('product_image');
            $imageName = $image->getClientOriginalName();
            Storage::put($imageName, file_get_contents($image));
            $product->product_image = $imageName;
        }

        if ($request->featured == 'on') {
            $product->featured = true;
        } else {
            $product->featured = false;
        }
        if ($request->new == 'on') {
            $product->new = true;
        } else {
            $product->new = false;
        }
        if ($request->outlet == 'on') {
            $product->outlet = true;
        } else {
            $product->outlet = false;
        }
        if ($request->on_sale == 'on') {
            $product->on_sale = true;
            $product->sale_price = $request->sale_price;
        } else {
            $product->on_sale = false;
            $product->sale_price = 0;
        }

        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->slug = $request->slug;
        $product->price = $request->price;
        $product->brand_id = $request->brand_id;
        $product->inventory = $request->inventory;
        $product->weight = $request->weight;
        $product->details = $request->details;
        $product->description = $request->description;
        $product->save();

        $product->categories()->sync($request->cats);

        if ($request->has('colors')) {
            $product->attributesValues()->sync($request->colors);
        }
        if ($request->has('shoe_sizes')) {
            $product->attributesValues()->syncWithoutDetaching($request->shoe_sizes);
        }
        if ($request->has('clothing_sizes')) {
            $product->attributesValues()->syncWithoutDetaching($request->clothing_sizes);
        }

        return redirect()->route('products.index')->withSuccessMessage('New product has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $revs = $product->reviews()->orderByDesc('created_at')->get();
        $brands = Brand::all();
        $atts = Attribute::all();
        $categories = collect(Category::all()->toTree());
        $proAtts = $product->attributesValues()->get();
        $proCats = $product->categories()->get();
        return view('admin.products.edit', compact('product', 'revs', 'brands', 'atts', 'proAtts', 'categories', 'proCats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $request->validate([
            'name' => 'required',
            'sku' => 'required|max:255',
            'slug' => 'required|max:255',
            'price' => 'required|numeric',
            'inventory' => 'required|numeric',
            'weight' => 'numeric',
            'details' => 'string|nullable',
            'description' => 'string|nullable',
            'image' => 'image|mimes:png,jpg,gif,jpeg|max:2048'
        ]);

        //  Image upload
        if ($request->hasFile('product_image')) {
            $image = $request->file('product_image');
            $imageName = $image->getClientOriginalName();
            if ($imageName !== $product->product_image) {
                Storage::put($imageName, file_get_contents($image));
                $product->product_image = $imageName;
            }
        }

        if ($request->featured == 'on') {
            $product->featured = true;
        } else {
            $product->featured = false;
        }
        if ($request->new == 'on') {
            $product->new = true;
        } else {
            $product->new = false;
        }
        if ($request->outlet == 'on') {
            $product->outlet = true;
        } else {
            $product->outlet = false;
        }
        if ($request->on_sale == 'on') {
            $product->on_sale = true;
            $product->sale_price = $request->sale_price;
        } else {
            $product->on_sale = false;
            $product->sale_price = 0;
        }

        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->slug = $request->slug;
        $product->price = $request->price;
        $product->brand_id = $request->brand_id;
        $product->inventory = $request->inventory;
        $product->weight = $request->weight;
        $product->details = $request->details;
        $product->description = $request->description;

        $product->categories()->sync($request->cats);

        if ($request->has('colors')) {
            $product->attributesValues()->sync($request->colors);
        }
        if ($request->has('shoe_sizes')) {
            $product->attributesValues()->syncWithoutDetaching($request->shoe_sizes);
        }
        if ($request->has('clothing_sizes')) {
            $product->attributesValues()->syncWithoutDetaching($request->clothing_sizes);
        }

        $product->save();

        return redirect()->back()->withSuccessMessage('Product has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function basedOnCategory(Request $request)
    {
        $products = Product::all();
        return response()->json($products, 200);
    }
}
