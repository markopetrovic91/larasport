<?php

namespace App\Http\Controllers\Admin;

use App\Attribute;
use App\AttributeValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttributesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributes = Attribute::all();

        return view('admin.attributes.index', compact('attributes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.attributes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required|max:255',
            'value.*' => 'required|string'
        ]);

        $att = Attribute::create(['name' => $request->name]);
        if ($att) {
            foreach ($request->value as $val) {

                AttributeValue::create([
                    'value' => $val,
                    'attribute_id' => $att->id
                ]);
            }
        } else {
            return redirect()->back()->withErrors('This attribute already exists');
        }
        return redirect()->route('attributes.index')->withSuccessMessage('The new attribute is set!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attribute = Attribute::find($id);

        return view('admin.attributes.edit', compact('attribute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $att = Attribute::find($id);
        $att->name = $request->name;
        $att->save();
        return redirect()->back()->withSuccessMessage('Attribute name has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $att = Attribute::find($id);

        if ($att->delete()) {
            return redirect()->back()->withSuccessMessage('Attribute is deleted successfully!');
        }
        return redirect()->back();
    }
}
