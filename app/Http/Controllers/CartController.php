<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Favorite;
use App\Product;
use Auth;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totals = getAllTotals();
        $wishlists = null;
        if (Auth::user()) {
            $user = Auth::user();
            $wishlists = $user->favorites()->orderBy('product_id')->get();
        }
        return view('frontend.cart.index')->with([
            'wishlists' => $wishlists,
            'discount' => $totals->get('discount'),
            'newSubtotal' => $totals->get('newSubtotal'),
            'newTotal' => $totals->get('newTotal')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });
        if ($duplicates->isNotEmpty()) {
            return redirect()->route('cart.index')->with('success_message', "Item is already in your Cart");
        }

        $options = array('inventory' => $request->inv);
        if ($request->has('radio_color')) {
            $options['Color'] = $request->radio_color;
        }
        if ($request->has('radio_size')) {
            $options['Size'] = $request->radio_size;
        }

        Cart::add($request->id, $request->name, $request->quantity, $request->price, $options)->associate('App\Product');
        return redirect()->route('cart.index')->with('success_message', 'Item has been added to your cart!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cart::update($id, $request->quantity);
        updateCouponSession();
        session()->flash('success_message', 'Your cart has been updated successfully!');
        return response()->json(['success_message' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return back()->with('success_message', 'Item has been removed!');
    }
    /**
     * Add product to Wish List
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getWishlist()
    {
        $item = Product::get();
        Cart::instance('wishlist')->add($item->id, $item->name, 1, $item->price)->associate('App\Product');
        return redirect()->route('shop.show')->with('success_message', 'Item has been added to your Wishlist');
    }
}
