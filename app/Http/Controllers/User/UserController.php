<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\CheckoutRequest;
use App\OrderProduct;
use App\Product;
use DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mockery\Exception;
use Spatie\Newsletter\NewsletterFacade as Newsletter;

class UserController extends Controller
{
    /**
     * Displaying user data
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::find($id);
        $orders = $user->orders()->get();
        $subscribed = Newsletter::isSubscribed($user->email);
        return view('frontend.user.index', compact(['orders', 'user', 'subscribed']));
    }

    public function info($id)
    {
        $user = User::find($id);
        $userAddress = $user->orders()->first();
        return view('frontend.user.index', compact(['user', 'userAddress']));
    }

    public function addresses($id)
    {
        $userinfo = 'neki info';
        return view('frontend.user.index')->with('userinfo');
    }
    public function orders($id)
    {
        $user = User::find($id);
        $orders = $user->orders()->get();
        return view('frontend.user.index', compact('orders'));
    }

    public function subscription($id)
    {
        $user = User::find($id);
        $subscribed = Newsletter::isSubscribed($user->email);
        return view('frontend.user.index', compact('subscribed'));
    }

    public function subscriptionPost(Request $request, $id)
    {
        $user = User::find($id);
        if ($request->is_subscribed) {
            if (Newsletter::isSubscribed($user->email)) {
                return redirect()->back()->withSuccessMessage("You're already subscribed.");
            }
            Newsletter::subscribeOrUpdate($user->email, ['firstName' => $user->name, 'lastname' => '']);
            return redirect()->back()->withSuccessMessage("You're subscribed now. Thanks!");
        } else {
            // remove subscription
            if (Newsletter::isSubscribed($user->email)) {
                Newsletter::unsubscribe($user->email);
                return redirect()->back()->withSuccessMessage("You're unsubscribed now.");
            } else {
                return redirect()->back();
            }
        }
    }

    public function wishlist($id)
    {
        $user = User::find($id);
        $wishlist = $user->favorites()->get();
        return view('frontend.user.index', compact('wishlist'));
    }

    public function removeFromWishlist($id, $productId)
    {
        $user = User::find($id);
        $user->favorites()->detach($productId);
        return redirect()->back()->withSuccessMessage('Product successfuly removed from wishlist!');
    }

    public function clearWishlist($id)
    {
        $user = User::find($id);
        $user->favorites()->detach();
        return redirect()->back()->withSuccessMessage('Your wish list is clear now!');
    }

    public function quickReorder($id, $orderid)
    {
        $user = User::find($id);
        $products = OrderProduct::where('order_id', $orderid)->get();

        foreach ($products as $product) {
            $pr = Product::find($product->product_id);
            Cart::add($pr->id, $pr->name, $product->quantity, $pr->price, ['size' => 'large', 'inventory' => $pr->inventory])->associate('App\Product');
            return redirect()->route('cart.index')->withSuccessMessage('All products from you order are successfully added!');
        }
    }

    public function userUpdate(CheckoutRequest $request, $id)
    {
        $address = User::find($id)->orders->last()->address;
        $address->first_name = $request->first_name;
        $address->last_name = $request->last_name;
        $address->phone = $request->phone;
        $address->address = $request->address;
        $address->zip = $request->zip;
        $address->city = $request->city;
        $address->country = $request->country;
        $address->save();
        return redirect()->back()->withSuccessMessage('You profile is successfully updated!');
    }
}
