<?php

namespace App\Http\Controllers\Shop;

use App\Address;
use App\Http\Requests\CheckoutRequest;
use App\Order;
use App\OrderProduct;
use App\Product;
use Cartalyst\Stripe\Exception\CardErrorException;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totals = getAllTotals();

        return view('frontend.checkout.checkout')->with([
            'tax' => $totals->get('tax'),
            'discount' => $totals->get('discount'),
            'newSubtotal' => $totals->get('newSubtotal'),
            'newTotal' => $totals->get('newTotal')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CheckoutRequest $request)
    {

        if ($request->payment_methid == 'stripe') {
            // Charge stripe
            $contents = Cart::content()->map(function ($item) {
                return $item->model->slug . ', ' . $item->qty;
            })->values()->toJson();

            try {
                $charge = Stripe::charges()->create([
                    'amount' => getAllTotals()->get('newTotal'),
                    'currency' => 'USD',
                    'source' => $request->stripeToken,
                    'description' => 'Order',
                    'receipt_email' => $request->email,
                    'metadata' => [
                        'contents' => $contents,
                        'quantity' => Cart::instance('default')->count(),
                        'discount' => collect(session()->get('coupon'))->toJson(),
                        'customer_email' => $request->email,
                        'info' => $request->additional,
                        'phone' => $request->phone,
                    ],
                ]);
            } catch (CardErrorException $e) {
                $this->addToOrdersTable($request, $e->getMessage());
                return back()->withErrors('Errors!' . $e->getMessage());
            }
        }

        $orderId = $this->addToOrdersTable($request, null);

        // SUCCESSFUL
        Cart::instance('default')->destroy();

        session()->forget('coupon');

        return redirect()->route('confirmation.index')
            ->with('order_success', 'You order #' . $orderId . ' has been successfully accepted! <br> We will contact you as soon as possible. Thank you!');
    }
    private function addToOrdersTable($request, $error)
    {

        // Insert into addresses table
        $addressId = Address::create([
            'user_id' => auth()->user() ? auth()->user()->id : null,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'country' => ucfirst($request->country),
            'address' => $request->address,
            'zip' => $request->zip,
            'city' => $request->city,
        ]);

        // Insert into orders table
        $totals = getAllTotals();
        $order = Order::create([
            'user_id' => auth()->user() ? auth()->user()->id : null,
            'address_id' => $addressId->id,
            'additional' => $request->additional,
            'name_on_card' => $request->name_on_card,
            'billing_discount' => $totals->get('discount'),
            'billing_discount_code' => session()->get('coupon')['name'],
            'billing_subtotal' => $totals->get('newSubtotal'),
            'tax' => $totals->get('tax'),
            'billing_total' => $totals->get('newTotal'),
            'payment_gateway' => $request->payment_method,
            'shipped' => false,
            'status' => 'Pending',
            'error' => $error
        ]);

        // Insert into order_product table
        foreach (Cart::content() as $item) {
            $proAtts = $item->options;
            $proAtts->pull('inventory');
            $array = array();
            foreach ($proAtts as $key => $value) {
                $array[] = $key . ' : ' . $value;
            }
            $comma_separated = implode(", ", $array);

            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $item->model->id,
                'quantity' => $item->qty,
                'attributes' => $comma_separated
            ]);
            Product::find($item->id)->decrement('inventory', $item->qty);
        }
        return $order->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { }
}
