<?php

namespace App\Http\Controllers\Shop;

use App\Brand;
use App\Category;
use App\Review;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Auth\EloquentUserProvider;

class ShopController extends Controller
{
    /**
     * Display a listing of the paginated products and filters.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $qstr = $request->query();
        $products = Product::filter($request, $this->getFilters())->orderByDesc('id')->paginate(6);
        $brand = Brand::all();
        $categories = collect(Category::all()->toTree());
        $category = Category::all();
        $filters = collect([
            ['name' => 'brand', 'data' => $brand],
            ['name' => 'category', 'data' => $category]
        ]);

        return view('frontend.shop.shop', compact('products', 'filters', 'brand', 'category', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->favorites()->attach($request->id);
        return 'passed';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        $migtAlsoLike = Product::where('slug', '!=', $slug)->mightAlsoLike()->get();
        $reviews = Review::where('product_id', $product->id)
            ->where('approved', true)->orderBy('id', 'desc')->get();
        $atts = $product->attributesValues()->get();

        return view('frontend.shop.product', compact(['product', 'migtAlsoLike', 'reviews', 'atts']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function favoriteProduct($id)
    {
        if (Auth::user()) {
            Auth::user()->favorites()->attach($id);
            //            return true;
        } else {
            return false;
        }
    }
    public function unFavoriteProduct($id)
    {
        Auth::user()->favorites()->detach($id);
    }
    public function getWishList()
    {
        return "wishlist";
    }

    protected function getFilters()
    {
        return [];
    }

    public function getSearchResults($q = null)
    {
        $products = Product::where('name', 'LIKE', '%' . $q . '%')->get();

        return response()->json([
            'model' => $products
        ]);
    }
}
