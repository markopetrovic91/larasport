<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfirmationController extends Controller
{
    public function index()
    {
        return view('frontend.shop.thankyou');
    }
}
