<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\Services\SocialAccountService;

class SocialAuthController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function fbRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from twitter api.
     *
     * @return callback URL from twitter
     */
    public function fbCallback(SocialAccountService $service)
    {
        $user = $service->createOrGetFbUser(Socialite::driver('facebook')->user());
        auth()->login($user);
        return redirect()->to('/');
    }

    /**
     * Create a redirect method to twitter api.
     *
     * @return void
     */
    public function twRedirect()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Return a callback method from Linkedin api.
     *
     * @return callback URL from Linkedin
     */
    public function twCallback(SocialAccountService $service)
    {
        $user = $service->createOrGetTwitterUser(Socialite::driver('twitter')->user());
        auth()->login($user);
        return redirect()->to('/');
    }

    /**
     * Create a redirect method to Linkedin api.
     *
     * @return void
     */
    public function ldRedirect()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    /**
     * Return a callback method from Linkedin api.
     *
     * @return callback URL from Linkedin
     */
    public function ldCallback(SocialAccountService $service)
    {
        $user = $service->createOrGetLinkedInUser(Socialite::driver('linkedin')->user());
        auth()->login($user);
        return redirect()->to('/');
    }

    /**
     * Create a redirect method to Google api.
     *
     * @return void
     */
    public function ggRedirect()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Return a callback method from Google api.
     *
     * @return callback URL from Google
     */
    public function ggCallback(SocialAccountService $service)
    {
        $user = $service->createOrGetGoogleInUser(Socialite::driver('google')->user());
        auth()->login($user);
        return redirect()->to('/');
    }
}
