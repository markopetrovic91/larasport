<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $featured = Product::where('featured', true)->orderByDesc('id')->take(3)->get();
        $brands = Brand::all();
        return view('frontend.home', compact('featured', 'brands'));
    }

    public function privacyPolicy()
    {
        return view('frontend.privacyPolicy');
    }

    public function termsOfService()
    {
        return view('frontend.terms-of-service');
    }

    public function getContact()
    {
        return view('frontend.contact');
    }

    public function postContact(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);
        try {
            Mail::to('markopetrovic91@gmail.com')->send(new ContactMail($request));
            return redirect()->back()->with('success_message', 'Thank you for getting in touch! We will contact you as soon as possible.');
        } catch (Exception $e) {
            return 'greska drugar';
        }
    }
}
