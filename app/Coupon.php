<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    public static function findByCode($code)
    {
        return self::where('code', $code)->first();
    }
    public function coupon()
    {
        return $this->morphTo();
    }

    public function discount()
    {
        return $this->coupon->discount();
    }
    public function couponValue()
    {
        return $this->coupon->couponValue();
    }
}
