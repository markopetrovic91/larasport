<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['id', 'user_id', 'first_name', 'last_name', 'phone', 'country', 'address', 'zip', 'city'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function order()
    {
        $this->hasOne('App\Order');
    }
}
