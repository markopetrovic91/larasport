<?php

namespace App;

use App\Filters\Product\ProductFilters;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    protected $fillable = ['name', 'sku', 'slug', 'details', 'price', 'description', 'weight', 'product_image', 'category_id', 'featured'];

    public function categories()
    {
        return $this->belongsToMany('App\Category')->withTimestamps();
    }

    public function brand()
    {
        return $this->belongsTo('App\Product');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }
    public function presentPrice()
    {
        return money_format('$%i', $this->price);
    }

    public function scopeMightAlsoLike($query)
    {
        return $query->inRandomOrder()->take(4);
    }

    public function scopeFilter(Builder $builder, $request, array $filters = [])
    {
        return (new ProductFilters($request))->add($filters)->filter($builder);
    }

    public function favorited()
    {
        return (bool)Favorite::where('user_id', Auth::id())
            ->where('product_id', $this->id)
            ->first();
    }
    public function favorites()
    {
        return $this->belongsToMany(User::class, 'favorites', 'product_id', 'user_id')->withTimestamps();
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order')->withPivot('quantity');
    }

    public function attributesValues()
    {
        return $this->belongsToMany('App\AttributeValue');
    }
}
