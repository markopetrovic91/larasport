<?php

use App\Coupon;
use App\Category;
use App\Product;

if (!function_exists('formatPrice')) {
    function formatPrice($price)
    {
        return money_format("$%.2n", $price);
    }
}

if (!function_exists('updateCouponSession')) {
    function updateCouponSession()
    {
        if (session()->has('coupon')) {
            $coupon = Coupon::findByCode(session()->get('coupon')['name']);
            session()->forget('coupon');
            session()->put('coupon', [
                'name' => $coupon->code,
                'discount' => $coupon->discount()
            ]);
        }
    }
}

if (!function_exists('getAllTotals')) {
    function getAllTotals()
    {
        $discount = session()->get('coupon')['discount'] ?? 0;
        $newsubtotal = (Cart::subtotal() - $discount);
        $tax = config('cart.index') / 100;
        $newtotal = $newsubtotal;
        return collect([
            'tax' => $tax,
            'discount' => $discount,
            'newSubtotal' => $newsubtotal,
            'newTotal' => $newtotal
        ]);
    }
}
// Generate category list in dashboard
if (!function_exists('generateCategoryLists')) {
    function generateCategoryLists(array $elements, $parentId = 0, $indent = 0)
    {

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {

                if ($indent == 2) {
                    echo '--';
                } elseif ($indent == 1) {
                    echo '-';
                }
                echo '<li>' . $element['name'] . '</li>';
                $children = generateCategoryLists($elements, $element["id"], $indent + 1);
            } else {
                //                display root categories
                echo $element['name'];
                echo '<div>' . $indent . '</div>';
            }
        }
    }
}
// get number of products for certain category
if (!function_exists('adminGetProductNumber')) {
    function adminGetProductNumber($catID)
    {
        $cat = Category::find($catID);
        return count($cat->products);
    }
}

if (!function_exists('findProductById')) {
    function findProductById($id)
    {
        return $product = Product::find($id);
    }
}

if (!function_exists('getProductImage')) {
    function getProductImage($url)
    {
        return '/storage/' . $url;
    }
}
